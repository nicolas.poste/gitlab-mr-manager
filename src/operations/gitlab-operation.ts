import { GraphQLClient } from 'graphql-request';
import { MergeRequest } from '../model/gitlab-api';
import { GitlabResult } from '../model/gitlab-result';

export abstract class GitlabOperation {

    protected readonly GITLAB_HOST: string = String(process.env.GITLAB_HOST);

    public constructor(private graphQLClient: GraphQLClient, protected headersConfig: HeaderConfig) {
    }

    public proceed = (): void => {
        this.graphQLClient.request<GitlabResult>(this.query())
            .then(this.extractMrs)
            .then(this.filter)
            .then(mrs => mrs.forEach(this.safePerform))
    }

    private extractMrs = (res: GitlabResult): MergeRequest[] => {
        return [...res?.group?.projects?.nodes || [], ...res?.namespace?.projects?.nodes || []]
                .filter(node => node && node.mergeRequests && node.mergeRequests.nodes && node.mergeRequests.nodes.length)
                .flatMap(node => node?.mergeRequests?.nodes)
                .map(mr => <MergeRequest>mr)
            || [];
    }

    protected abstract query(): string;

    protected abstract filter(mrs: MergeRequest[]): MergeRequest[];

    private safePerform = (mr: MergeRequest): void => {
        try {
            this.perform(mr);
        } catch (e) {
            console.log(`Error with MR ${mr.id}: ${e}`);
        }
    }

    protected abstract perform(mr: MergeRequest): void;

    protected mrToString = (mr: MergeRequest) => `MergeRequest (${mr.projectId}, ${mr.iid}, ${mr.title})`;

}
