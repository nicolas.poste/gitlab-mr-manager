import { MergeRequest } from '../model/gitlab-api';
import axios from 'axios';
import { GitlabOperation } from './gitlab-operation'

// @ts-ignore
import preval from 'preval.macro';

export class ApproveRenovateMr extends GitlabOperation {

    protected query = (): string => {
        return preval`
        const fs = require('fs');
        const path = require('path');
        const file = path.resolve('queries', 'renovate-mrs.graphql');
        module.exports = fs.readFileSync(file, 'utf8');
    `;
    }

    protected filter = (mrs: MergeRequest[]): MergeRequest[] => {
        const renovateMrsToApprove: MergeRequest[] = mrs.filter(mr => mr && mr.approvalsLeft && mr.approvalsLeft > 0)
        console.log(`renovateMrsToApprove: ${renovateMrsToApprove.map(mr => this.mrToString(mr)).join(', ')}`);
        return renovateMrsToApprove;
    }

    protected perform = ({projectId, iid: mergeRequestId}: MergeRequest): void => {
        axios.post(`${this.GITLAB_HOST}/api/v4/projects/${projectId}/merge_requests/${mergeRequestId}/approve`,
            '',
            this.headersConfig);
    }
}
