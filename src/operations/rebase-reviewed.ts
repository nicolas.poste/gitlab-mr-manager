import { MergeRequest } from '../model/gitlab-api';
import axios from 'axios';
import { GitlabOperation } from './gitlab-operation';

// @ts-ignore
import preval from 'preval.macro';

export class RebaseReviewed extends GitlabOperation {

    protected query = (): string => {
        return preval`
        const fs = require('fs');
        const path = require('path');
        const file = path.resolve('queries', 'mrs-to-rebase.graphql');
        module.exports = fs.readFileSync(file, 'utf8');
    `;
    }

    protected filter = (mrs: MergeRequest[]): MergeRequest[] => {
        const mrsToRebase: MergeRequest[] = mrs.filter(mr => mr && mr.shouldBeRebased && !mr.workInProgress);
        console.log(`mrsToRebase: ${mrsToRebase.map(mr => this.mrToString(mr)).join(', ')}`);
        return mrsToRebase;
    }

    protected perform = ({projectId, iid: mergeRequestId}: MergeRequest) => {
        return axios.put(`${this.GITLAB_HOST}/api/v4/projects/${projectId}/merge_requests/${mergeRequestId}/rebase`,
            '',
            this.headersConfig);
    }
}
