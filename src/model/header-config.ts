interface HeaderConfig {
    headers: { [key: string]: string }
}
