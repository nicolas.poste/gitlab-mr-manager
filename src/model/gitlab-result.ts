import { Group, Namespace } from './gitlab-api';

export type GitlabResult = {
    group: Group;
    namespace: Namespace;
}
