export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
    ID: string;
    String: string;
    Boolean: boolean;
    Int: number;
    Float: number;
    /** Identifier of Board */
    BoardID: any;
    /** Identifier of Ci::Pipeline */
    CiPipelineID: any;
    /** Identifier of Clusters::Agent */
    ClustersAgentID: any;
    /** Identifier of Clusters::AgentToken */
    ClustersAgentTokenID: any;
    /** Identifier of DastScannerProfile */
    DastScannerProfileID: any;
    /** Identifier of DastSiteProfile */
    DastSiteProfileID: any;
    /** Identifier of DesignManagement::Design */
    DesignManagementDesignID: any;
    /** Identifier of Epic */
    EpicID: any;
    /** An ISO 8601-encoded date */
    ISO8601Date: any;
    /** Identifier of Issue */
    IssueID: any;
    /** Identifier of Iteration */
    IterationID: any;
    /** Represents untyped JSON */
    JSON: any;
    /** Identifier of Label */
    LabelID: any;
    /** Identifier of Milestone */
    MilestoneID: any;
    /** Time represented in ISO 8601 */
    Time: any;
    /** A regexp containing patterns sourced from user input */
    UntrustedRegexp: any;
    Upload: any;
    /** Identifier of User */
    UserID: any;
    /** Identifier of Vulnerability */
    VulnerabilityID: any;
};

export type CurrentUserTodos = {
    /** Todos for the current user */
    currentUserTodos: TodoConnection;
};


export type CurrentUserTodosCurrentUserTodosArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    state?: Maybe<TodoStateEnum>;
};

export type DesignFields = {
    /** The diff refs for this design */
    diffRefs: DiffRefs;
    /** How this design was changed in the current version */
    event: DesignVersionEvent;
    /** The filename of the design */
    filename: Scalars['String'];
    /** The full path to the design file */
    fullPath: Scalars['String'];
    /** The ID of this design */
    id: Scalars['ID'];
    /** The URL of the full-sized image */
    image: Scalars['String'];
    /** The URL of the design resized to fit within the bounds of 432x230. This will be `null` if the image has not been generated */
    imageV432x230?: Maybe<Scalars['String']>;
    /** The issue the design belongs to */
    issue: Issue;
    /** The total count of user-created notes for this design */
    notesCount: Scalars['Int'];
    /** The project the design belongs to */
    project: Project;
};

export type Entry = {
    /** Flat path of the entry */
    flatPath: Scalars['String'];
    /** ID of the entry */
    id: Scalars['ID'];
    /** Name of the entry */
    name: Scalars['String'];
    /** Path of the entry */
    path: Scalars['String'];
    /** Last commit sha for the entry */
    sha: Scalars['String'];
    /** Type of tree entry */
    type: EntryType;
};

export type MemberInterface = {
    /** GitLab::Access level */
    accessLevel?: Maybe<AccessLevel>;
    /** Date and time the membership was created */
    createdAt?: Maybe<Scalars['Time']>;
    /** User that authorized membership */
    createdBy?: Maybe<User>;
    /** Date and time the membership expires */
    expiresAt?: Maybe<Scalars['Time']>;
    /** ID of the member */
    id: Scalars['ID'];
    /** Date and time the membership was last updated */
    updatedAt?: Maybe<Scalars['Time']>;
    /** User that is associated with the member object */
    user: User;
};

export type Noteable = {
    /** All discussions on this noteable */
    discussions: DiscussionConnection;
    /** All notes on this noteable */
    notes: NoteConnection;
};


export type NoteableDiscussionsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type NoteableNotesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

export type ResolvableInterface = {
    /** Indicates if the object can be resolved */
    resolvable: Scalars['Boolean'];
    /** Indicates if the object is resolved */
    resolved: Scalars['Boolean'];
    /** Timestamp of when the object was resolved */
    resolvedAt?: Maybe<Scalars['Time']>;
    /** User who resolved the object */
    resolvedBy?: Maybe<User>;
};

export type Service = {
    /** Indicates if the service is active */
    active?: Maybe<Scalars['Boolean']>;
    /** Class name of the service */
    type?: Maybe<Scalars['String']>;
};

export type TimeboxBurnupTimeSeriesInterface = {
    /** Daily scope and completed totals for burnup charts */
    burnupTimeSeries?: Maybe<Array<BurnupChartDailyTotals>>;
};

/** Represents a vulnerability location. The fields with data will depend on the vulnerability report type */
export type VulnerabilityLocation = VulnerabilityLocationContainerScanning | VulnerabilityLocationCoverageFuzzing | VulnerabilityLocationDast | VulnerabilityLocationDependencyScanning | VulnerabilityLocationSast | VulnerabilityLocationSecretDetection;

/** Represents the access level of a relationship between a User and object that it is related to */
export type AccessLevel = {
    __typename?: 'AccessLevel';
    /** Integer representation of access level */
    integerValue?: Maybe<Scalars['Int']>;
    /** String representation of access level */
    stringValue?: Maybe<AccessLevelEnum>;
};

/** Autogenerated return type of AddAwardEmoji */
export type AddAwardEmojiPayload = {
    __typename?: 'AddAwardEmojiPayload';
    /** The award emoji after mutation */
    awardEmoji?: Maybe<AwardEmoji>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** Autogenerated return type of AddProjectToSecurityDashboard */
export type AddProjectToSecurityDashboardPayload = {
    __typename?: 'AddProjectToSecurityDashboardPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** Project that was added to the Instance Security Dashboard */
    project?: Maybe<Project>;
};

/** Autogenerated return type of AdminSidekiqQueuesDeleteJobs */
export type AdminSidekiqQueuesDeleteJobsPayload = {
    __typename?: 'AdminSidekiqQueuesDeleteJobsPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** Information about the status of the deletion request */
    result?: Maybe<DeleteJobsResponse>;
};

/** Describes an alert from the project's Alert Management */
export type AlertManagementAlert = Noteable & {
    __typename?: 'AlertManagementAlert';
    /** Assignees of the alert */
    assignees?: Maybe<UserConnection>;
    /** Timestamp the alert was created */
    createdAt?: Maybe<Scalars['Time']>;
    /** Description of the alert */
    description?: Maybe<Scalars['String']>;
    /** Alert details */
    details?: Maybe<Scalars['JSON']>;
    /** The URL of the alert detail page */
    detailsUrl: Scalars['String'];
    /** All discussions on this noteable */
    discussions: DiscussionConnection;
    /** Timestamp the alert ended */
    endedAt?: Maybe<Scalars['Time']>;
    /** Number of events of this alert */
    eventCount?: Maybe<Scalars['Int']>;
    /** List of hosts the alert came from */
    hosts?: Maybe<Array<Scalars['String']>>;
    /** Internal ID of the alert */
    iid: Scalars['ID'];
    /** Internal ID of the GitLab issue attached to the alert */
    issueIid?: Maybe<Scalars['ID']>;
    /** URL for metrics embed for the alert */
    metricsDashboardUrl?: Maybe<Scalars['String']>;
    /** Monitoring tool the alert came from */
    monitoringTool?: Maybe<Scalars['String']>;
    /** All notes on this noteable */
    notes: NoteConnection;
    /** The alert condition for Prometheus */
    prometheusAlert?: Maybe<PrometheusAlert>;
    /** Runbook for the alert as defined in alert details */
    runbook?: Maybe<Scalars['String']>;
    /** Service the alert came from */
    service?: Maybe<Scalars['String']>;
    /** Severity of the alert */
    severity?: Maybe<AlertManagementSeverity>;
    /** Timestamp the alert was raised */
    startedAt?: Maybe<Scalars['Time']>;
    /** Status of the alert */
    status?: Maybe<AlertManagementStatus>;
    /** Title of the alert */
    title?: Maybe<Scalars['String']>;
    /** Todos of the current user for the alert */
    todos?: Maybe<TodoConnection>;
    /** Timestamp the alert was last updated */
    updatedAt?: Maybe<Scalars['Time']>;
};


/** Describes an alert from the project's Alert Management */
export type AlertManagementAlertAssigneesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** Describes an alert from the project's Alert Management */
export type AlertManagementAlertDiscussionsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** Describes an alert from the project's Alert Management */
export type AlertManagementAlertNotesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** Describes an alert from the project's Alert Management */
export type AlertManagementAlertTodosArgs = {
    action?: Maybe<Array<TodoActionEnum>>;
    after?: Maybe<Scalars['String']>;
    authorId?: Maybe<Array<Scalars['ID']>>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    groupId?: Maybe<Array<Scalars['ID']>>;
    last?: Maybe<Scalars['Int']>;
    projectId?: Maybe<Array<Scalars['ID']>>;
    state?: Maybe<Array<TodoStateEnum>>;
    type?: Maybe<Array<TodoTargetEnum>>;
};

/** The connection type for AlertManagementAlert. */
export type AlertManagementAlertConnection = {
    __typename?: 'AlertManagementAlertConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<AlertManagementAlertEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<AlertManagementAlert>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type AlertManagementAlertEdge = {
    __typename?: 'AlertManagementAlertEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<AlertManagementAlert>;
};

/** Represents total number of alerts for the represented categories */
export type AlertManagementAlertStatusCountsType = {
    __typename?: 'AlertManagementAlertStatusCountsType';
    /** Number of alerts with status ACKNOWLEDGED for the project */
    acknowledged?: Maybe<Scalars['Int']>;
    /** Total number of alerts for the project */
    all?: Maybe<Scalars['Int']>;
    /** Number of alerts with status IGNORED for the project */
    ignored?: Maybe<Scalars['Int']>;
    /** Number of alerts with status TRIGGERED or ACKNOWLEDGED for the project */
    open?: Maybe<Scalars['Int']>;
    /** Number of alerts with status RESOLVED for the project */
    resolved?: Maybe<Scalars['Int']>;
    /** Number of alerts with status TRIGGERED for the project */
    triggered?: Maybe<Scalars['Int']>;
};

/** Autogenerated return type of AlertSetAssignees */
export type AlertSetAssigneesPayload = {
    __typename?: 'AlertSetAssigneesPayload';
    /** The alert after mutation */
    alert?: Maybe<AlertManagementAlert>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The issue created after mutation */
    issue?: Maybe<Issue>;
    /** The todo after mutation */
    todo?: Maybe<Todo>;
};

/** Autogenerated return type of AlertTodoCreate */
export type AlertTodoCreatePayload = {
    __typename?: 'AlertTodoCreatePayload';
    /** The alert after mutation */
    alert?: Maybe<AlertManagementAlert>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The issue created after mutation */
    issue?: Maybe<Issue>;
    /** The todo after mutation */
    todo?: Maybe<Todo>;
};

/** An emoji awarded by a user */
export type AwardEmoji = {
    __typename?: 'AwardEmoji';
    /** The emoji description */
    description: Scalars['String'];
    /** The emoji as an icon */
    emoji: Scalars['String'];
    /** The emoji name */
    name: Scalars['String'];
    /** The emoji in unicode */
    unicode: Scalars['String'];
    /** The unicode version for this emoji */
    unicodeVersion: Scalars['String'];
    /** The user who awarded the emoji */
    user: User;
};

/** Autogenerated return type of AwardEmojiAdd */
export type AwardEmojiAddPayload = {
    __typename?: 'AwardEmojiAddPayload';
    /** The award emoji after mutation */
    awardEmoji?: Maybe<AwardEmoji>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** Autogenerated return type of AwardEmojiRemove */
export type AwardEmojiRemovePayload = {
    __typename?: 'AwardEmojiRemovePayload';
    /** The award emoji after mutation */
    awardEmoji?: Maybe<AwardEmoji>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** Autogenerated return type of AwardEmojiToggle */
export type AwardEmojiTogglePayload = {
    __typename?: 'AwardEmojiTogglePayload';
    /** The award emoji after mutation */
    awardEmoji?: Maybe<AwardEmoji>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** Indicates the status of the emoji. True if the toggle awarded the emoji, and false if the toggle removed the emoji. */
    toggledOn: Scalars['Boolean'];
};

export type BaseService = Service & {
    __typename?: 'BaseService';
    /** Indicates if the service is active */
    active?: Maybe<Scalars['Boolean']>;
    /** Class name of the service */
    type?: Maybe<Scalars['String']>;
};

export type Blob = Entry & {
    __typename?: 'Blob';
    /** Flat path of the entry */
    flatPath: Scalars['String'];
    /** ID of the entry */
    id: Scalars['ID'];
    /** LFS ID of the blob */
    lfsOid?: Maybe<Scalars['String']>;
    /** Blob mode in numeric format */
    mode?: Maybe<Scalars['String']>;
    /** Name of the entry */
    name: Scalars['String'];
    /** Path of the entry */
    path: Scalars['String'];
    /** Last commit sha for the entry */
    sha: Scalars['String'];
    /** Type of tree entry */
    type: EntryType;
    /** Web path of the blob */
    webPath?: Maybe<Scalars['String']>;
    /** Web URL of the blob */
    webUrl?: Maybe<Scalars['String']>;
};

/** The connection type for Blob. */
export type BlobConnection = {
    __typename?: 'BlobConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<BlobEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Blob>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type BlobEdge = {
    __typename?: 'BlobEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Blob>;
};

/** Represents a project or group board */
export type Board = {
    __typename?: 'Board';
    /** The board assignee. */
    assignee?: Maybe<User>;
    /** Epics associated with board issues. */
    epics?: Maybe<EpicConnection>;
    /** Whether or not backlog list is hidden. */
    hideBacklogList?: Maybe<Scalars['Boolean']>;
    /** Whether or not closed list is hidden. */
    hideClosedList?: Maybe<Scalars['Boolean']>;
    /** ID (global ID) of the board */
    id: Scalars['ID'];
    /** Lists of the board */
    lists?: Maybe<BoardListConnection>;
    /** The board milestone. */
    milestone?: Maybe<Milestone>;
    /** Name of the board */
    name?: Maybe<Scalars['String']>;
    /** Weight of the board. */
    weight?: Maybe<Scalars['Int']>;
};


/** Represents a project or group board */
export type BoardEpicsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    issueFilters?: Maybe<BoardIssueInput>;
    last?: Maybe<Scalars['Int']>;
};


/** Represents a project or group board */
export type BoardListsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    id?: Maybe<Scalars['ID']>;
    last?: Maybe<Scalars['Int']>;
};

/** The connection type for Board. */
export type BoardConnection = {
    __typename?: 'BoardConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<BoardEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Board>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type BoardEdge = {
    __typename?: 'BoardEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Board>;
};

/** Represents a list for an issue board */
export type BoardList = {
    __typename?: 'BoardList';
    /** Assignee in the list */
    assignee?: Maybe<User>;
    /** Indicates if list is collapsed for this user */
    collapsed?: Maybe<Scalars['Boolean']>;
    /** ID (global ID) of the list */
    id: Scalars['ID'];
    /** Board issues */
    issues?: Maybe<IssueConnection>;
    /** Count of issues in the list */
    issuesCount?: Maybe<Scalars['Int']>;
    /** Label of the list */
    label?: Maybe<Label>;
    /** The current limit metric for the list */
    limitMetric?: Maybe<ListLimitMetric>;
    /** Type of the list */
    listType: Scalars['String'];
    /** Maximum number of issues in the list */
    maxIssueCount?: Maybe<Scalars['Int']>;
    /** Maximum weight of issues in the list */
    maxIssueWeight?: Maybe<Scalars['Int']>;
    /** Milestone of the list */
    milestone?: Maybe<Milestone>;
    /** Position of list within the board */
    position?: Maybe<Scalars['Int']>;
    /** Title of the list */
    title: Scalars['String'];
    /** Total weight of all issues in the list */
    totalWeight?: Maybe<Scalars['Int']>;
};


/** Represents a list for an issue board */
export type BoardListIssuesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    filters?: Maybe<BoardIssueInput>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** The connection type for BoardList. */
export type BoardListConnection = {
    __typename?: 'BoardListConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<BoardListEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<BoardList>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** Autogenerated return type of BoardListCreate */
export type BoardListCreatePayload = {
    __typename?: 'BoardListCreatePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** List of the issue board */
    list?: Maybe<BoardList>;
};

/** An edge in a connection. */
export type BoardListEdge = {
    __typename?: 'BoardListEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<BoardList>;
};

/** Autogenerated return type of BoardListUpdateLimitMetrics */
export type BoardListUpdateLimitMetricsPayload = {
    __typename?: 'BoardListUpdateLimitMetricsPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The updated list */
    list?: Maybe<BoardList>;
};

export type Branch = {
    __typename?: 'Branch';
    /** Commit for the branch */
    commit?: Maybe<Commit>;
    /** Name of the branch */
    name: Scalars['String'];
};

/** Represents the total number of issues and their weights for a particular day */
export type BurnupChartDailyTotals = {
    __typename?: 'BurnupChartDailyTotals';
    /** Number of closed issues as of this day */
    completedCount: Scalars['Int'];
    /** Total weight of closed issues as of this day */
    completedWeight: Scalars['Int'];
    /** Date for burnup totals */
    date: Scalars['ISO8601Date'];
    /** Number of issues as of this day */
    scopeCount: Scalars['Int'];
    /** Total weight of issues as of this day */
    scopeWeight: Scalars['Int'];
};

export type CiGroup = {
    __typename?: 'CiGroup';
    /** Jobs in group */
    jobs?: Maybe<CiJobConnection>;
    /** Name of the job group */
    name?: Maybe<Scalars['String']>;
    /** Size of the group */
    size?: Maybe<Scalars['Int']>;
};


export type CiGroupJobsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** The connection type for CiGroup. */
export type CiGroupConnection = {
    __typename?: 'CiGroupConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<CiGroupEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<CiGroup>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type CiGroupEdge = {
    __typename?: 'CiGroupEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<CiGroup>;
};

export type CiJob = {
    __typename?: 'CiJob';
    /** Name of the job */
    name?: Maybe<Scalars['String']>;
    /** Builds that must complete before the jobs run */
    needs?: Maybe<CiJobConnection>;
};


export type CiJobNeedsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** The connection type for CiJob. */
export type CiJobConnection = {
    __typename?: 'CiJobConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<CiJobEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<CiJob>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type CiJobEdge = {
    __typename?: 'CiJobEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<CiJob>;
};

export type CiStage = {
    __typename?: 'CiStage';
    /** Group of jobs for the stage */
    groups?: Maybe<CiGroupConnection>;
    /** Name of the stage */
    name?: Maybe<Scalars['String']>;
};


export type CiStageGroupsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** The connection type for CiStage. */
export type CiStageConnection = {
    __typename?: 'CiStageConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<CiStageEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<CiStage>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type CiStageEdge = {
    __typename?: 'CiStageEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<CiStage>;
};

export type ClusterAgent = {
    __typename?: 'ClusterAgent';
    /** Timestamp the cluster agent was created */
    createdAt?: Maybe<Scalars['Time']>;
    /** ID of the cluster agent */
    id: Scalars['ID'];
    /** Name of the cluster agent */
    name?: Maybe<Scalars['String']>;
    /** The project this cluster agent is associated with */
    project?: Maybe<Project>;
    /** Tokens associated with the cluster agent */
    tokens?: Maybe<ClusterAgentTokenConnection>;
    /** Timestamp the cluster agent was updated */
    updatedAt?: Maybe<Scalars['Time']>;
};


export type ClusterAgentTokensArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** The connection type for ClusterAgent. */
export type ClusterAgentConnection = {
    __typename?: 'ClusterAgentConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<ClusterAgentEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<ClusterAgent>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** Autogenerated return type of ClusterAgentDelete */
export type ClusterAgentDeletePayload = {
    __typename?: 'ClusterAgentDeletePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** An edge in a connection. */
export type ClusterAgentEdge = {
    __typename?: 'ClusterAgentEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<ClusterAgent>;
};

export type ClusterAgentToken = {
    __typename?: 'ClusterAgentToken';
    /** Cluster agent this token is associated with */
    clusterAgent?: Maybe<ClusterAgent>;
    /** Timestamp the token was created */
    createdAt?: Maybe<Scalars['Time']>;
    /** Global ID of the token */
    id: Scalars['ClustersAgentTokenID'];
};

/** The connection type for ClusterAgentToken. */
export type ClusterAgentTokenConnection = {
    __typename?: 'ClusterAgentTokenConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<ClusterAgentTokenEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<ClusterAgentToken>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** Autogenerated return type of ClusterAgentTokenCreate */
export type ClusterAgentTokenCreatePayload = {
    __typename?: 'ClusterAgentTokenCreatePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** Token secret value. Make sure you save it - you won't be able to access it again */
    secret?: Maybe<Scalars['String']>;
    /** Token created after mutation */
    token?: Maybe<ClusterAgentToken>;
};

/** Autogenerated return type of ClusterAgentTokenDelete */
export type ClusterAgentTokenDeletePayload = {
    __typename?: 'ClusterAgentTokenDeletePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** An edge in a connection. */
export type ClusterAgentTokenEdge = {
    __typename?: 'ClusterAgentTokenEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<ClusterAgentToken>;
};

export type Commit = {
    __typename?: 'Commit';
    /** Author of the commit */
    author?: Maybe<User>;
    /** Commit authors gravatar */
    authorGravatar?: Maybe<Scalars['String']>;
    /** Commit authors name */
    authorName?: Maybe<Scalars['String']>;
    /** Timestamp of when the commit was authored */
    authoredDate?: Maybe<Scalars['Time']>;
    /** Description of the commit message */
    description?: Maybe<Scalars['String']>;
    /** The GitLab Flavored Markdown rendering of `description` */
    descriptionHtml?: Maybe<Scalars['String']>;
    /** ID (global ID) of the commit */
    id: Scalars['ID'];
    /**
     * Latest pipeline of the commit. Deprecated in 12.5: Use `pipelines`
     * @deprecated Use `pipelines`. Deprecated in 12.5
     */
    latestPipeline?: Maybe<Pipeline>;
    /** Raw commit message */
    message?: Maybe<Scalars['String']>;
    /** Pipelines of the commit ordered latest first */
    pipelines?: Maybe<PipelineConnection>;
    /** SHA1 ID of the commit */
    sha: Scalars['String'];
    /** Rendered HTML of the commit signature */
    signatureHtml?: Maybe<Scalars['String']>;
    /** Title of the commit message */
    title?: Maybe<Scalars['String']>;
    /** The GitLab Flavored Markdown rendering of `title` */
    titleHtml?: Maybe<Scalars['String']>;
    /** Web path of the commit */
    webPath: Scalars['String'];
    /** Web URL of the commit */
    webUrl: Scalars['String'];
};


export type CommitLatestPipelineArgs = {
    ref?: Maybe<Scalars['String']>;
    sha?: Maybe<Scalars['String']>;
    status?: Maybe<PipelineStatusEnum>;
};


export type CommitPipelinesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    ref?: Maybe<Scalars['String']>;
    sha?: Maybe<Scalars['String']>;
    status?: Maybe<PipelineStatusEnum>;
};

/** Autogenerated return type of CommitCreate */
export type CommitCreatePayload = {
    __typename?: 'CommitCreatePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The commit after mutation */
    commit?: Maybe<Commit>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** Represents a ComplianceFramework associated with a Project */
export type ComplianceFramework = {
    __typename?: 'ComplianceFramework';
    /** Name of the compliance framework */
    name: ProjectSettingEnum;
};

/** The connection type for ComplianceFramework. */
export type ComplianceFrameworkConnection = {
    __typename?: 'ComplianceFrameworkConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<ComplianceFrameworkEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<ComplianceFramework>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type ComplianceFrameworkEdge = {
    __typename?: 'ComplianceFrameworkEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<ComplianceFramework>;
};

/** Autogenerated return type of ConfigureSast */
export type ConfigureSastPayload = {
    __typename?: 'ConfigureSastPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** Status of creating the commit for the supplied SAST CI configuration */
    status: Scalars['String'];
    /** Redirect path to use when the response is successful */
    successPath?: Maybe<Scalars['String']>;
};

/** A tag expiration policy designed to keep only the images that matter most */
export type ContainerExpirationPolicy = {
    __typename?: 'ContainerExpirationPolicy';
    /** This container expiration policy schedule */
    cadence: ContainerExpirationPolicyCadenceEnum;
    /** Timestamp of when the container expiration policy was created */
    createdAt: Scalars['Time'];
    /** Indicates whether this container expiration policy is enabled */
    enabled: Scalars['Boolean'];
    /** Number of tags to retain */
    keepN?: Maybe<ContainerExpirationPolicyKeepEnum>;
    /** Tags with names matching this regex pattern will expire */
    nameRegex?: Maybe<Scalars['UntrustedRegexp']>;
    /** Tags with names matching this regex pattern will be preserved */
    nameRegexKeep?: Maybe<Scalars['UntrustedRegexp']>;
    /** Next time that this container expiration policy will get executed */
    nextRunAt?: Maybe<Scalars['Time']>;
    /** Tags older that this will expire */
    olderThan?: Maybe<ContainerExpirationPolicyOlderThanEnum>;
    /** Timestamp of when the container expiration policy was updated */
    updatedAt: Scalars['Time'];
};

/** Autogenerated return type of CreateAlertIssue */
export type CreateAlertIssuePayload = {
    __typename?: 'CreateAlertIssuePayload';
    /** The alert after mutation */
    alert?: Maybe<AlertManagementAlert>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The issue created after mutation */
    issue?: Maybe<Issue>;
    /** The todo after mutation */
    todo?: Maybe<Todo>;
};

/** Autogenerated return type of CreateAnnotation */
export type CreateAnnotationPayload = {
    __typename?: 'CreateAnnotationPayload';
    /** The created annotation */
    annotation?: Maybe<MetricsDashboardAnnotation>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** Autogenerated return type of CreateBranch */
export type CreateBranchPayload = {
    __typename?: 'CreateBranchPayload';
    /** Branch after mutation */
    branch?: Maybe<Branch>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** Autogenerated return type of CreateClusterAgent */
export type CreateClusterAgentPayload = {
    __typename?: 'CreateClusterAgentPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Cluster agent created after mutation */
    clusterAgent?: Maybe<ClusterAgent>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** Autogenerated return type of CreateDiffNote */
export type CreateDiffNotePayload = {
    __typename?: 'CreateDiffNotePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The note after mutation */
    note?: Maybe<Note>;
};

/** Autogenerated return type of CreateEpic */
export type CreateEpicPayload = {
    __typename?: 'CreateEpicPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The created epic */
    epic?: Maybe<Epic>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** Autogenerated return type of CreateImageDiffNote */
export type CreateImageDiffNotePayload = {
    __typename?: 'CreateImageDiffNotePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The note after mutation */
    note?: Maybe<Note>;
};

/** Autogenerated return type of CreateIteration */
export type CreateIterationPayload = {
    __typename?: 'CreateIterationPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The created iteration */
    iteration?: Maybe<Iteration>;
};

/** Autogenerated return type of CreateNote */
export type CreateNotePayload = {
    __typename?: 'CreateNotePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The note after mutation */
    note?: Maybe<Note>;
};

/** Autogenerated return type of CreateRequirement */
export type CreateRequirementPayload = {
    __typename?: 'CreateRequirementPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The requirement after mutation */
    requirement?: Maybe<Requirement>;
};

/** Autogenerated return type of CreateSnippet */
export type CreateSnippetPayload = {
    __typename?: 'CreateSnippetPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The snippet after mutation */
    snippet?: Maybe<Snippet>;
};

/** Autogenerated return type of CreateTestCase */
export type CreateTestCasePayload = {
    __typename?: 'CreateTestCasePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The test case created */
    testCase?: Maybe<Issue>;
};

/** Autogenerated return type of DastOnDemandScanCreate */
export type DastOnDemandScanCreatePayload = {
    __typename?: 'DastOnDemandScanCreatePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** URL of the pipeline that was created. */
    pipelineUrl?: Maybe<Scalars['String']>;
};

/** Represents a DAST scanner profile */
export type DastScannerProfile = {
    __typename?: 'DastScannerProfile';
    /** Relative web path to the edit page of a scanner profile */
    editPath?: Maybe<Scalars['String']>;
    /** ID of the DAST scanner profile */
    globalId: Scalars['DastScannerProfileID'];
    /**
     * ID of the DAST scanner profile. Deprecated in 13.4: Use `global_id`
     * @deprecated Use `global_id`. Deprecated in 13.4
     */
    id: Scalars['ID'];
    /** Name of the DAST scanner profile */
    profileName?: Maybe<Scalars['String']>;
    /** The maximum number of minutes allowed for the spider to traverse the site */
    spiderTimeout?: Maybe<Scalars['Int']>;
    /** The maximum number of seconds allowed for the site under test to respond to a request */
    targetTimeout?: Maybe<Scalars['Int']>;
};

/** The connection type for DastScannerProfile. */
export type DastScannerProfileConnection = {
    __typename?: 'DastScannerProfileConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<DastScannerProfileEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<DastScannerProfile>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** Autogenerated return type of DastScannerProfileCreate */
export type DastScannerProfileCreatePayload = {
    __typename?: 'DastScannerProfileCreatePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** ID of the scanner profile. */
    globalId?: Maybe<Scalars['DastScannerProfileID']>;
    /**
     * ID of the scanner profile.. Deprecated in 13.4: Use `global_id`
     * @deprecated Use `global_id`. Deprecated in 13.4
     */
    id?: Maybe<Scalars['ID']>;
};

/** Autogenerated return type of DastScannerProfileDelete */
export type DastScannerProfileDeletePayload = {
    __typename?: 'DastScannerProfileDeletePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** An edge in a connection. */
export type DastScannerProfileEdge = {
    __typename?: 'DastScannerProfileEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<DastScannerProfile>;
};

/** Autogenerated return type of DastScannerProfileUpdate */
export type DastScannerProfileUpdatePayload = {
    __typename?: 'DastScannerProfileUpdatePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** ID of the scanner profile. */
    id?: Maybe<Scalars['DastScannerProfileID']>;
};

/** Represents a DAST Site Profile */
export type DastSiteProfile = {
    __typename?: 'DastSiteProfile';
    /** Relative web path to the edit page of a site profile */
    editPath?: Maybe<Scalars['String']>;
    /** ID of the site profile */
    id: Scalars['DastSiteProfileID'];
    /** The name of the site profile */
    profileName?: Maybe<Scalars['String']>;
    /** The URL of the target to be scanned */
    targetUrl?: Maybe<Scalars['String']>;
    /** Permissions for the current user on the resource */
    userPermissions: DastSiteProfilePermissions;
    /** The current validation status of the site profile */
    validationStatus?: Maybe<DastSiteProfileValidationStatusEnum>;
};

/** The connection type for DastSiteProfile. */
export type DastSiteProfileConnection = {
    __typename?: 'DastSiteProfileConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<DastSiteProfileEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<DastSiteProfile>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** Autogenerated return type of DastSiteProfileCreate */
export type DastSiteProfileCreatePayload = {
    __typename?: 'DastSiteProfileCreatePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** ID of the site profile. */
    id?: Maybe<Scalars['DastSiteProfileID']>;
};

/** Autogenerated return type of DastSiteProfileDelete */
export type DastSiteProfileDeletePayload = {
    __typename?: 'DastSiteProfileDeletePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** An edge in a connection. */
export type DastSiteProfileEdge = {
    __typename?: 'DastSiteProfileEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<DastSiteProfile>;
};

/** Check permissions for the current user on site profile */
export type DastSiteProfilePermissions = {
    __typename?: 'DastSiteProfilePermissions';
    /** Indicates the user can perform `create_on_demand_dast_scan` on this resource */
    createOnDemandDastScan: Scalars['Boolean'];
};

/** Autogenerated return type of DastSiteProfileUpdate */
export type DastSiteProfileUpdatePayload = {
    __typename?: 'DastSiteProfileUpdatePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** ID of the site profile. */
    id?: Maybe<Scalars['DastSiteProfileID']>;
};

/** Autogenerated return type of DeleteAnnotation */
export type DeleteAnnotationPayload = {
    __typename?: 'DeleteAnnotationPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** The response from the AdminSidekiqQueuesDeleteJobs mutation */
export type DeleteJobsResponse = {
    __typename?: 'DeleteJobsResponse';
    /** Whether or not the entire queue was processed in time; if not, retrying the same request is safe */
    completed?: Maybe<Scalars['Boolean']>;
    /** The number of matching jobs deleted */
    deletedJobs?: Maybe<Scalars['Int']>;
    /** The queue size after processing */
    queueSize?: Maybe<Scalars['Int']>;
};

/** A single design */
export type Design = CurrentUserTodos & DesignFields & Noteable & {
    __typename?: 'Design';
    /** Todos for the current user */
    currentUserTodos: TodoConnection;
    /** The diff refs for this design */
    diffRefs: DiffRefs;
    /** All discussions on this noteable */
    discussions: DiscussionConnection;
    /** How this design was changed in the current version */
    event: DesignVersionEvent;
    /** The filename of the design */
    filename: Scalars['String'];
    /** The full path to the design file */
    fullPath: Scalars['String'];
    /** The ID of this design */
    id: Scalars['ID'];
    /** The URL of the full-sized image */
    image: Scalars['String'];
    /** The URL of the design resized to fit within the bounds of 432x230. This will be `null` if the image has not been generated */
    imageV432x230?: Maybe<Scalars['String']>;
    /** The issue the design belongs to */
    issue: Issue;
    /** All notes on this noteable */
    notes: NoteConnection;
    /** The total count of user-created notes for this design */
    notesCount: Scalars['Int'];
    /** The project the design belongs to */
    project: Project;
    /** All versions related to this design ordered newest first */
    versions: DesignVersionConnection;
};


/** A single design */
export type DesignCurrentUserTodosArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    state?: Maybe<TodoStateEnum>;
};


/** A single design */
export type DesignDiscussionsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** A single design */
export type DesignNotesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** A single design */
export type DesignVersionsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    earlierOrEqualToId?: Maybe<Scalars['ID']>;
    earlierOrEqualToSha?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** A design pinned to a specific version. The image field reflects the design as of the associated version */
export type DesignAtVersion = DesignFields & {
    __typename?: 'DesignAtVersion';
    /** The underlying design */
    design: Design;
    /** The diff refs for this design */
    diffRefs: DiffRefs;
    /** How this design was changed in the current version */
    event: DesignVersionEvent;
    /** The filename of the design */
    filename: Scalars['String'];
    /** The full path to the design file */
    fullPath: Scalars['String'];
    /** The ID of this design */
    id: Scalars['ID'];
    /** The URL of the full-sized image */
    image: Scalars['String'];
    /** The URL of the design resized to fit within the bounds of 432x230. This will be `null` if the image has not been generated */
    imageV432x230?: Maybe<Scalars['String']>;
    /** The issue the design belongs to */
    issue: Issue;
    /** The total count of user-created notes for this design */
    notesCount: Scalars['Int'];
    /** The project the design belongs to */
    project: Project;
    /** The version this design-at-versions is pinned to */
    version: DesignVersion;
};

/** The connection type for DesignAtVersion. */
export type DesignAtVersionConnection = {
    __typename?: 'DesignAtVersionConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<DesignAtVersionEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<DesignAtVersion>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type DesignAtVersionEdge = {
    __typename?: 'DesignAtVersionEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<DesignAtVersion>;
};

/** A collection of designs */
export type DesignCollection = {
    __typename?: 'DesignCollection';
    /** Find a specific design */
    design?: Maybe<Design>;
    /** Find a design as of a version */
    designAtVersion?: Maybe<DesignAtVersion>;
    /** All designs for the design collection */
    designs: DesignConnection;
    /** Issue associated with the design collection */
    issue: Issue;
    /** Project associated with the design collection */
    project: Project;
    /** A specific version */
    version?: Maybe<DesignVersion>;
    /** All versions related to all designs, ordered newest first */
    versions: DesignVersionConnection;
};


/** A collection of designs */
export type DesignCollectionDesignArgs = {
    filename?: Maybe<Scalars['String']>;
    id?: Maybe<Scalars['ID']>;
};


/** A collection of designs */
export type DesignCollectionDesignAtVersionArgs = {
    id: Scalars['ID'];
};


/** A collection of designs */
export type DesignCollectionDesignsArgs = {
    after?: Maybe<Scalars['String']>;
    atVersion?: Maybe<Scalars['ID']>;
    before?: Maybe<Scalars['String']>;
    filenames?: Maybe<Array<Scalars['String']>>;
    first?: Maybe<Scalars['Int']>;
    ids?: Maybe<Array<Scalars['ID']>>;
    last?: Maybe<Scalars['Int']>;
};


/** A collection of designs */
export type DesignCollectionVersionArgs = {
    id?: Maybe<Scalars['ID']>;
    sha?: Maybe<Scalars['String']>;
};


/** A collection of designs */
export type DesignCollectionVersionsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    earlierOrEqualToId?: Maybe<Scalars['ID']>;
    earlierOrEqualToSha?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** The connection type for Design. */
export type DesignConnection = {
    __typename?: 'DesignConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<DesignEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Design>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type DesignEdge = {
    __typename?: 'DesignEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Design>;
};

export type DesignManagement = {
    __typename?: 'DesignManagement';
    /** Find a design as of a version */
    designAtVersion?: Maybe<DesignAtVersion>;
    /** Find a version */
    version?: Maybe<DesignVersion>;
};


export type DesignManagementDesignAtVersionArgs = {
    id: Scalars['ID'];
};


export type DesignManagementVersionArgs = {
    id: Scalars['ID'];
};

/** Autogenerated return type of DesignManagementDelete */
export type DesignManagementDeletePayload = {
    __typename?: 'DesignManagementDeletePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The new version in which the designs are deleted */
    version?: Maybe<DesignVersion>;
};

/** Autogenerated return type of DesignManagementMove */
export type DesignManagementMovePayload = {
    __typename?: 'DesignManagementMovePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The current state of the collection */
    designCollection?: Maybe<DesignCollection>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** Autogenerated return type of DesignManagementUpload */
export type DesignManagementUploadPayload = {
    __typename?: 'DesignManagementUploadPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The designs that were uploaded by the mutation */
    designs: Array<Design>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** Any designs that were skipped from the upload due to there being no change to their content since their last version */
    skippedDesigns: Array<Design>;
};

/** A specific version in which designs were added, modified or deleted */
export type DesignVersion = {
    __typename?: 'DesignVersion';
    /** A particular design as of this version, provided it is visible at this version */
    designAtVersion: DesignAtVersion;
    /** All designs that were changed in the version */
    designs: DesignConnection;
    /** All designs that are visible at this version, as of this version */
    designsAtVersion: DesignAtVersionConnection;
    /** ID of the design version */
    id: Scalars['ID'];
    /** SHA of the design version */
    sha: Scalars['ID'];
};


/** A specific version in which designs were added, modified or deleted */
export type DesignVersionDesignAtVersionArgs = {
    designId?: Maybe<Scalars['ID']>;
    filename?: Maybe<Scalars['String']>;
    id?: Maybe<Scalars['ID']>;
};


/** A specific version in which designs were added, modified or deleted */
export type DesignVersionDesignsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** A specific version in which designs were added, modified or deleted */
export type DesignVersionDesignsAtVersionArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    filenames?: Maybe<Array<Scalars['String']>>;
    first?: Maybe<Scalars['Int']>;
    ids?: Maybe<Array<Scalars['ID']>>;
    last?: Maybe<Scalars['Int']>;
};

/** The connection type for DesignVersion. */
export type DesignVersionConnection = {
    __typename?: 'DesignVersionConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<DesignVersionEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<DesignVersion>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type DesignVersionEdge = {
    __typename?: 'DesignVersionEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<DesignVersion>;
};

/** Autogenerated return type of DestroyBoard */
export type DestroyBoardPayload = {
    __typename?: 'DestroyBoardPayload';
    /** The board after mutation */
    board?: Maybe<Board>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** Autogenerated return type of DestroyNote */
export type DestroyNotePayload = {
    __typename?: 'DestroyNotePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The note after mutation */
    note?: Maybe<Note>;
};

/** Autogenerated return type of DestroySnippet */
export type DestroySnippetPayload = {
    __typename?: 'DestroySnippetPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The snippet after mutation */
    snippet?: Maybe<Snippet>;
};

export type DetailedStatus = {
    __typename?: 'DetailedStatus';
    /** Path of the details for the pipeline status */
    detailsPath: Scalars['String'];
    /** Favicon of the pipeline status */
    favicon: Scalars['String'];
    /** Group of the pipeline status */
    group: Scalars['String'];
    /** Indicates if the pipeline status has further details */
    hasDetails: Scalars['Boolean'];
    /** Icon of the pipeline status */
    icon: Scalars['String'];
    /** Label of the pipeline status */
    label: Scalars['String'];
    /** Text of the pipeline status */
    text: Scalars['String'];
    /** Tooltip associated with the pipeline status */
    tooltip: Scalars['String'];
};

export type DiffPosition = {
    __typename?: 'DiffPosition';
    /** Information about the branch, HEAD, and base at the time of commenting */
    diffRefs: DiffRefs;
    /** Path of the file that was changed */
    filePath: Scalars['String'];
    /** Total height of the image */
    height?: Maybe<Scalars['Int']>;
    /** Line on HEAD SHA that was changed */
    newLine?: Maybe<Scalars['Int']>;
    /** Path of the file on the HEAD SHA */
    newPath?: Maybe<Scalars['String']>;
    /** Line on start SHA that was changed */
    oldLine?: Maybe<Scalars['Int']>;
    /** Path of the file on the start SHA */
    oldPath?: Maybe<Scalars['String']>;
    /** Type of file the position refers to */
    positionType: DiffPositionType;
    /** Total width of the image */
    width?: Maybe<Scalars['Int']>;
    /** X position of the note */
    x?: Maybe<Scalars['Int']>;
    /** Y position of the note */
    y?: Maybe<Scalars['Int']>;
};

export type DiffRefs = {
    __typename?: 'DiffRefs';
    /** Merge base of the branch the comment was made on */
    baseSha?: Maybe<Scalars['String']>;
    /** SHA of the HEAD at the time the comment was made */
    headSha: Scalars['String'];
    /** SHA of the branch being compared against */
    startSha: Scalars['String'];
};

/** Changes to a single file */
export type DiffStats = {
    __typename?: 'DiffStats';
    /** Number of lines added to this file */
    additions: Scalars['Int'];
    /** Number of lines deleted from this file */
    deletions: Scalars['Int'];
    /** File path, relative to repository root */
    path: Scalars['String'];
};

/** Aggregated summary of changes */
export type DiffStatsSummary = {
    __typename?: 'DiffStatsSummary';
    /** Number of lines added */
    additions: Scalars['Int'];
    /** Number of lines changed */
    changes: Scalars['Int'];
    /** Number of lines deleted */
    deletions: Scalars['Int'];
    /** Number of files changed */
    fileCount: Scalars['Int'];
};

export type Discussion = ResolvableInterface & {
    __typename?: 'Discussion';
    /** Timestamp of the discussion's creation */
    createdAt: Scalars['Time'];
    /** ID of this discussion */
    id: Scalars['ID'];
    /** All notes in the discussion */
    notes: NoteConnection;
    /** ID used to reply to this discussion */
    replyId: Scalars['ID'];
    /** Indicates if the object can be resolved */
    resolvable: Scalars['Boolean'];
    /** Indicates if the object is resolved */
    resolved: Scalars['Boolean'];
    /** Timestamp of when the object was resolved */
    resolvedAt?: Maybe<Scalars['Time']>;
    /** User who resolved the object */
    resolvedBy?: Maybe<User>;
};


export type DiscussionNotesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** The connection type for Discussion. */
export type DiscussionConnection = {
    __typename?: 'DiscussionConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<DiscussionEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Discussion>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type DiscussionEdge = {
    __typename?: 'DiscussionEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Discussion>;
};

/** Autogenerated return type of DiscussionToggleResolve */
export type DiscussionToggleResolvePayload = {
    __typename?: 'DiscussionToggleResolvePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The discussion after mutation */
    discussion?: Maybe<Discussion>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** Autogenerated return type of DismissVulnerability */
export type DismissVulnerabilityPayload = {
    __typename?: 'DismissVulnerabilityPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The vulnerability after dismissal */
    vulnerability?: Maybe<Vulnerability>;
};

/** Describes where code is deployed for a project */
export type Environment = {
    __typename?: 'Environment';
    /** ID of the environment */
    id: Scalars['ID'];
    /** The most severe open alert for the environment. If multiple alerts have equal severity, the most recent is returned. */
    latestOpenedMostSevereAlert?: Maybe<AlertManagementAlert>;
    /** Metrics dashboard schema for the environment */
    metricsDashboard?: Maybe<MetricsDashboard>;
    /** Human-readable name of the environment */
    name: Scalars['String'];
    /** State of the environment, for example: available/stopped */
    state: Scalars['String'];
};


/** Describes where code is deployed for a project */
export type EnvironmentMetricsDashboardArgs = {
    path: Scalars['String'];
};

/** The connection type for Environment. */
export type EnvironmentConnection = {
    __typename?: 'EnvironmentConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<EnvironmentEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Environment>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type EnvironmentEdge = {
    __typename?: 'EnvironmentEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Environment>;
};

/** Represents an epic */
export type Epic = CurrentUserTodos & Noteable & {
    __typename?: 'Epic';
    /** Author of the epic */
    author: User;
    /** Children (sub-epics) of the epic */
    children?: Maybe<EpicConnection>;
    /** Timestamp of the epic's closure */
    closedAt?: Maybe<Scalars['Time']>;
    /** Indicates if the epic is confidential */
    confidential?: Maybe<Scalars['Boolean']>;
    /** Timestamp of the epic's creation */
    createdAt?: Maybe<Scalars['Time']>;
    /** Todos for the current user */
    currentUserTodos: TodoConnection;
    /** Number of open and closed descendant epics and issues */
    descendantCounts?: Maybe<EpicDescendantCount>;
    /** Total weight of open and closed issues in the epic and its descendants */
    descendantWeightSum?: Maybe<EpicDescendantWeights>;
    /** Description of the epic */
    description?: Maybe<Scalars['String']>;
    /** All discussions on this noteable */
    discussions: DiscussionConnection;
    /** Number of downvotes the epic has received */
    downvotes: Scalars['Int'];
    /** Due date of the epic */
    dueDate?: Maybe<Scalars['Time']>;
    /** Fixed due date of the epic */
    dueDateFixed?: Maybe<Scalars['Time']>;
    /** Inherited due date of the epic from milestones */
    dueDateFromMilestones?: Maybe<Scalars['Time']>;
    /** Indicates if the due date has been manually set */
    dueDateIsFixed?: Maybe<Scalars['Boolean']>;
    /** Group to which the epic belongs */
    group: Group;
    /** Indicates if the epic has children */
    hasChildren: Scalars['Boolean'];
    /** Indicates if the epic has direct issues */
    hasIssues: Scalars['Boolean'];
    /** Indicates if the epic has a parent epic */
    hasParent: Scalars['Boolean'];
    /** Current health status of the epic */
    healthStatus?: Maybe<EpicHealthStatus>;
    /** ID of the epic */
    id: Scalars['ID'];
    /** Internal ID of the epic */
    iid: Scalars['ID'];
    /** A list of issues associated with the epic */
    issues?: Maybe<EpicIssueConnection>;
    /** Labels assigned to the epic */
    labels?: Maybe<LabelConnection>;
    /** All notes on this noteable */
    notes: NoteConnection;
    /** Parent epic of the epic */
    parent?: Maybe<Epic>;
    /** List of participants for the epic */
    participants?: Maybe<UserConnection>;
    /** Internal reference of the epic. Returned in shortened format by default */
    reference: Scalars['String'];
    /** URI path of the epic-issue relationship */
    relationPath?: Maybe<Scalars['String']>;
    /** The relative position of the epic in the epic tree */
    relativePosition?: Maybe<Scalars['Int']>;
    /** Start date of the epic */
    startDate?: Maybe<Scalars['Time']>;
    /** Fixed start date of the epic */
    startDateFixed?: Maybe<Scalars['Time']>;
    /** Inherited start date of the epic from milestones */
    startDateFromMilestones?: Maybe<Scalars['Time']>;
    /** Indicates if the start date has been manually set */
    startDateIsFixed?: Maybe<Scalars['Boolean']>;
    /** State of the epic */
    state: EpicState;
    /** Indicates the currently logged in user is subscribed to the epic */
    subscribed: Scalars['Boolean'];
    /** Title of the epic */
    title?: Maybe<Scalars['String']>;
    /** Timestamp of the epic's last activity */
    updatedAt?: Maybe<Scalars['Time']>;
    /** Number of upvotes the epic has received */
    upvotes: Scalars['Int'];
    /** Permissions for the current user on the resource */
    userPermissions: EpicPermissions;
    /** Web path of the epic */
    webPath: Scalars['String'];
    /** Web URL of the epic */
    webUrl: Scalars['String'];
};


/** Represents an epic */
export type EpicChildrenArgs = {
    after?: Maybe<Scalars['String']>;
    authorUsername?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    endDate?: Maybe<Scalars['Time']>;
    first?: Maybe<Scalars['Int']>;
    iid?: Maybe<Scalars['ID']>;
    iidStartsWith?: Maybe<Scalars['String']>;
    iids?: Maybe<Array<Scalars['ID']>>;
    labelName?: Maybe<Array<Scalars['String']>>;
    last?: Maybe<Scalars['Int']>;
    milestoneTitle?: Maybe<Scalars['String']>;
    search?: Maybe<Scalars['String']>;
    sort?: Maybe<EpicSort>;
    startDate?: Maybe<Scalars['Time']>;
    state?: Maybe<EpicState>;
};


/** Represents an epic */
export type EpicCurrentUserTodosArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    state?: Maybe<TodoStateEnum>;
};


/** Represents an epic */
export type EpicDiscussionsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** Represents an epic */
export type EpicIssuesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** Represents an epic */
export type EpicLabelsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** Represents an epic */
export type EpicNotesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** Represents an epic */
export type EpicParticipantsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** Represents an epic */
export type EpicReferenceArgs = {
    full?: Maybe<Scalars['Boolean']>;
};

/** Autogenerated return type of EpicAddIssue */
export type EpicAddIssuePayload = {
    __typename?: 'EpicAddIssuePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The epic after mutation */
    epic?: Maybe<Epic>;
    /** The epic-issue relation */
    epicIssue?: Maybe<EpicIssue>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** The connection type for Epic. */
export type EpicConnection = {
    __typename?: 'EpicConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<EpicEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Epic>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** Counts of descendent epics */
export type EpicDescendantCount = {
    __typename?: 'EpicDescendantCount';
    /** Number of closed child epics */
    closedEpics?: Maybe<Scalars['Int']>;
    /** Number of closed epic issues */
    closedIssues?: Maybe<Scalars['Int']>;
    /** Number of opened child epics */
    openedEpics?: Maybe<Scalars['Int']>;
    /** Number of opened epic issues */
    openedIssues?: Maybe<Scalars['Int']>;
};

/** Total weight of open and closed descendant issues */
export type EpicDescendantWeights = {
    __typename?: 'EpicDescendantWeights';
    /** Total weight of completed (closed) issues in this epic, including epic descendants */
    closedIssues?: Maybe<Scalars['Int']>;
    /** Total weight of opened issues in this epic, including epic descendants */
    openedIssues?: Maybe<Scalars['Int']>;
};

/** An edge in a connection. */
export type EpicEdge = {
    __typename?: 'EpicEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Epic>;
};

/** Health status of child issues */
export type EpicHealthStatus = {
    __typename?: 'EpicHealthStatus';
    /** Number of issues at risk */
    issuesAtRisk?: Maybe<Scalars['Int']>;
    /** Number of issues that need attention */
    issuesNeedingAttention?: Maybe<Scalars['Int']>;
    /** Number of issues on track */
    issuesOnTrack?: Maybe<Scalars['Int']>;
};

/** Relationship between an epic and an issue */
export type EpicIssue = CurrentUserTodos & Noteable & {
    __typename?: 'EpicIssue';
    /** Alert associated to this issue */
    alertManagementAlert?: Maybe<AlertManagementAlert>;
    /** Assignees of the issue */
    assignees?: Maybe<UserConnection>;
    /** User that created the issue */
    author: User;
    /** Indicates the issue is blocked */
    blocked: Scalars['Boolean'];
    /** Timestamp of when the issue was closed */
    closedAt?: Maybe<Scalars['Time']>;
    /** Indicates the issue is confidential */
    confidential: Scalars['Boolean'];
    /** Timestamp of when the issue was created */
    createdAt: Scalars['Time'];
    /** Todos for the current user */
    currentUserTodos: TodoConnection;
    /** Description of the issue */
    description?: Maybe<Scalars['String']>;
    /** The GitLab Flavored Markdown rendering of `description` */
    descriptionHtml?: Maybe<Scalars['String']>;
    /** Collection of design images associated with this issue */
    designCollection?: Maybe<DesignCollection>;
    /**
     * The designs associated with this issue. Deprecated in 12.2: Use `designCollection`
     * @deprecated Use `designCollection`. Deprecated in 12.2
     */
    designs?: Maybe<DesignCollection>;
    /** Indicates discussion is locked on the issue */
    discussionLocked: Scalars['Boolean'];
    /** All discussions on this noteable */
    discussions: DiscussionConnection;
    /** Number of downvotes the issue has received */
    downvotes: Scalars['Int'];
    /** Due date of the issue */
    dueDate?: Maybe<Scalars['Time']>;
    /** Epic to which this issue belongs */
    epic?: Maybe<Epic>;
    /** ID of the epic-issue relation */
    epicIssueId: Scalars['ID'];
    /** Current health status. Returns null if `save_issuable_health_status` feature flag is disabled. */
    healthStatus?: Maybe<HealthStatus>;
    /** Global ID of the epic-issue relation */
    id?: Maybe<Scalars['ID']>;
    /** Internal ID of the issue */
    iid: Scalars['ID'];
    /** Iteration of the issue */
    iteration?: Maybe<Iteration>;
    /** Labels of the issue */
    labels?: Maybe<LabelConnection>;
    /** Milestone of the issue */
    milestone?: Maybe<Milestone>;
    /** All notes on this noteable */
    notes: NoteConnection;
    /** List of participants in the issue */
    participants?: Maybe<UserConnection>;
    /** Internal reference of the issue. Returned in shortened format by default */
    reference: Scalars['String'];
    /** URI path of the epic-issue relation */
    relationPath?: Maybe<Scalars['String']>;
    /** Relative position of the issue (used for positioning in epic tree and issue boards) */
    relativePosition?: Maybe<Scalars['Int']>;
    /** Severity level of the incident */
    severity?: Maybe<IssuableSeverity>;
    /** State of the issue */
    state: IssueState;
    /** Indicates whether an issue is published to the status page */
    statusPagePublishedIncident?: Maybe<Scalars['Boolean']>;
    /** Indicates the currently logged in user is subscribed to the issue */
    subscribed: Scalars['Boolean'];
    /** Task completion status of the issue */
    taskCompletionStatus: TaskCompletionStatus;
    /** Time estimate of the issue */
    timeEstimate: Scalars['Int'];
    /** Title of the issue */
    title: Scalars['String'];
    /** The GitLab Flavored Markdown rendering of `title` */
    titleHtml?: Maybe<Scalars['String']>;
    /** Total time reported as spent on the issue */
    totalTimeSpent: Scalars['Int'];
    /** Type of the issue */
    type?: Maybe<IssueType>;
    /** Timestamp of when the issue was last updated */
    updatedAt: Scalars['Time'];
    /** Number of upvotes the issue has received */
    upvotes: Scalars['Int'];
    /** Number of user notes of the issue */
    userNotesCount: Scalars['Int'];
    /** Permissions for the current user on the resource */
    userPermissions: IssuePermissions;
    /** Web path of the issue */
    webPath: Scalars['String'];
    /** Web URL of the issue */
    webUrl: Scalars['String'];
    /** Weight of the issue */
    weight?: Maybe<Scalars['Int']>;
};


/** Relationship between an epic and an issue */
export type EpicIssueAssigneesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** Relationship between an epic and an issue */
export type EpicIssueCurrentUserTodosArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    state?: Maybe<TodoStateEnum>;
};


/** Relationship between an epic and an issue */
export type EpicIssueDiscussionsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** Relationship between an epic and an issue */
export type EpicIssueLabelsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** Relationship between an epic and an issue */
export type EpicIssueNotesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** Relationship between an epic and an issue */
export type EpicIssueParticipantsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** Relationship between an epic and an issue */
export type EpicIssueReferenceArgs = {
    full?: Maybe<Scalars['Boolean']>;
};

/** The connection type for EpicIssue. */
export type EpicIssueConnection = {
    __typename?: 'EpicIssueConnection';
    /** Total count of collection */
    count: Scalars['Int'];
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<EpicIssueEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<EpicIssue>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type EpicIssueEdge = {
    __typename?: 'EpicIssueEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<EpicIssue>;
};

/** Check permissions for the current user on an epic */
export type EpicPermissions = {
    __typename?: 'EpicPermissions';
    /** Indicates the user can perform `admin_epic` on this resource */
    adminEpic: Scalars['Boolean'];
    /** Indicates the user can perform `award_emoji` on this resource */
    awardEmoji: Scalars['Boolean'];
    /** Indicates the user can perform `create_epic` on this resource */
    createEpic: Scalars['Boolean'];
    /** Indicates the user can perform `create_note` on this resource */
    createNote: Scalars['Boolean'];
    /** Indicates the user can perform `destroy_epic` on this resource */
    destroyEpic: Scalars['Boolean'];
    /** Indicates the user can perform `read_epic` on this resource */
    readEpic: Scalars['Boolean'];
    /** Indicates the user can perform `read_epic_iid` on this resource */
    readEpicIid: Scalars['Boolean'];
    /** Indicates the user can perform `update_epic` on this resource */
    updateEpic: Scalars['Boolean'];
};

/** Autogenerated return type of EpicSetSubscription */
export type EpicSetSubscriptionPayload = {
    __typename?: 'EpicSetSubscriptionPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The epic after mutation */
    epic?: Maybe<Epic>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** Autogenerated return type of EpicTreeReorder */
export type EpicTreeReorderPayload = {
    __typename?: 'EpicTreeReorderPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

export type GeoNode = {
    __typename?: 'GeoNode';
    /** The maximum concurrency of container repository sync for this secondary node */
    containerRepositoriesMaxCapacity?: Maybe<Scalars['Int']>;
    /** Indicates whether this Geo node is enabled */
    enabled?: Maybe<Scalars['Boolean']>;
    /** The maximum concurrency of LFS/attachment backfill for this secondary node */
    filesMaxCapacity?: Maybe<Scalars['Int']>;
    /** ID of this GeoNode */
    id: Scalars['ID'];
    /** The URL defined on the primary node that secondary nodes should use to contact it */
    internalUrl?: Maybe<Scalars['String']>;
    /** The interval (in days) in which the repository verification is valid. Once expired, it will be reverified */
    minimumReverificationInterval?: Maybe<Scalars['Int']>;
    /** The unique identifier for this Geo node */
    name?: Maybe<Scalars['String']>;
    /** Indicates whether this Geo node is the primary */
    primary?: Maybe<Scalars['Boolean']>;
    /** The maximum concurrency of repository backfill for this secondary node */
    reposMaxCapacity?: Maybe<Scalars['Int']>;
    /** The namespaces that should be synced, if `selective_sync_type` == `namespaces` */
    selectiveSyncNamespaces?: Maybe<NamespaceConnection>;
    /** The repository storages whose projects should be synced, if `selective_sync_type` == `shards` */
    selectiveSyncShards?: Maybe<Array<Scalars['String']>>;
    /** Indicates if syncing is limited to only specific groups, or shards */
    selectiveSyncType?: Maybe<Scalars['String']>;
    /** Indicates if this secondary node will replicate blobs in Object Storage */
    syncObjectStorage?: Maybe<Scalars['Boolean']>;
    /** The user-facing URL for this Geo node */
    url?: Maybe<Scalars['String']>;
    /** The maximum concurrency of repository verification for this secondary node */
    verificationMaxCapacity?: Maybe<Scalars['Int']>;
};


export type GeoNodeSelectiveSyncNamespacesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

export type GrafanaIntegration = {
    __typename?: 'GrafanaIntegration';
    /** Timestamp of the issue's creation */
    createdAt: Scalars['Time'];
    /** Indicates whether Grafana integration is enabled */
    enabled: Scalars['Boolean'];
    /** URL for the Grafana host for the Grafana integration */
    grafanaUrl: Scalars['String'];
    /** Internal ID of the Grafana integration */
    id: Scalars['ID'];
    /**
     * API token for the Grafana integration. Deprecated in 12.7: Plain text token has been masked for security reasons
     * @deprecated Plain text token has been masked for security reasons. Deprecated in 12.7
     */
    token: Scalars['String'];
    /** Timestamp of the issue's last activity */
    updatedAt: Scalars['Time'];
};

export type Group = {
    __typename?: 'Group';
    /** Indicates whether Auto DevOps is enabled for all projects within this group */
    autoDevopsEnabled?: Maybe<Scalars['Boolean']>;
    /** Avatar URL of the group */
    avatarUrl?: Maybe<Scalars['String']>;
    /** A single board of the group */
    board?: Maybe<Board>;
    /** Boards of the group */
    boards?: Maybe<BoardConnection>;
    /** Description of the namespace */
    description?: Maybe<Scalars['String']>;
    /** The GitLab Flavored Markdown rendering of `description` */
    descriptionHtml?: Maybe<Scalars['String']>;
    /** Indicates if a group has email notifications disabled */
    emailsDisabled?: Maybe<Scalars['Boolean']>;
    /** Find a single epic */
    epic?: Maybe<Epic>;
    /** Find epics */
    epics?: Maybe<EpicConnection>;
    /** Indicates if Epics are enabled for namespace */
    epicsEnabled?: Maybe<Scalars['Boolean']>;
    /** Full name of the namespace */
    fullName: Scalars['String'];
    /** Full path of the namespace */
    fullPath: Scalars['ID'];
    /** A membership of a user within this group */
    groupMembers?: Maybe<GroupMemberConnection>;
    /** Indicates if Group timelogs are enabled for namespace */
    groupTimelogsEnabled?: Maybe<Scalars['Boolean']>;
    /** ID of the namespace */
    id: Scalars['ID'];
    /** Status of the temporary storage increase */
    isTemporaryStorageIncreaseEnabled: Scalars['Boolean'];
    /** Issues of the group */
    issues?: Maybe<IssueConnection>;
    /** Find iterations */
    iterations?: Maybe<IterationConnection>;
    /** A label available on this group */
    label?: Maybe<Label>;
    /** Labels available on this group */
    labels?: Maybe<LabelConnection>;
    /** Indicates if Large File Storage (LFS) is enabled for namespace */
    lfsEnabled?: Maybe<Scalars['Boolean']>;
    /** Indicates if a group is disabled from getting mentioned */
    mentionsDisabled?: Maybe<Scalars['Boolean']>;
    /** Milestones of the group */
    milestones?: Maybe<MilestoneConnection>;
    /** Name of the namespace */
    name: Scalars['String'];
    /** Parent group */
    parent?: Maybe<Group>;
    /** Path of the namespace */
    path: Scalars['String'];
    /** The permission level required to create projects in the group */
    projectCreationLevel?: Maybe<Scalars['String']>;
    /** Projects within this namespace */
    projects: ProjectConnection;
    /** Indicates if users can request access to namespace */
    requestAccessEnabled?: Maybe<Scalars['Boolean']>;
    /** Indicates if all users in this group are required to set up two-factor authentication */
    requireTwoFactorAuthentication?: Maybe<Scalars['Boolean']>;
    /** Aggregated storage statistics of the namespace. Only available for root namespaces */
    rootStorageStatistics?: Maybe<RootStorageStatistics>;
    /** Indicates if sharing a project with another group within this group is prevented */
    shareWithGroupLock?: Maybe<Scalars['Boolean']>;
    /** Total storage limit of the root namespace in bytes */
    storageSizeLimit?: Maybe<Scalars['Float']>;
    /** The permission level required to create subgroups within the group */
    subgroupCreationLevel?: Maybe<Scalars['String']>;
    /** Date until the temporary storage increase is active */
    temporaryStorageIncreaseEndsOn?: Maybe<Scalars['Time']>;
    /** Time logged in issues by group members */
    timelogs: TimelogConnection;
    /** Time before two-factor authentication is enforced */
    twoFactorGracePeriod?: Maybe<Scalars['Int']>;
    /** Permissions for the current user on the resource */
    userPermissions: GroupPermissions;
    /** Visibility of the namespace */
    visibility?: Maybe<Scalars['String']>;
    /** Vulnerabilities reported on the projects in the group and its subgroups */
    vulnerabilities?: Maybe<VulnerabilityConnection>;
    /** Number of vulnerabilities per day for the projects in the group and its subgroups */
    vulnerabilitiesCountByDay?: Maybe<VulnerabilitiesCountByDayConnection>;
    /**
     * Number of vulnerabilities per severity level, per day, for the projects in the group and its subgroups. Deprecated in 13.3: Use `vulnerabilitiesCountByDay`
     * @deprecated Use `vulnerabilitiesCountByDay`. Deprecated in 13.3
     */
    vulnerabilitiesCountByDayAndSeverity?: Maybe<VulnerabilitiesCountByDayAndSeverityConnection>;
    /** Represents vulnerable project counts for each grade */
    vulnerabilityGrades: Array<VulnerableProjectsByGrade>;
    /** Vulnerability scanners reported on the project vulnerabilties of the group and its subgroups */
    vulnerabilityScanners?: Maybe<VulnerabilityScannerConnection>;
    /** Counts for each vulnerability severity in the group and its subgroups */
    vulnerabilitySeveritiesCount?: Maybe<VulnerabilitySeveritiesCount>;
    /** Web URL of the group */
    webUrl: Scalars['String'];
};


export type GroupBoardArgs = {
    id?: Maybe<Scalars['ID']>;
};


export type GroupBoardsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    id?: Maybe<Scalars['ID']>;
    last?: Maybe<Scalars['Int']>;
};


export type GroupEpicArgs = {
    authorUsername?: Maybe<Scalars['String']>;
    endDate?: Maybe<Scalars['Time']>;
    iid?: Maybe<Scalars['ID']>;
    iidStartsWith?: Maybe<Scalars['String']>;
    iids?: Maybe<Array<Scalars['ID']>>;
    labelName?: Maybe<Array<Scalars['String']>>;
    milestoneTitle?: Maybe<Scalars['String']>;
    search?: Maybe<Scalars['String']>;
    sort?: Maybe<EpicSort>;
    startDate?: Maybe<Scalars['Time']>;
    state?: Maybe<EpicState>;
};


export type GroupEpicsArgs = {
    after?: Maybe<Scalars['String']>;
    authorUsername?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    endDate?: Maybe<Scalars['Time']>;
    first?: Maybe<Scalars['Int']>;
    iid?: Maybe<Scalars['ID']>;
    iidStartsWith?: Maybe<Scalars['String']>;
    iids?: Maybe<Array<Scalars['ID']>>;
    labelName?: Maybe<Array<Scalars['String']>>;
    last?: Maybe<Scalars['Int']>;
    milestoneTitle?: Maybe<Scalars['String']>;
    search?: Maybe<Scalars['String']>;
    sort?: Maybe<EpicSort>;
    startDate?: Maybe<Scalars['Time']>;
    state?: Maybe<EpicState>;
};


export type GroupGroupMembersArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    search?: Maybe<Scalars['String']>;
};


export type GroupIssuesArgs = {
    after?: Maybe<Scalars['String']>;
    assigneeId?: Maybe<Scalars['String']>;
    assigneeUsername?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    closedAfter?: Maybe<Scalars['Time']>;
    closedBefore?: Maybe<Scalars['Time']>;
    createdAfter?: Maybe<Scalars['Time']>;
    createdBefore?: Maybe<Scalars['Time']>;
    first?: Maybe<Scalars['Int']>;
    iid?: Maybe<Scalars['String']>;
    iids?: Maybe<Array<Scalars['String']>>;
    includeSubgroups?: Maybe<Scalars['Boolean']>;
    iterationId?: Maybe<Array<Maybe<Scalars['ID']>>>;
    labelName?: Maybe<Array<Maybe<Scalars['String']>>>;
    last?: Maybe<Scalars['Int']>;
    milestoneTitle?: Maybe<Array<Maybe<Scalars['String']>>>;
    search?: Maybe<Scalars['String']>;
    sort?: Maybe<IssueSort>;
    state?: Maybe<IssuableState>;
    types?: Maybe<Array<IssueType>>;
    updatedAfter?: Maybe<Scalars['Time']>;
    updatedBefore?: Maybe<Scalars['Time']>;
};


export type GroupIterationsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    endDate?: Maybe<Scalars['Time']>;
    first?: Maybe<Scalars['Int']>;
    id?: Maybe<Scalars['ID']>;
    iid?: Maybe<Scalars['ID']>;
    includeAncestors?: Maybe<Scalars['Boolean']>;
    last?: Maybe<Scalars['Int']>;
    startDate?: Maybe<Scalars['Time']>;
    state?: Maybe<IterationState>;
    title?: Maybe<Scalars['String']>;
};


export type GroupLabelArgs = {
    title: Scalars['String'];
};


export type GroupLabelsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    searchTerm?: Maybe<Scalars['String']>;
};


export type GroupMilestonesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    endDate?: Maybe<Scalars['Time']>;
    first?: Maybe<Scalars['Int']>;
    ids?: Maybe<Array<Scalars['ID']>>;
    includeDescendants?: Maybe<Scalars['Boolean']>;
    last?: Maybe<Scalars['Int']>;
    startDate?: Maybe<Scalars['Time']>;
    state?: Maybe<MilestoneStateEnum>;
};


export type GroupProjectsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    hasVulnerabilities?: Maybe<Scalars['Boolean']>;
    includeSubgroups?: Maybe<Scalars['Boolean']>;
    last?: Maybe<Scalars['Int']>;
    search?: Maybe<Scalars['String']>;
    sort?: Maybe<NamespaceProjectSort>;
};


export type GroupTimelogsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    endDate?: Maybe<Scalars['Time']>;
    endTime?: Maybe<Scalars['Time']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    startDate?: Maybe<Scalars['Time']>;
    startTime?: Maybe<Scalars['Time']>;
};


export type GroupVulnerabilitiesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    hasIssues?: Maybe<Scalars['Boolean']>;
    hasResolution?: Maybe<Scalars['Boolean']>;
    last?: Maybe<Scalars['Int']>;
    projectId?: Maybe<Array<Scalars['ID']>>;
    reportType?: Maybe<Array<VulnerabilityReportType>>;
    scanner?: Maybe<Array<Scalars['String']>>;
    severity?: Maybe<Array<VulnerabilitySeverity>>;
    sort?: Maybe<VulnerabilitySort>;
    state?: Maybe<Array<VulnerabilityState>>;
};


export type GroupVulnerabilitiesCountByDayArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    endDate: Scalars['ISO8601Date'];
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    startDate: Scalars['ISO8601Date'];
};


export type GroupVulnerabilitiesCountByDayAndSeverityArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    endDate: Scalars['ISO8601Date'];
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    startDate: Scalars['ISO8601Date'];
};


export type GroupVulnerabilityScannersArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type GroupVulnerabilitySeveritiesCountArgs = {
    projectId?: Maybe<Array<Scalars['ID']>>;
    reportType?: Maybe<Array<VulnerabilityReportType>>;
    scanner?: Maybe<Array<Scalars['String']>>;
    severity?: Maybe<Array<VulnerabilitySeverity>>;
    state?: Maybe<Array<VulnerabilityState>>;
};

/** Represents a Group Membership */
export type GroupMember = MemberInterface & {
    __typename?: 'GroupMember';
    /** GitLab::Access level */
    accessLevel?: Maybe<AccessLevel>;
    /** Date and time the membership was created */
    createdAt?: Maybe<Scalars['Time']>;
    /** User that authorized membership */
    createdBy?: Maybe<User>;
    /** Date and time the membership expires */
    expiresAt?: Maybe<Scalars['Time']>;
    /** Group that a User is a member of */
    group?: Maybe<Group>;
    /** ID of the member */
    id: Scalars['ID'];
    /** Date and time the membership was last updated */
    updatedAt?: Maybe<Scalars['Time']>;
    /** User that is associated with the member object */
    user: User;
    /** Permissions for the current user on the resource */
    userPermissions: GroupPermissions;
};

/** The connection type for GroupMember. */
export type GroupMemberConnection = {
    __typename?: 'GroupMemberConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<GroupMemberEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<GroupMember>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type GroupMemberEdge = {
    __typename?: 'GroupMemberEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<GroupMember>;
};

export type GroupPermissions = {
    __typename?: 'GroupPermissions';
    /** Indicates the user can perform `read_group` on this resource */
    readGroup: Scalars['Boolean'];
};

export type InstanceSecurityDashboard = {
    __typename?: 'InstanceSecurityDashboard';
    /** Projects selected in Instance Security Dashboard */
    projects: ProjectConnection;
    /** Represents vulnerable project counts for each grade */
    vulnerabilityGrades: Array<VulnerableProjectsByGrade>;
    /** Vulnerability scanners reported on the vulnerabilties from projects selected in Instance Security Dashboard */
    vulnerabilityScanners?: Maybe<VulnerabilityScannerConnection>;
    /** Counts for each vulnerability severity from projects selected in Instance Security Dashboard */
    vulnerabilitySeveritiesCount?: Maybe<VulnerabilitySeveritiesCount>;
};


export type InstanceSecurityDashboardProjectsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type InstanceSecurityDashboardVulnerabilityScannersArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type InstanceSecurityDashboardVulnerabilitySeveritiesCountArgs = {
    projectId?: Maybe<Array<Scalars['ID']>>;
    reportType?: Maybe<Array<VulnerabilityReportType>>;
    scanner?: Maybe<Array<Scalars['String']>>;
    severity?: Maybe<Array<VulnerabilitySeverity>>;
    state?: Maybe<Array<VulnerabilityState>>;
};

/** Represents a recorded measurement (object count) for the Admins */
export type InstanceStatisticsMeasurement = {
    __typename?: 'InstanceStatisticsMeasurement';
    /** Object count */
    count: Scalars['Int'];
    /** The type of objects being measured */
    identifier: MeasurementIdentifier;
    /** The time the measurement was recorded */
    recordedAt?: Maybe<Scalars['Time']>;
};

/** The connection type for InstanceStatisticsMeasurement. */
export type InstanceStatisticsMeasurementConnection = {
    __typename?: 'InstanceStatisticsMeasurementConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<InstanceStatisticsMeasurementEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<InstanceStatisticsMeasurement>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type InstanceStatisticsMeasurementEdge = {
    __typename?: 'InstanceStatisticsMeasurementEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<InstanceStatisticsMeasurement>;
};

export type Issue = CurrentUserTodos & Noteable & {
    __typename?: 'Issue';
    /** Alert associated to this issue */
    alertManagementAlert?: Maybe<AlertManagementAlert>;
    /** Assignees of the issue */
    assignees?: Maybe<UserConnection>;
    /** User that created the issue */
    author: User;
    /** Indicates the issue is blocked */
    blocked: Scalars['Boolean'];
    /** Timestamp of when the issue was closed */
    closedAt?: Maybe<Scalars['Time']>;
    /** Indicates the issue is confidential */
    confidential: Scalars['Boolean'];
    /** Timestamp of when the issue was created */
    createdAt: Scalars['Time'];
    /** Todos for the current user */
    currentUserTodos: TodoConnection;
    /** Description of the issue */
    description?: Maybe<Scalars['String']>;
    /** The GitLab Flavored Markdown rendering of `description` */
    descriptionHtml?: Maybe<Scalars['String']>;
    /** Collection of design images associated with this issue */
    designCollection?: Maybe<DesignCollection>;
    /**
     * The designs associated with this issue. Deprecated in 12.2: Use `designCollection`
     * @deprecated Use `designCollection`. Deprecated in 12.2
     */
    designs?: Maybe<DesignCollection>;
    /** Indicates discussion is locked on the issue */
    discussionLocked: Scalars['Boolean'];
    /** All discussions on this noteable */
    discussions: DiscussionConnection;
    /** Number of downvotes the issue has received */
    downvotes: Scalars['Int'];
    /** Due date of the issue */
    dueDate?: Maybe<Scalars['Time']>;
    /** Epic to which this issue belongs */
    epic?: Maybe<Epic>;
    /** Current health status. Returns null if `save_issuable_health_status` feature flag is disabled. */
    healthStatus?: Maybe<HealthStatus>;
    /** ID of the issue */
    id: Scalars['ID'];
    /** Internal ID of the issue */
    iid: Scalars['ID'];
    /** Iteration of the issue */
    iteration?: Maybe<Iteration>;
    /** Labels of the issue */
    labels?: Maybe<LabelConnection>;
    /** Milestone of the issue */
    milestone?: Maybe<Milestone>;
    /** All notes on this noteable */
    notes: NoteConnection;
    /** List of participants in the issue */
    participants?: Maybe<UserConnection>;
    /** Internal reference of the issue. Returned in shortened format by default */
    reference: Scalars['String'];
    /** Relative position of the issue (used for positioning in epic tree and issue boards) */
    relativePosition?: Maybe<Scalars['Int']>;
    /** Severity level of the incident */
    severity?: Maybe<IssuableSeverity>;
    /** State of the issue */
    state: IssueState;
    /** Indicates whether an issue is published to the status page */
    statusPagePublishedIncident?: Maybe<Scalars['Boolean']>;
    /** Indicates the currently logged in user is subscribed to the issue */
    subscribed: Scalars['Boolean'];
    /** Task completion status of the issue */
    taskCompletionStatus: TaskCompletionStatus;
    /** Time estimate of the issue */
    timeEstimate: Scalars['Int'];
    /** Title of the issue */
    title: Scalars['String'];
    /** The GitLab Flavored Markdown rendering of `title` */
    titleHtml?: Maybe<Scalars['String']>;
    /** Total time reported as spent on the issue */
    totalTimeSpent: Scalars['Int'];
    /** Type of the issue */
    type?: Maybe<IssueType>;
    /** Timestamp of when the issue was last updated */
    updatedAt: Scalars['Time'];
    /** Number of upvotes the issue has received */
    upvotes: Scalars['Int'];
    /** Number of user notes of the issue */
    userNotesCount: Scalars['Int'];
    /** Permissions for the current user on the resource */
    userPermissions: IssuePermissions;
    /** Web path of the issue */
    webPath: Scalars['String'];
    /** Web URL of the issue */
    webUrl: Scalars['String'];
    /** Weight of the issue */
    weight?: Maybe<Scalars['Int']>;
};


export type IssueAssigneesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type IssueCurrentUserTodosArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    state?: Maybe<TodoStateEnum>;
};


export type IssueDiscussionsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type IssueLabelsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type IssueNotesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type IssueParticipantsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type IssueReferenceArgs = {
    full?: Maybe<Scalars['Boolean']>;
};

/** The connection type for Issue. */
export type IssueConnection = {
    __typename?: 'IssueConnection';
    /** Total count of collection */
    count: Scalars['Int'];
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<IssueEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Issue>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type IssueEdge = {
    __typename?: 'IssueEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Issue>;
};

/** Autogenerated return type of IssueMoveList */
export type IssueMoveListPayload = {
    __typename?: 'IssueMoveListPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The issue after mutation */
    issue?: Maybe<Issue>;
};

/** Check permissions for the current user on a issue */
export type IssuePermissions = {
    __typename?: 'IssuePermissions';
    /** Indicates the user can perform `admin_issue` on this resource */
    adminIssue: Scalars['Boolean'];
    /** Indicates the user can perform `create_design` on this resource */
    createDesign: Scalars['Boolean'];
    /** Indicates the user can perform `create_note` on this resource */
    createNote: Scalars['Boolean'];
    /** Indicates the user can perform `destroy_design` on this resource */
    destroyDesign: Scalars['Boolean'];
    /** Indicates the user can perform `read_design` on this resource */
    readDesign: Scalars['Boolean'];
    /** Indicates the user can perform `read_issue` on this resource */
    readIssue: Scalars['Boolean'];
    /** Indicates the user can perform `reopen_issue` on this resource */
    reopenIssue: Scalars['Boolean'];
    /** Indicates the user can perform `update_issue` on this resource */
    updateIssue: Scalars['Boolean'];
};

/** Autogenerated return type of IssueSetAssignees */
export type IssueSetAssigneesPayload = {
    __typename?: 'IssueSetAssigneesPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The issue after mutation */
    issue?: Maybe<Issue>;
};

/** Autogenerated return type of IssueSetConfidential */
export type IssueSetConfidentialPayload = {
    __typename?: 'IssueSetConfidentialPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The issue after mutation */
    issue?: Maybe<Issue>;
};

/** Autogenerated return type of IssueSetDueDate */
export type IssueSetDueDatePayload = {
    __typename?: 'IssueSetDueDatePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The issue after mutation */
    issue?: Maybe<Issue>;
};

/** Autogenerated return type of IssueSetEpic */
export type IssueSetEpicPayload = {
    __typename?: 'IssueSetEpicPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The issue after mutation */
    issue?: Maybe<Issue>;
};

/** Autogenerated return type of IssueSetIteration */
export type IssueSetIterationPayload = {
    __typename?: 'IssueSetIterationPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The issue after mutation */
    issue?: Maybe<Issue>;
};

/** Autogenerated return type of IssueSetLocked */
export type IssueSetLockedPayload = {
    __typename?: 'IssueSetLockedPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The issue after mutation */
    issue?: Maybe<Issue>;
};

/** Autogenerated return type of IssueSetSeverity */
export type IssueSetSeverityPayload = {
    __typename?: 'IssueSetSeverityPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The issue after mutation */
    issue?: Maybe<Issue>;
};

/** Autogenerated return type of IssueSetSubscription */
export type IssueSetSubscriptionPayload = {
    __typename?: 'IssueSetSubscriptionPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The issue after mutation */
    issue?: Maybe<Issue>;
};

/** Autogenerated return type of IssueSetWeight */
export type IssueSetWeightPayload = {
    __typename?: 'IssueSetWeightPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The issue after mutation */
    issue?: Maybe<Issue>;
};

/** Represents total number of issues for the represented statuses */
export type IssueStatusCountsType = {
    __typename?: 'IssueStatusCountsType';
    /** Number of issues with status ALL for the project */
    all?: Maybe<Scalars['Int']>;
    /** Number of issues with status CLOSED for the project */
    closed?: Maybe<Scalars['Int']>;
    /** Number of issues with status OPENED for the project */
    opened?: Maybe<Scalars['Int']>;
};

/** Represents an iteration object */
export type Iteration = TimeboxBurnupTimeSeriesInterface & {
    __typename?: 'Iteration';
    /** Daily scope and completed totals for burnup charts */
    burnupTimeSeries?: Maybe<Array<BurnupChartDailyTotals>>;
    /** Timestamp of iteration creation */
    createdAt: Scalars['Time'];
    /** Description of the iteration */
    description?: Maybe<Scalars['String']>;
    /** The GitLab Flavored Markdown rendering of `description` */
    descriptionHtml?: Maybe<Scalars['String']>;
    /** Timestamp of the iteration due date */
    dueDate?: Maybe<Scalars['Time']>;
    /** ID of the iteration */
    id: Scalars['ID'];
    /** Internal ID of the iteration */
    iid: Scalars['ID'];
    /** Web path of the iteration, scoped to the query parent. Only valid for Project parents. Returns null in other contexts */
    scopedPath?: Maybe<Scalars['String']>;
    /** Web URL of the iteration, scoped to the query parent. Only valid for Project parents. Returns null in other contexts */
    scopedUrl?: Maybe<Scalars['String']>;
    /** Timestamp of the iteration start date */
    startDate?: Maybe<Scalars['Time']>;
    /** State of the iteration */
    state: IterationState;
    /** Title of the iteration */
    title: Scalars['String'];
    /** Timestamp of last iteration update */
    updatedAt: Scalars['Time'];
    /** Web path of the iteration */
    webPath: Scalars['String'];
    /** Web URL of the iteration */
    webUrl: Scalars['String'];
};

/** The connection type for Iteration. */
export type IterationConnection = {
    __typename?: 'IterationConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<IterationEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Iteration>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type IterationEdge = {
    __typename?: 'IterationEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Iteration>;
};

export type JiraImport = {
    __typename?: 'JiraImport';
    /** Timestamp of when the Jira import was created */
    createdAt?: Maybe<Scalars['Time']>;
    /** Count of issues that failed to import */
    failedToImportCount: Scalars['Int'];
    /** Count of issues that were successfully imported */
    importedIssuesCount: Scalars['Int'];
    /** Project key for the imported Jira project */
    jiraProjectKey: Scalars['String'];
    /** Timestamp of when the Jira import was scheduled */
    scheduledAt?: Maybe<Scalars['Time']>;
    /** User that started the Jira import */
    scheduledBy?: Maybe<User>;
    /** Total count of issues that were attempted to import */
    totalIssueCount: Scalars['Int'];
};

/** The connection type for JiraImport. */
export type JiraImportConnection = {
    __typename?: 'JiraImportConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<JiraImportEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<JiraImport>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type JiraImportEdge = {
    __typename?: 'JiraImportEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<JiraImport>;
};

/** Autogenerated return type of JiraImportStart */
export type JiraImportStartPayload = {
    __typename?: 'JiraImportStartPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The Jira import data after mutation */
    jiraImport?: Maybe<JiraImport>;
};

/** Autogenerated return type of JiraImportUsers */
export type JiraImportUsersPayload = {
    __typename?: 'JiraImportUsersPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** Users returned from Jira, matched by email and name if possible. */
    jiraUsers?: Maybe<Array<JiraUser>>;
};

export type JiraProject = {
    __typename?: 'JiraProject';
    /** Key of the Jira project */
    key: Scalars['String'];
    /** Name of the Jira project */
    name?: Maybe<Scalars['String']>;
    /** ID of the Jira project */
    projectId: Scalars['Int'];
};

/** The connection type for JiraProject. */
export type JiraProjectConnection = {
    __typename?: 'JiraProjectConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<JiraProjectEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<JiraProject>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type JiraProjectEdge = {
    __typename?: 'JiraProjectEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<JiraProject>;
};

export type JiraService = Service & {
    __typename?: 'JiraService';
    /** Indicates if the service is active */
    active?: Maybe<Scalars['Boolean']>;
    /** List of all Jira projects fetched through Jira REST API */
    projects?: Maybe<JiraProjectConnection>;
    /** Class name of the service */
    type?: Maybe<Scalars['String']>;
};


export type JiraServiceProjectsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    name?: Maybe<Scalars['String']>;
};

export type JiraUser = {
    __typename?: 'JiraUser';
    /** ID of the matched GitLab user */
    gitlabId?: Maybe<Scalars['Int']>;
    /** Name of the matched GitLab user */
    gitlabName?: Maybe<Scalars['String']>;
    /** Username of the matched GitLab user */
    gitlabUsername?: Maybe<Scalars['String']>;
    /** Account ID of the Jira user */
    jiraAccountId: Scalars['String'];
    /** Display name of the Jira user */
    jiraDisplayName: Scalars['String'];
    /** Email of the Jira user, returned only for users with public emails */
    jiraEmail?: Maybe<Scalars['String']>;
};

export type Label = {
    __typename?: 'Label';
    /** Background color of the label */
    color: Scalars['String'];
    /** Description of the label (Markdown rendered as HTML for caching) */
    description?: Maybe<Scalars['String']>;
    /** The GitLab Flavored Markdown rendering of `description` */
    descriptionHtml?: Maybe<Scalars['String']>;
    /** Label ID */
    id: Scalars['ID'];
    /** Text color of the label */
    textColor: Scalars['String'];
    /** Content of the label */
    title: Scalars['String'];
};

/** The connection type for Label. */
export type LabelConnection = {
    __typename?: 'LabelConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<LabelEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Label>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type LabelEdge = {
    __typename?: 'LabelEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Label>;
};

/** Autogenerated return type of MarkAsSpamSnippet */
export type MarkAsSpamSnippetPayload = {
    __typename?: 'MarkAsSpamSnippetPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The snippet after mutation */
    snippet?: Maybe<Snippet>;
};

/** The connection type for MemberInterface. */
export type MemberInterfaceConnection = {
    __typename?: 'MemberInterfaceConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<MemberInterfaceEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<MemberInterface>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type MemberInterfaceEdge = {
    __typename?: 'MemberInterfaceEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<MemberInterface>;
};

export type MergeRequest = CurrentUserTodos & Noteable & {
    __typename?: 'MergeRequest';
    /** Indicates if members of the target project can push to the fork */
    allowCollaboration?: Maybe<Scalars['Boolean']>;
    /** Number of approvals left */
    approvalsLeft?: Maybe<Scalars['Int']>;
    /** Number of approvals required */
    approvalsRequired?: Maybe<Scalars['Int']>;
    /** Indicates if the merge request has all the required approvals. Returns true if no required approvals are configured. */
    approved: Scalars['Boolean'];
    /** Users who approved the merge request */
    approvedBy?: Maybe<UserConnection>;
    /** Assignees of the merge request */
    assignees?: Maybe<UserConnection>;
    /** User who created this merge request */
    author?: Maybe<User>;
    /** Indicates if auto merge is enabled for the merge request */
    autoMergeEnabled: Scalars['Boolean'];
    /** Number of commits in the merge request */
    commitCount?: Maybe<Scalars['Int']>;
    /** Indicates if the merge request has conflicts */
    conflicts: Scalars['Boolean'];
    /** Timestamp of when the merge request was created */
    createdAt: Scalars['Time'];
    /** Todos for the current user */
    currentUserTodos: TodoConnection;
    /** Default merge commit message of the merge request */
    defaultMergeCommitMessage?: Maybe<Scalars['String']>;
    /** Description of the merge request (Markdown rendered as HTML for caching) */
    description?: Maybe<Scalars['String']>;
    /** The GitLab Flavored Markdown rendering of `description` */
    descriptionHtml?: Maybe<Scalars['String']>;
    /** Diff head SHA of the merge request */
    diffHeadSha?: Maybe<Scalars['String']>;
    /** References of the base SHA, the head SHA, and the start SHA for this merge request */
    diffRefs?: Maybe<DiffRefs>;
    /** Details about which files were changed in this merge request */
    diffStats?: Maybe<Array<DiffStats>>;
    /** Summary of which files were changed in this merge request */
    diffStatsSummary?: Maybe<DiffStatsSummary>;
    /** Indicates if comments on the merge request are locked to members only */
    discussionLocked: Scalars['Boolean'];
    /** All discussions on this noteable */
    discussions: DiscussionConnection;
    /** Number of downvotes for the merge request */
    downvotes: Scalars['Int'];
    /** Indicates if the project settings will lead to source branch deletion after merge */
    forceRemoveSourceBranch?: Maybe<Scalars['Boolean']>;
    /** The pipeline running on the branch HEAD of the merge request */
    headPipeline?: Maybe<Pipeline>;
    /** ID of the merge request */
    id: Scalars['ID'];
    /** Internal ID of the merge request */
    iid: Scalars['String'];
    /** Commit SHA of the merge request if merge is in progress */
    inProgressMergeCommitSha?: Maybe<Scalars['String']>;
    /** Labels of the merge request */
    labels?: Maybe<LabelConnection>;
    /**
     * Default merge commit message of the merge request. Deprecated in 11.8: Use `defaultMergeCommitMessage`
     * @deprecated Use `defaultMergeCommitMessage`. Deprecated in 11.8
     */
    mergeCommitMessage?: Maybe<Scalars['String']>;
    /** SHA of the merge request commit (set once merged) */
    mergeCommitSha?: Maybe<Scalars['String']>;
    /** Error message due to a merge error */
    mergeError?: Maybe<Scalars['String']>;
    /** Indicates if a merge is currently occurring */
    mergeOngoing: Scalars['Boolean'];
    /** Status of the merge request */
    mergeStatus?: Maybe<Scalars['String']>;
    /** Indicates if the merge has been set to be merged when its pipeline succeeds (MWPS) */
    mergeWhenPipelineSucceeds?: Maybe<Scalars['Boolean']>;
    /** Indicates if all discussions in the merge request have been resolved, allowing the merge request to be merged */
    mergeableDiscussionsState?: Maybe<Scalars['Boolean']>;
    /** Timestamp of when the merge request was merged, null if not merged */
    mergedAt?: Maybe<Scalars['Time']>;
    /** The milestone of the merge request */
    milestone?: Maybe<Milestone>;
    /** All notes on this noteable */
    notes: NoteConnection;
    /** Participants in the merge request */
    participants?: Maybe<UserConnection>;
    /** Pipelines for the merge request */
    pipelines?: Maybe<PipelineConnection>;
    /** Alias for target_project */
    project: Project;
    /** ID of the merge request project */
    projectId: Scalars['Int'];
    /** Rebase commit SHA of the merge request */
    rebaseCommitSha?: Maybe<Scalars['String']>;
    /** Indicates if there is a rebase currently in progress for the merge request */
    rebaseInProgress: Scalars['Boolean'];
    /** Internal reference of the merge request. Returned in shortened format by default */
    reference: Scalars['String'];
    /** Indicates if the merge request will be rebased */
    shouldBeRebased: Scalars['Boolean'];
    /** Indicates if the source branch of the merge request will be deleted after merge */
    shouldRemoveSourceBranch?: Maybe<Scalars['Boolean']>;
    /** Source branch of the merge request */
    sourceBranch: Scalars['String'];
    /** Indicates if the source branch of the merge request exists */
    sourceBranchExists: Scalars['Boolean'];
    /** Source project of the merge request */
    sourceProject?: Maybe<Project>;
    /** ID of the merge request source project */
    sourceProjectId?: Maybe<Scalars['Int']>;
    /** State of the merge request */
    state: MergeRequestState;
    /** Indicates if the currently logged in user is subscribed to this merge request */
    subscribed: Scalars['Boolean'];
    /** Target branch of the merge request */
    targetBranch: Scalars['String'];
    /** Indicates if the target branch of the merge request exists */
    targetBranchExists: Scalars['Boolean'];
    /** Target project of the merge request */
    targetProject: Project;
    /** ID of the merge request target project */
    targetProjectId: Scalars['Int'];
    /** Completion status of tasks */
    taskCompletionStatus: TaskCompletionStatus;
    /** Time estimate of the merge request */
    timeEstimate: Scalars['Int'];
    /** Title of the merge request */
    title: Scalars['String'];
    /** The GitLab Flavored Markdown rendering of `title` */
    titleHtml?: Maybe<Scalars['String']>;
    /** Total time reported as spent on the merge request */
    totalTimeSpent: Scalars['Int'];
    /** Timestamp of when the merge request was last updated */
    updatedAt: Scalars['Time'];
    /** Number of upvotes for the merge request */
    upvotes: Scalars['Int'];
    /** User notes count of the merge request */
    userNotesCount?: Maybe<Scalars['Int']>;
    /** Permissions for the current user on the resource */
    userPermissions: MergeRequestPermissions;
    /** Web URL of the merge request */
    webUrl?: Maybe<Scalars['String']>;
    /** Indicates if the merge request is a work in progress (WIP) */
    workInProgress: Scalars['Boolean'];
};


export type MergeRequestApprovedByArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type MergeRequestAssigneesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type MergeRequestCurrentUserTodosArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    state?: Maybe<TodoStateEnum>;
};


export type MergeRequestDiffStatsArgs = {
    path?: Maybe<Scalars['String']>;
};


export type MergeRequestDiscussionsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type MergeRequestLabelsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type MergeRequestNotesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type MergeRequestParticipantsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type MergeRequestPipelinesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    ref?: Maybe<Scalars['String']>;
    sha?: Maybe<Scalars['String']>;
    status?: Maybe<PipelineStatusEnum>;
};


export type MergeRequestReferenceArgs = {
    full?: Maybe<Scalars['Boolean']>;
};

/** The connection type for MergeRequest. */
export type MergeRequestConnection = {
    __typename?: 'MergeRequestConnection';
    /** Total count of collection */
    count: Scalars['Int'];
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<MergeRequestEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<MergeRequest>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** Autogenerated return type of MergeRequestCreate */
export type MergeRequestCreatePayload = {
    __typename?: 'MergeRequestCreatePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The merge request after mutation */
    mergeRequest?: Maybe<MergeRequest>;
};

/** An edge in a connection. */
export type MergeRequestEdge = {
    __typename?: 'MergeRequestEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<MergeRequest>;
};

/** Check permissions for the current user on a merge request */
export type MergeRequestPermissions = {
    __typename?: 'MergeRequestPermissions';
    /** Indicates the user can perform `admin_merge_request` on this resource */
    adminMergeRequest: Scalars['Boolean'];
    /** Indicates the user can perform `can_merge` on this resource */
    canMerge: Scalars['Boolean'];
    /** Indicates the user can perform `cherry_pick_on_current_merge_request` on this resource */
    cherryPickOnCurrentMergeRequest: Scalars['Boolean'];
    /** Indicates the user can perform `create_note` on this resource */
    createNote: Scalars['Boolean'];
    /** Indicates the user can perform `push_to_source_branch` on this resource */
    pushToSourceBranch: Scalars['Boolean'];
    /** Indicates the user can perform `read_merge_request` on this resource */
    readMergeRequest: Scalars['Boolean'];
    /** Indicates the user can perform `remove_source_branch` on this resource */
    removeSourceBranch: Scalars['Boolean'];
    /** Indicates the user can perform `revert_on_current_merge_request` on this resource */
    revertOnCurrentMergeRequest: Scalars['Boolean'];
    /** Indicates the user can perform `update_merge_request` on this resource */
    updateMergeRequest: Scalars['Boolean'];
};

/** Autogenerated return type of MergeRequestSetAssignees */
export type MergeRequestSetAssigneesPayload = {
    __typename?: 'MergeRequestSetAssigneesPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The merge request after mutation */
    mergeRequest?: Maybe<MergeRequest>;
};

/** Autogenerated return type of MergeRequestSetLabels */
export type MergeRequestSetLabelsPayload = {
    __typename?: 'MergeRequestSetLabelsPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The merge request after mutation */
    mergeRequest?: Maybe<MergeRequest>;
};

/** Autogenerated return type of MergeRequestSetLocked */
export type MergeRequestSetLockedPayload = {
    __typename?: 'MergeRequestSetLockedPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The merge request after mutation */
    mergeRequest?: Maybe<MergeRequest>;
};

/** Autogenerated return type of MergeRequestSetMilestone */
export type MergeRequestSetMilestonePayload = {
    __typename?: 'MergeRequestSetMilestonePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The merge request after mutation */
    mergeRequest?: Maybe<MergeRequest>;
};

/** Autogenerated return type of MergeRequestSetSubscription */
export type MergeRequestSetSubscriptionPayload = {
    __typename?: 'MergeRequestSetSubscriptionPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The merge request after mutation */
    mergeRequest?: Maybe<MergeRequest>;
};

/** Autogenerated return type of MergeRequestSetWip */
export type MergeRequestSetWipPayload = {
    __typename?: 'MergeRequestSetWipPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The merge request after mutation */
    mergeRequest?: Maybe<MergeRequest>;
};

/** Autogenerated return type of MergeRequestUpdate */
export type MergeRequestUpdatePayload = {
    __typename?: 'MergeRequestUpdatePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The merge request after mutation */
    mergeRequest?: Maybe<MergeRequest>;
};

export type Metadata = {
    __typename?: 'Metadata';
    /** Revision */
    revision: Scalars['String'];
    /** Version */
    version: Scalars['String'];
};

export type MetricsDashboard = {
    __typename?: 'MetricsDashboard';
    /** Annotations added to the dashboard */
    annotations?: Maybe<MetricsDashboardAnnotationConnection>;
    /** Path to a file with the dashboard definition */
    path?: Maybe<Scalars['String']>;
    /** Dashboard schema validation warnings */
    schemaValidationWarnings?: Maybe<Array<Scalars['String']>>;
};


export type MetricsDashboardAnnotationsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    from: Scalars['Time'];
    last?: Maybe<Scalars['Int']>;
    to?: Maybe<Scalars['Time']>;
};

export type MetricsDashboardAnnotation = {
    __typename?: 'MetricsDashboardAnnotation';
    /** Description of the annotation */
    description?: Maybe<Scalars['String']>;
    /** Timestamp marking end of annotated time span */
    endingAt?: Maybe<Scalars['Time']>;
    /** ID of the annotation */
    id: Scalars['ID'];
    /** ID of a dashboard panel to which the annotation should be scoped */
    panelId?: Maybe<Scalars['String']>;
    /** Timestamp marking start of annotated time span */
    startingAt?: Maybe<Scalars['Time']>;
};

/** The connection type for MetricsDashboardAnnotation. */
export type MetricsDashboardAnnotationConnection = {
    __typename?: 'MetricsDashboardAnnotationConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<MetricsDashboardAnnotationEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<MetricsDashboardAnnotation>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type MetricsDashboardAnnotationEdge = {
    __typename?: 'MetricsDashboardAnnotationEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<MetricsDashboardAnnotation>;
};

/** Represents a milestone */
export type Milestone = TimeboxBurnupTimeSeriesInterface & {
    __typename?: 'Milestone';
    /** Daily scope and completed totals for burnup charts */
    burnupTimeSeries?: Maybe<Array<BurnupChartDailyTotals>>;
    /** Timestamp of milestone creation */
    createdAt: Scalars['Time'];
    /** Description of the milestone */
    description?: Maybe<Scalars['String']>;
    /** Timestamp of the milestone due date */
    dueDate?: Maybe<Scalars['Time']>;
    /** Indicates if milestone is at group level */
    groupMilestone: Scalars['Boolean'];
    /** ID of the milestone */
    id: Scalars['ID'];
    /** Indicates if milestone is at project level */
    projectMilestone: Scalars['Boolean'];
    /** Timestamp of the milestone start date */
    startDate?: Maybe<Scalars['Time']>;
    /** State of the milestone */
    state: MilestoneStateEnum;
    /** Milestone statistics */
    stats?: Maybe<MilestoneStats>;
    /** Indicates if milestone is at subgroup level */
    subgroupMilestone: Scalars['Boolean'];
    /** Title of the milestone */
    title: Scalars['String'];
    /** Timestamp of last milestone update */
    updatedAt: Scalars['Time'];
    /** Web path of the milestone */
    webPath: Scalars['String'];
};

/** The connection type for Milestone. */
export type MilestoneConnection = {
    __typename?: 'MilestoneConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<MilestoneEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Milestone>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type MilestoneEdge = {
    __typename?: 'MilestoneEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Milestone>;
};

/** Contains statistics about a milestone */
export type MilestoneStats = {
    __typename?: 'MilestoneStats';
    /** Number of closed issues associated with the milestone */
    closedIssuesCount?: Maybe<Scalars['Int']>;
    /** Total number of issues associated with the milestone */
    totalIssuesCount?: Maybe<Scalars['Int']>;
};

export type Mutation = {
    __typename?: 'Mutation';
    /** @deprecated Use awardEmojiAdd. Deprecated in 13.2 */
    addAwardEmoji?: Maybe<AddAwardEmojiPayload>;
    addProjectToSecurityDashboard?: Maybe<AddProjectToSecurityDashboardPayload>;
    adminSidekiqQueuesDeleteJobs?: Maybe<AdminSidekiqQueuesDeleteJobsPayload>;
    alertSetAssignees?: Maybe<AlertSetAssigneesPayload>;
    alertTodoCreate?: Maybe<AlertTodoCreatePayload>;
    awardEmojiAdd?: Maybe<AwardEmojiAddPayload>;
    awardEmojiRemove?: Maybe<AwardEmojiRemovePayload>;
    awardEmojiToggle?: Maybe<AwardEmojiTogglePayload>;
    boardListCreate?: Maybe<BoardListCreatePayload>;
    boardListUpdateLimitMetrics?: Maybe<BoardListUpdateLimitMetricsPayload>;
    clusterAgentDelete?: Maybe<ClusterAgentDeletePayload>;
    clusterAgentTokenCreate?: Maybe<ClusterAgentTokenCreatePayload>;
    clusterAgentTokenDelete?: Maybe<ClusterAgentTokenDeletePayload>;
    commitCreate?: Maybe<CommitCreatePayload>;
    configureSast?: Maybe<ConfigureSastPayload>;
    createAlertIssue?: Maybe<CreateAlertIssuePayload>;
    createAnnotation?: Maybe<CreateAnnotationPayload>;
    createBranch?: Maybe<CreateBranchPayload>;
    createClusterAgent?: Maybe<CreateClusterAgentPayload>;
    createDiffNote?: Maybe<CreateDiffNotePayload>;
    createEpic?: Maybe<CreateEpicPayload>;
    createImageDiffNote?: Maybe<CreateImageDiffNotePayload>;
    createIteration?: Maybe<CreateIterationPayload>;
    createNote?: Maybe<CreateNotePayload>;
    createRequirement?: Maybe<CreateRequirementPayload>;
    createSnippet?: Maybe<CreateSnippetPayload>;
    createTestCase?: Maybe<CreateTestCasePayload>;
    dastOnDemandScanCreate?: Maybe<DastOnDemandScanCreatePayload>;
    dastScannerProfileCreate?: Maybe<DastScannerProfileCreatePayload>;
    dastScannerProfileDelete?: Maybe<DastScannerProfileDeletePayload>;
    dastScannerProfileUpdate?: Maybe<DastScannerProfileUpdatePayload>;
    dastSiteProfileCreate?: Maybe<DastSiteProfileCreatePayload>;
    dastSiteProfileDelete?: Maybe<DastSiteProfileDeletePayload>;
    dastSiteProfileUpdate?: Maybe<DastSiteProfileUpdatePayload>;
    deleteAnnotation?: Maybe<DeleteAnnotationPayload>;
    designManagementDelete?: Maybe<DesignManagementDeletePayload>;
    designManagementMove?: Maybe<DesignManagementMovePayload>;
    designManagementUpload?: Maybe<DesignManagementUploadPayload>;
    destroyBoard?: Maybe<DestroyBoardPayload>;
    destroyNote?: Maybe<DestroyNotePayload>;
    destroySnippet?: Maybe<DestroySnippetPayload>;
    /** Toggles the resolved state of a discussion */
    discussionToggleResolve?: Maybe<DiscussionToggleResolvePayload>;
    dismissVulnerability?: Maybe<DismissVulnerabilityPayload>;
    epicAddIssue?: Maybe<EpicAddIssuePayload>;
    epicSetSubscription?: Maybe<EpicSetSubscriptionPayload>;
    epicTreeReorder?: Maybe<EpicTreeReorderPayload>;
    issueMoveList?: Maybe<IssueMoveListPayload>;
    issueSetAssignees?: Maybe<IssueSetAssigneesPayload>;
    issueSetConfidential?: Maybe<IssueSetConfidentialPayload>;
    issueSetDueDate?: Maybe<IssueSetDueDatePayload>;
    issueSetEpic?: Maybe<IssueSetEpicPayload>;
    issueSetIteration?: Maybe<IssueSetIterationPayload>;
    issueSetLocked?: Maybe<IssueSetLockedPayload>;
    issueSetSeverity?: Maybe<IssueSetSeverityPayload>;
    issueSetSubscription?: Maybe<IssueSetSubscriptionPayload>;
    issueSetWeight?: Maybe<IssueSetWeightPayload>;
    jiraImportStart?: Maybe<JiraImportStartPayload>;
    jiraImportUsers?: Maybe<JiraImportUsersPayload>;
    markAsSpamSnippet?: Maybe<MarkAsSpamSnippetPayload>;
    mergeRequestCreate?: Maybe<MergeRequestCreatePayload>;
    mergeRequestSetAssignees?: Maybe<MergeRequestSetAssigneesPayload>;
    mergeRequestSetLabels?: Maybe<MergeRequestSetLabelsPayload>;
    mergeRequestSetLocked?: Maybe<MergeRequestSetLockedPayload>;
    mergeRequestSetMilestone?: Maybe<MergeRequestSetMilestonePayload>;
    mergeRequestSetSubscription?: Maybe<MergeRequestSetSubscriptionPayload>;
    mergeRequestSetWip?: Maybe<MergeRequestSetWipPayload>;
    /** Update attributes of a merge request */
    mergeRequestUpdate?: Maybe<MergeRequestUpdatePayload>;
    namespaceIncreaseStorageTemporarily?: Maybe<NamespaceIncreaseStorageTemporarilyPayload>;
    pipelineCancel?: Maybe<PipelineCancelPayload>;
    pipelineDestroy?: Maybe<PipelineDestroyPayload>;
    pipelineRetry?: Maybe<PipelineRetryPayload>;
    /** @deprecated Use awardEmojiRemove. Deprecated in 13.2 */
    removeAwardEmoji?: Maybe<RemoveAwardEmojiPayload>;
    removeProjectFromSecurityDashboard?: Maybe<RemoveProjectFromSecurityDashboardPayload>;
    /** @deprecated Use DastOnDemandScanCreate. Deprecated in 13.4 */
    runDastScan?: Maybe<RunDastScanPayload>;
    todoMarkDone?: Maybe<TodoMarkDonePayload>;
    todoRestore?: Maybe<TodoRestorePayload>;
    todoRestoreMany?: Maybe<TodoRestoreManyPayload>;
    todosMarkAllDone?: Maybe<TodosMarkAllDonePayload>;
    /** @deprecated Use awardEmojiToggle. Deprecated in 13.2 */
    toggleAwardEmoji?: Maybe<ToggleAwardEmojiPayload>;
    updateAlertStatus?: Maybe<UpdateAlertStatusPayload>;
    updateBoard?: Maybe<UpdateBoardPayload>;
    updateBoardList?: Maybe<UpdateBoardListPayload>;
    updateContainerExpirationPolicy?: Maybe<UpdateContainerExpirationPolicyPayload>;
    updateEpic?: Maybe<UpdateEpicPayload>;
    /** Updates a DiffNote on an image (a `Note` where the `position.positionType` is `"image"`). If the body of the Note contains only quick actions, the Note will be destroyed during the update, and no Note will be returned */
    updateImageDiffNote?: Maybe<UpdateImageDiffNotePayload>;
    updateIssue?: Maybe<UpdateIssuePayload>;
    updateIteration?: Maybe<UpdateIterationPayload>;
    /** Updates a Note. If the body of the Note contains only quick actions, the Note will be destroyed during the update, and no Note will be returned */
    updateNote?: Maybe<UpdateNotePayload>;
    updateRequirement?: Maybe<UpdateRequirementPayload>;
    updateSnippet?: Maybe<UpdateSnippetPayload>;
    vulnerabilityResolve?: Maybe<VulnerabilityResolvePayload>;
};


export type MutationAddAwardEmojiArgs = {
    input: AddAwardEmojiInput;
};


export type MutationAddProjectToSecurityDashboardArgs = {
    input: AddProjectToSecurityDashboardInput;
};


export type MutationAdminSidekiqQueuesDeleteJobsArgs = {
    input: AdminSidekiqQueuesDeleteJobsInput;
};


export type MutationAlertSetAssigneesArgs = {
    input: AlertSetAssigneesInput;
};


export type MutationAlertTodoCreateArgs = {
    input: AlertTodoCreateInput;
};


export type MutationAwardEmojiAddArgs = {
    input: AwardEmojiAddInput;
};


export type MutationAwardEmojiRemoveArgs = {
    input: AwardEmojiRemoveInput;
};


export type MutationAwardEmojiToggleArgs = {
    input: AwardEmojiToggleInput;
};


export type MutationBoardListCreateArgs = {
    input: BoardListCreateInput;
};


export type MutationBoardListUpdateLimitMetricsArgs = {
    input: BoardListUpdateLimitMetricsInput;
};


export type MutationClusterAgentDeleteArgs = {
    input: ClusterAgentDeleteInput;
};


export type MutationClusterAgentTokenCreateArgs = {
    input: ClusterAgentTokenCreateInput;
};


export type MutationClusterAgentTokenDeleteArgs = {
    input: ClusterAgentTokenDeleteInput;
};


export type MutationCommitCreateArgs = {
    input: CommitCreateInput;
};


export type MutationConfigureSastArgs = {
    input: ConfigureSastInput;
};


export type MutationCreateAlertIssueArgs = {
    input: CreateAlertIssueInput;
};


export type MutationCreateAnnotationArgs = {
    input: CreateAnnotationInput;
};


export type MutationCreateBranchArgs = {
    input: CreateBranchInput;
};


export type MutationCreateClusterAgentArgs = {
    input: CreateClusterAgentInput;
};


export type MutationCreateDiffNoteArgs = {
    input: CreateDiffNoteInput;
};


export type MutationCreateEpicArgs = {
    input: CreateEpicInput;
};


export type MutationCreateImageDiffNoteArgs = {
    input: CreateImageDiffNoteInput;
};


export type MutationCreateIterationArgs = {
    input: CreateIterationInput;
};


export type MutationCreateNoteArgs = {
    input: CreateNoteInput;
};


export type MutationCreateRequirementArgs = {
    input: CreateRequirementInput;
};


export type MutationCreateSnippetArgs = {
    input: CreateSnippetInput;
};


export type MutationCreateTestCaseArgs = {
    input: CreateTestCaseInput;
};


export type MutationDastOnDemandScanCreateArgs = {
    input: DastOnDemandScanCreateInput;
};


export type MutationDastScannerProfileCreateArgs = {
    input: DastScannerProfileCreateInput;
};


export type MutationDastScannerProfileDeleteArgs = {
    input: DastScannerProfileDeleteInput;
};


export type MutationDastScannerProfileUpdateArgs = {
    input: DastScannerProfileUpdateInput;
};


export type MutationDastSiteProfileCreateArgs = {
    input: DastSiteProfileCreateInput;
};


export type MutationDastSiteProfileDeleteArgs = {
    input: DastSiteProfileDeleteInput;
};


export type MutationDastSiteProfileUpdateArgs = {
    input: DastSiteProfileUpdateInput;
};


export type MutationDeleteAnnotationArgs = {
    input: DeleteAnnotationInput;
};


export type MutationDesignManagementDeleteArgs = {
    input: DesignManagementDeleteInput;
};


export type MutationDesignManagementMoveArgs = {
    input: DesignManagementMoveInput;
};


export type MutationDesignManagementUploadArgs = {
    input: DesignManagementUploadInput;
};


export type MutationDestroyBoardArgs = {
    input: DestroyBoardInput;
};


export type MutationDestroyNoteArgs = {
    input: DestroyNoteInput;
};


export type MutationDestroySnippetArgs = {
    input: DestroySnippetInput;
};


export type MutationDiscussionToggleResolveArgs = {
    input: DiscussionToggleResolveInput;
};


export type MutationDismissVulnerabilityArgs = {
    input: DismissVulnerabilityInput;
};


export type MutationEpicAddIssueArgs = {
    input: EpicAddIssueInput;
};


export type MutationEpicSetSubscriptionArgs = {
    input: EpicSetSubscriptionInput;
};


export type MutationEpicTreeReorderArgs = {
    input: EpicTreeReorderInput;
};


export type MutationIssueMoveListArgs = {
    input: IssueMoveListInput;
};


export type MutationIssueSetAssigneesArgs = {
    input: IssueSetAssigneesInput;
};


export type MutationIssueSetConfidentialArgs = {
    input: IssueSetConfidentialInput;
};


export type MutationIssueSetDueDateArgs = {
    input: IssueSetDueDateInput;
};


export type MutationIssueSetEpicArgs = {
    input: IssueSetEpicInput;
};


export type MutationIssueSetIterationArgs = {
    input: IssueSetIterationInput;
};


export type MutationIssueSetLockedArgs = {
    input: IssueSetLockedInput;
};


export type MutationIssueSetSeverityArgs = {
    input: IssueSetSeverityInput;
};


export type MutationIssueSetSubscriptionArgs = {
    input: IssueSetSubscriptionInput;
};


export type MutationIssueSetWeightArgs = {
    input: IssueSetWeightInput;
};


export type MutationJiraImportStartArgs = {
    input: JiraImportStartInput;
};


export type MutationJiraImportUsersArgs = {
    input: JiraImportUsersInput;
};


export type MutationMarkAsSpamSnippetArgs = {
    input: MarkAsSpamSnippetInput;
};


export type MutationMergeRequestCreateArgs = {
    input: MergeRequestCreateInput;
};


export type MutationMergeRequestSetAssigneesArgs = {
    input: MergeRequestSetAssigneesInput;
};


export type MutationMergeRequestSetLabelsArgs = {
    input: MergeRequestSetLabelsInput;
};


export type MutationMergeRequestSetLockedArgs = {
    input: MergeRequestSetLockedInput;
};


export type MutationMergeRequestSetMilestoneArgs = {
    input: MergeRequestSetMilestoneInput;
};


export type MutationMergeRequestSetSubscriptionArgs = {
    input: MergeRequestSetSubscriptionInput;
};


export type MutationMergeRequestSetWipArgs = {
    input: MergeRequestSetWipInput;
};


export type MutationMergeRequestUpdateArgs = {
    input: MergeRequestUpdateInput;
};


export type MutationNamespaceIncreaseStorageTemporarilyArgs = {
    input: NamespaceIncreaseStorageTemporarilyInput;
};


export type MutationPipelineCancelArgs = {
    input: PipelineCancelInput;
};


export type MutationPipelineDestroyArgs = {
    input: PipelineDestroyInput;
};


export type MutationPipelineRetryArgs = {
    input: PipelineRetryInput;
};


export type MutationRemoveAwardEmojiArgs = {
    input: RemoveAwardEmojiInput;
};


export type MutationRemoveProjectFromSecurityDashboardArgs = {
    input: RemoveProjectFromSecurityDashboardInput;
};


export type MutationRunDastScanArgs = {
    input: RunDastScanInput;
};


export type MutationTodoMarkDoneArgs = {
    input: TodoMarkDoneInput;
};


export type MutationTodoRestoreArgs = {
    input: TodoRestoreInput;
};


export type MutationTodoRestoreManyArgs = {
    input: TodoRestoreManyInput;
};


export type MutationTodosMarkAllDoneArgs = {
    input: TodosMarkAllDoneInput;
};


export type MutationToggleAwardEmojiArgs = {
    input: ToggleAwardEmojiInput;
};


export type MutationUpdateAlertStatusArgs = {
    input: UpdateAlertStatusInput;
};


export type MutationUpdateBoardArgs = {
    input: UpdateBoardInput;
};


export type MutationUpdateBoardListArgs = {
    input: UpdateBoardListInput;
};


export type MutationUpdateContainerExpirationPolicyArgs = {
    input: UpdateContainerExpirationPolicyInput;
};


export type MutationUpdateEpicArgs = {
    input: UpdateEpicInput;
};


export type MutationUpdateImageDiffNoteArgs = {
    input: UpdateImageDiffNoteInput;
};


export type MutationUpdateIssueArgs = {
    input: UpdateIssueInput;
};


export type MutationUpdateIterationArgs = {
    input: UpdateIterationInput;
};


export type MutationUpdateNoteArgs = {
    input: UpdateNoteInput;
};


export type MutationUpdateRequirementArgs = {
    input: UpdateRequirementInput;
};


export type MutationUpdateSnippetArgs = {
    input: UpdateSnippetInput;
};


export type MutationVulnerabilityResolveArgs = {
    input: VulnerabilityResolveInput;
};

export type Namespace = {
    __typename?: 'Namespace';
    /** Description of the namespace */
    description?: Maybe<Scalars['String']>;
    /** The GitLab Flavored Markdown rendering of `description` */
    descriptionHtml?: Maybe<Scalars['String']>;
    /** Full name of the namespace */
    fullName: Scalars['String'];
    /** Full path of the namespace */
    fullPath: Scalars['ID'];
    /** ID of the namespace */
    id: Scalars['ID'];
    /** Status of the temporary storage increase */
    isTemporaryStorageIncreaseEnabled: Scalars['Boolean'];
    /** Indicates if Large File Storage (LFS) is enabled for namespace */
    lfsEnabled?: Maybe<Scalars['Boolean']>;
    /** Name of the namespace */
    name: Scalars['String'];
    /** Path of the namespace */
    path: Scalars['String'];
    /** Projects within this namespace */
    projects: ProjectConnection;
    /** Indicates if users can request access to namespace */
    requestAccessEnabled?: Maybe<Scalars['Boolean']>;
    /** Aggregated storage statistics of the namespace. Only available for root namespaces */
    rootStorageStatistics?: Maybe<RootStorageStatistics>;
    /** Total storage limit of the root namespace in bytes */
    storageSizeLimit?: Maybe<Scalars['Float']>;
    /** Date until the temporary storage increase is active */
    temporaryStorageIncreaseEndsOn?: Maybe<Scalars['Time']>;
    /** Visibility of the namespace */
    visibility?: Maybe<Scalars['String']>;
};


export type NamespaceProjectsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    hasVulnerabilities?: Maybe<Scalars['Boolean']>;
    includeSubgroups?: Maybe<Scalars['Boolean']>;
    last?: Maybe<Scalars['Int']>;
    search?: Maybe<Scalars['String']>;
    sort?: Maybe<NamespaceProjectSort>;
};

/** The connection type for Namespace. */
export type NamespaceConnection = {
    __typename?: 'NamespaceConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<NamespaceEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Namespace>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type NamespaceEdge = {
    __typename?: 'NamespaceEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Namespace>;
};

/** Autogenerated return type of NamespaceIncreaseStorageTemporarily */
export type NamespaceIncreaseStorageTemporarilyPayload = {
    __typename?: 'NamespaceIncreaseStorageTemporarilyPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The namespace after mutation */
    namespace?: Maybe<Namespace>;
};

export type Note = ResolvableInterface & {
    __typename?: 'Note';
    /** User who wrote this note */
    author: User;
    /** Content of the note */
    body: Scalars['String'];
    /** The GitLab Flavored Markdown rendering of `note` */
    bodyHtml?: Maybe<Scalars['String']>;
    /** Indicates if this note is confidential */
    confidential?: Maybe<Scalars['Boolean']>;
    /** Timestamp of the note creation */
    createdAt: Scalars['Time'];
    /** The discussion this note is a part of */
    discussion?: Maybe<Discussion>;
    /** ID of the note */
    id: Scalars['ID'];
    /** The position of this note on a diff */
    position?: Maybe<DiffPosition>;
    /** Project associated with the note */
    project?: Maybe<Project>;
    /** Indicates if the object can be resolved */
    resolvable: Scalars['Boolean'];
    /** Indicates if the object is resolved */
    resolved: Scalars['Boolean'];
    /** Timestamp of when the object was resolved */
    resolvedAt?: Maybe<Scalars['Time']>;
    /** User who resolved the object */
    resolvedBy?: Maybe<User>;
    /** Indicates whether this note was created by the system or by a user */
    system: Scalars['Boolean'];
    /** Name of the icon corresponding to a system note */
    systemNoteIconName?: Maybe<Scalars['String']>;
    /** Timestamp of the note's last activity */
    updatedAt: Scalars['Time'];
    /** Permissions for the current user on the resource */
    userPermissions: NotePermissions;
};

/** The connection type for Note. */
export type NoteConnection = {
    __typename?: 'NoteConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<NoteEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Note>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type NoteEdge = {
    __typename?: 'NoteEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Note>;
};

export type NotePermissions = {
    __typename?: 'NotePermissions';
    /** Indicates the user can perform `admin_note` on this resource */
    adminNote: Scalars['Boolean'];
    /** Indicates the user can perform `award_emoji` on this resource */
    awardEmoji: Scalars['Boolean'];
    /** Indicates the user can perform `create_note` on this resource */
    createNote: Scalars['Boolean'];
    /** Indicates the user can perform `read_note` on this resource */
    readNote: Scalars['Boolean'];
    /** Indicates the user can perform `resolve_note` on this resource */
    resolveNote: Scalars['Boolean'];
};

/** Represents a package */
export type Package = {
    __typename?: 'Package';
    /** The created date */
    createdAt: Scalars['Time'];
    /** The ID of the package */
    id: Scalars['ID'];
    /** The name of the package */
    name: Scalars['String'];
    /** The type of the package */
    packageType: PackageTypeEnum;
    /** The update date */
    updatedAt: Scalars['Time'];
    /** The version of the package */
    version?: Maybe<Scalars['String']>;
};

/** The connection type for Package. */
export type PackageConnection = {
    __typename?: 'PackageConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<PackageEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Package>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type PackageEdge = {
    __typename?: 'PackageEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Package>;
};

/** Information about pagination in a connection. */
export type PageInfo = {
    __typename?: 'PageInfo';
    /** When paginating forwards, the cursor to continue. */
    endCursor?: Maybe<Scalars['String']>;
    /** When paginating forwards, are there more items? */
    hasNextPage: Scalars['Boolean'];
    /** When paginating backwards, are there more items? */
    hasPreviousPage: Scalars['Boolean'];
    /** When paginating backwards, the cursor to continue. */
    startCursor?: Maybe<Scalars['String']>;
};

export type Pipeline = {
    __typename?: 'Pipeline';
    /** Base SHA of the source branch */
    beforeSha?: Maybe<Scalars['String']>;
    /** Specifies if a pipeline can be canceled */
    cancelable: Scalars['Boolean'];
    /** Timestamp of the pipeline's commit */
    committedAt?: Maybe<Scalars['Time']>;
    /** Config source of the pipeline (UNKNOWN_SOURCE, REPOSITORY_SOURCE, AUTO_DEVOPS_SOURCE, WEBIDE_SOURCE, REMOTE_SOURCE, EXTERNAL_PROJECT_SOURCE, BRIDGE_SOURCE, PARAMETER_SOURCE) */
    configSource?: Maybe<PipelineConfigSourceEnum>;
    /** Coverage percentage */
    coverage?: Maybe<Scalars['Float']>;
    /** Timestamp of the pipeline's creation */
    createdAt: Scalars['Time'];
    /** Detailed status of the pipeline */
    detailedStatus: DetailedStatus;
    /** Duration of the pipeline in seconds */
    duration?: Maybe<Scalars['Int']>;
    /** Timestamp of the pipeline's completion */
    finishedAt?: Maybe<Scalars['Time']>;
    /** ID of the pipeline */
    id: Scalars['ID'];
    /** Internal ID of the pipeline */
    iid: Scalars['String'];
    /** Specifies if a pipeline can be retried */
    retryable: Scalars['Boolean'];
    /** Vulnerability and scanned resource counts for each security scanner of the pipeline */
    securityReportSummary?: Maybe<SecurityReportSummary>;
    /** SHA of the pipeline's commit */
    sha: Scalars['String'];
    /** Stages of the pipeline */
    stages?: Maybe<CiStageConnection>;
    /** Timestamp when the pipeline was started */
    startedAt?: Maybe<Scalars['Time']>;
    /** Status of the pipeline (CREATED, WAITING_FOR_RESOURCE, PREPARING, PENDING, RUNNING, FAILED, SUCCESS, CANCELED, SKIPPED, MANUAL, SCHEDULED) */
    status: PipelineStatusEnum;
    /** Timestamp of the pipeline's last activity */
    updatedAt: Scalars['Time'];
    /** Pipeline user */
    user?: Maybe<User>;
    /** Permissions for the current user on the resource */
    userPermissions: PipelinePermissions;
};


export type PipelineStagesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** Autogenerated return type of PipelineCancel */
export type PipelineCancelPayload = {
    __typename?: 'PipelineCancelPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** The connection type for Pipeline. */
export type PipelineConnection = {
    __typename?: 'PipelineConnection';
    /** Total count of collection */
    count: Scalars['Int'];
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<PipelineEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Pipeline>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** Autogenerated return type of PipelineDestroy */
export type PipelineDestroyPayload = {
    __typename?: 'PipelineDestroyPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** An edge in a connection. */
export type PipelineEdge = {
    __typename?: 'PipelineEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Pipeline>;
};

export type PipelinePermissions = {
    __typename?: 'PipelinePermissions';
    /** Indicates the user can perform `admin_pipeline` on this resource */
    adminPipeline: Scalars['Boolean'];
    /** Indicates the user can perform `destroy_pipeline` on this resource */
    destroyPipeline: Scalars['Boolean'];
    /** Indicates the user can perform `update_pipeline` on this resource */
    updatePipeline: Scalars['Boolean'];
};

/** Autogenerated return type of PipelineRetry */
export type PipelineRetryPayload = {
    __typename?: 'PipelineRetryPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The pipeline after mutation */
    pipeline?: Maybe<Pipeline>;
};

export type Project = {
    __typename?: 'Project';
    /** A single Alert Management alert of the project */
    alertManagementAlert?: Maybe<AlertManagementAlert>;
    /** Counts of alerts by status for the project */
    alertManagementAlertStatusCounts?: Maybe<AlertManagementAlertStatusCountsType>;
    /** Alert Management alerts of the project */
    alertManagementAlerts?: Maybe<AlertManagementAlertConnection>;
    /** If `only_allow_merge_if_pipeline_succeeds` is true, indicates if merge requests of the project can also be merged with skipped jobs */
    allowMergeOnSkippedPipeline?: Maybe<Scalars['Boolean']>;
    /** Indicates the archived status of the project */
    archived?: Maybe<Scalars['Boolean']>;
    /** Indicates if issues referenced by merge requests and commits within the default branch are closed automatically */
    autocloseReferencedIssues?: Maybe<Scalars['Boolean']>;
    /** URL to avatar image file of the project */
    avatarUrl?: Maybe<Scalars['String']>;
    /** A single board of the project */
    board?: Maybe<Board>;
    /** Boards of the project */
    boards?: Maybe<BoardConnection>;
    /** Find a single cluster agent by name */
    clusterAgent?: Maybe<ClusterAgent>;
    /** Cluster agents associated with the project */
    clusterAgents?: Maybe<ClusterAgentConnection>;
    /** Compliance frameworks associated with the project */
    complianceFrameworks?: Maybe<ComplianceFrameworkConnection>;
    /** The container expiration policy of the project */
    containerExpirationPolicy?: Maybe<ContainerExpirationPolicy>;
    /** Indicates if the project stores Docker container images in a container registry */
    containerRegistryEnabled?: Maybe<Scalars['Boolean']>;
    /** Timestamp of the project creation */
    createdAt?: Maybe<Scalars['Time']>;
    /** The DAST scanner profiles associated with the project */
    dastScannerProfiles?: Maybe<DastScannerProfileConnection>;
    /** DAST Site Profile associated with the project */
    dastSiteProfile?: Maybe<DastSiteProfile>;
    /** DAST Site Profiles associated with the project */
    dastSiteProfiles?: Maybe<DastSiteProfileConnection>;
    /** Short description of the project */
    description?: Maybe<Scalars['String']>;
    /** The GitLab Flavored Markdown rendering of `description` */
    descriptionHtml?: Maybe<Scalars['String']>;
    /** A single environment of the project */
    environment?: Maybe<Environment>;
    /** Environments of the project */
    environments?: Maybe<EnvironmentConnection>;
    /** Number of times the project has been forked */
    forksCount: Scalars['Int'];
    /** Full path of the project */
    fullPath: Scalars['ID'];
    /** Grafana integration details for the project */
    grafanaIntegration?: Maybe<GrafanaIntegration>;
    /** Group of the project */
    group?: Maybe<Group>;
    /** URL to connect to the project via HTTPS */
    httpUrlToRepo?: Maybe<Scalars['String']>;
    /** ID of the project */
    id: Scalars['ID'];
    /** Status of import background job of the project */
    importStatus?: Maybe<Scalars['String']>;
    /** A single issue of the project */
    issue?: Maybe<Issue>;
    /** Counts of issues by status for the project */
    issueStatusCounts?: Maybe<IssueStatusCountsType>;
    /** Issues of the project */
    issues?: Maybe<IssueConnection>;
    /** Indicates if Issues are enabled for the current user */
    issuesEnabled?: Maybe<Scalars['Boolean']>;
    /** Find iterations */
    iterations?: Maybe<IterationConnection>;
    /** Status of Jira import background job of the project */
    jiraImportStatus?: Maybe<Scalars['String']>;
    /** Jira imports into the project */
    jiraImports?: Maybe<JiraImportConnection>;
    /** Indicates if CI/CD pipeline jobs are enabled for the current user */
    jobsEnabled?: Maybe<Scalars['Boolean']>;
    /** A label available on this project */
    label?: Maybe<Label>;
    /** Labels available on this project */
    labels?: Maybe<LabelConnection>;
    /** Timestamp of the project last activity */
    lastActivityAt?: Maybe<Scalars['Time']>;
    /** Indicates if the project has Large File Storage (LFS) enabled */
    lfsEnabled?: Maybe<Scalars['Boolean']>;
    /** A single merge request of the project */
    mergeRequest?: Maybe<MergeRequest>;
    /** Merge requests of the project */
    mergeRequests?: Maybe<MergeRequestConnection>;
    /** Indicates if Merge Requests are enabled for the current user */
    mergeRequestsEnabled?: Maybe<Scalars['Boolean']>;
    /** Indicates if no merge commits should be created and all merges should instead be fast-forwarded, which means that merging is only allowed if the branch could be fast-forwarded. */
    mergeRequestsFfOnlyEnabled?: Maybe<Scalars['Boolean']>;
    /** Milestones of the project */
    milestones?: Maybe<MilestoneConnection>;
    /** Name of the project (without namespace) */
    name: Scalars['String'];
    /** Full name of the project with its namespace */
    nameWithNamespace: Scalars['String'];
    /** Namespace of the project */
    namespace?: Maybe<Namespace>;
    /** Indicates if merge requests of the project can only be merged when all the discussions are resolved */
    onlyAllowMergeIfAllDiscussionsAreResolved?: Maybe<Scalars['Boolean']>;
    /** Indicates if merge requests of the project can only be merged with successful jobs */
    onlyAllowMergeIfPipelineSucceeds?: Maybe<Scalars['Boolean']>;
    /** Number of open issues for the project */
    openIssuesCount?: Maybe<Scalars['Int']>;
    /** Packages of the project */
    packages?: Maybe<PackageConnection>;
    /** Path of the project */
    path: Scalars['String'];
    /** Build pipeline of the project */
    pipeline?: Maybe<Pipeline>;
    /** Build pipelines of the project */
    pipelines?: Maybe<PipelineConnection>;
    /** Indicates if a link to create or view a merge request should display after a push to Git repositories of the project from the command line */
    printingMergeRequestLinkEnabled?: Maybe<Scalars['Boolean']>;
    /** Members of the project */
    projectMembers?: Maybe<MemberInterfaceConnection>;
    /** Indicates if there is public access to pipelines and job details of the project, including output logs and artifacts */
    publicJobs?: Maybe<Scalars['Boolean']>;
    /** A single release of the project */
    release?: Maybe<Release>;
    /** Releases of the project */
    releases?: Maybe<ReleaseConnection>;
    /** Indicates if `Delete source branch` option should be enabled by default for all new merge requests of the project */
    removeSourceBranchAfterMerge?: Maybe<Scalars['Boolean']>;
    /** Git repository of the project */
    repository?: Maybe<Repository>;
    /** Indicates if users can request member access to the project */
    requestAccessEnabled?: Maybe<Scalars['Boolean']>;
    /** Find a single requirement. Available only when feature flag `requirements_management` is enabled. */
    requirement?: Maybe<Requirement>;
    /** Number of requirements for the project by their state */
    requirementStatesCount?: Maybe<RequirementStatesCount>;
    /** Find requirements. Available only when feature flag `requirements_management` is enabled. */
    requirements?: Maybe<RequirementConnection>;
    /** SAST CI configuration for the project */
    sastCiConfiguration?: Maybe<SastCiConfiguration>;
    /** Path to project's security dashboard */
    securityDashboardPath?: Maybe<Scalars['String']>;
    /** Information about security analyzers used in the project */
    securityScanners?: Maybe<SecurityScanners>;
    /** Detailed version of a Sentry error on the project */
    sentryDetailedError?: Maybe<SentryDetailedError>;
    /** Paginated collection of Sentry errors on the project */
    sentryErrors?: Maybe<SentryErrorCollection>;
    /** E-mail address of the service desk. */
    serviceDeskAddress?: Maybe<Scalars['String']>;
    /** Indicates if the project has service desk enabled. */
    serviceDeskEnabled?: Maybe<Scalars['Boolean']>;
    /** Project services */
    services?: Maybe<ServiceConnection>;
    /** Indicates if shared runners are enabled for the project */
    sharedRunnersEnabled?: Maybe<Scalars['Boolean']>;
    /** Snippets of the project */
    snippets?: Maybe<SnippetConnection>;
    /** Indicates if Snippets are enabled for the current user */
    snippetsEnabled?: Maybe<Scalars['Boolean']>;
    /** URL to connect to the project via SSH */
    sshUrlToRepo?: Maybe<Scalars['String']>;
    /** Number of times the project has been starred */
    starCount: Scalars['Int'];
    /** Statistics of the project */
    statistics?: Maybe<ProjectStatistics>;
    /** The commit message used to apply merge request suggestions */
    suggestionCommitMessage?: Maybe<Scalars['String']>;
    /** List of project topics (not Git tags) */
    tagList?: Maybe<Scalars['String']>;
    /** Permissions for the current user on the resource */
    userPermissions: ProjectPermissions;
    /** Visibility of the project */
    visibility?: Maybe<Scalars['String']>;
    /** Vulnerabilities reported on the project */
    vulnerabilities?: Maybe<VulnerabilityConnection>;
    /** Number of vulnerabilities per day for the project */
    vulnerabilitiesCountByDay?: Maybe<VulnerabilitiesCountByDayConnection>;
    /** Vulnerability scanners reported on the project vulnerabilties */
    vulnerabilityScanners?: Maybe<VulnerabilityScannerConnection>;
    /** Counts for each vulnerability severity in the project */
    vulnerabilitySeveritiesCount?: Maybe<VulnerabilitySeveritiesCount>;
    /** Web URL of the project */
    webUrl?: Maybe<Scalars['String']>;
    /** Indicates if Wikis are enabled for the current user */
    wikiEnabled?: Maybe<Scalars['Boolean']>;
};


export type ProjectAlertManagementAlertArgs = {
    iid?: Maybe<Scalars['String']>;
    search?: Maybe<Scalars['String']>;
    sort?: Maybe<AlertManagementAlertSort>;
    statuses?: Maybe<Array<AlertManagementStatus>>;
};


export type ProjectAlertManagementAlertStatusCountsArgs = {
    search?: Maybe<Scalars['String']>;
};


export type ProjectAlertManagementAlertsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    iid?: Maybe<Scalars['String']>;
    last?: Maybe<Scalars['Int']>;
    search?: Maybe<Scalars['String']>;
    sort?: Maybe<AlertManagementAlertSort>;
    statuses?: Maybe<Array<AlertManagementStatus>>;
};


export type ProjectBoardArgs = {
    id?: Maybe<Scalars['ID']>;
};


export type ProjectBoardsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    id?: Maybe<Scalars['ID']>;
    last?: Maybe<Scalars['Int']>;
};


export type ProjectClusterAgentArgs = {
    name: Scalars['String'];
};


export type ProjectClusterAgentsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type ProjectComplianceFrameworksArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type ProjectDastScannerProfilesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type ProjectDastSiteProfileArgs = {
    id: Scalars['DastSiteProfileID'];
};


export type ProjectDastSiteProfilesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type ProjectEnvironmentArgs = {
    name?: Maybe<Scalars['String']>;
    search?: Maybe<Scalars['String']>;
    states?: Maybe<Array<Scalars['String']>>;
};


export type ProjectEnvironmentsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    name?: Maybe<Scalars['String']>;
    search?: Maybe<Scalars['String']>;
    states?: Maybe<Array<Scalars['String']>>;
};


export type ProjectIssueArgs = {
    assigneeId?: Maybe<Scalars['String']>;
    assigneeUsername?: Maybe<Scalars['String']>;
    closedAfter?: Maybe<Scalars['Time']>;
    closedBefore?: Maybe<Scalars['Time']>;
    createdAfter?: Maybe<Scalars['Time']>;
    createdBefore?: Maybe<Scalars['Time']>;
    iid?: Maybe<Scalars['String']>;
    iids?: Maybe<Array<Scalars['String']>>;
    iterationId?: Maybe<Array<Maybe<Scalars['ID']>>>;
    labelName?: Maybe<Array<Maybe<Scalars['String']>>>;
    milestoneTitle?: Maybe<Array<Maybe<Scalars['String']>>>;
    search?: Maybe<Scalars['String']>;
    sort?: Maybe<IssueSort>;
    state?: Maybe<IssuableState>;
    types?: Maybe<Array<IssueType>>;
    updatedAfter?: Maybe<Scalars['Time']>;
    updatedBefore?: Maybe<Scalars['Time']>;
};


export type ProjectIssueStatusCountsArgs = {
    assigneeId?: Maybe<Scalars['String']>;
    assigneeUsername?: Maybe<Scalars['String']>;
    closedAfter?: Maybe<Scalars['Time']>;
    closedBefore?: Maybe<Scalars['Time']>;
    createdAfter?: Maybe<Scalars['Time']>;
    createdBefore?: Maybe<Scalars['Time']>;
    iid?: Maybe<Scalars['String']>;
    iids?: Maybe<Array<Scalars['String']>>;
    labelName?: Maybe<Array<Maybe<Scalars['String']>>>;
    milestoneTitle?: Maybe<Array<Maybe<Scalars['String']>>>;
    search?: Maybe<Scalars['String']>;
    types?: Maybe<Array<IssueType>>;
    updatedAfter?: Maybe<Scalars['Time']>;
    updatedBefore?: Maybe<Scalars['Time']>;
};


export type ProjectIssuesArgs = {
    after?: Maybe<Scalars['String']>;
    assigneeId?: Maybe<Scalars['String']>;
    assigneeUsername?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    closedAfter?: Maybe<Scalars['Time']>;
    closedBefore?: Maybe<Scalars['Time']>;
    createdAfter?: Maybe<Scalars['Time']>;
    createdBefore?: Maybe<Scalars['Time']>;
    first?: Maybe<Scalars['Int']>;
    iid?: Maybe<Scalars['String']>;
    iids?: Maybe<Array<Scalars['String']>>;
    iterationId?: Maybe<Array<Maybe<Scalars['ID']>>>;
    labelName?: Maybe<Array<Maybe<Scalars['String']>>>;
    last?: Maybe<Scalars['Int']>;
    milestoneTitle?: Maybe<Array<Maybe<Scalars['String']>>>;
    search?: Maybe<Scalars['String']>;
    sort?: Maybe<IssueSort>;
    state?: Maybe<IssuableState>;
    types?: Maybe<Array<IssueType>>;
    updatedAfter?: Maybe<Scalars['Time']>;
    updatedBefore?: Maybe<Scalars['Time']>;
};


export type ProjectIterationsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    endDate?: Maybe<Scalars['Time']>;
    first?: Maybe<Scalars['Int']>;
    id?: Maybe<Scalars['ID']>;
    iid?: Maybe<Scalars['ID']>;
    includeAncestors?: Maybe<Scalars['Boolean']>;
    last?: Maybe<Scalars['Int']>;
    startDate?: Maybe<Scalars['Time']>;
    state?: Maybe<IterationState>;
    title?: Maybe<Scalars['String']>;
};


export type ProjectJiraImportsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type ProjectLabelArgs = {
    title: Scalars['String'];
};


export type ProjectLabelsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    searchTerm?: Maybe<Scalars['String']>;
};


export type ProjectMergeRequestArgs = {
    iid: Scalars['String'];
};


export type ProjectMergeRequestsArgs = {
    after?: Maybe<Scalars['String']>;
    assigneeUsername?: Maybe<Scalars['String']>;
    authorUsername?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    iids?: Maybe<Array<Scalars['String']>>;
    labels?: Maybe<Array<Scalars['String']>>;
    last?: Maybe<Scalars['Int']>;
    mergedAfter?: Maybe<Scalars['Time']>;
    mergedBefore?: Maybe<Scalars['Time']>;
    milestoneTitle?: Maybe<Scalars['String']>;
    sort?: Maybe<MergeRequestSort>;
    sourceBranches?: Maybe<Array<Scalars['String']>>;
    state?: Maybe<MergeRequestState>;
    targetBranches?: Maybe<Array<Scalars['String']>>;
};


export type ProjectMilestonesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    endDate?: Maybe<Scalars['Time']>;
    first?: Maybe<Scalars['Int']>;
    ids?: Maybe<Array<Scalars['ID']>>;
    includeAncestors?: Maybe<Scalars['Boolean']>;
    last?: Maybe<Scalars['Int']>;
    startDate?: Maybe<Scalars['Time']>;
    state?: Maybe<MilestoneStateEnum>;
};


export type ProjectPackagesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type ProjectPipelineArgs = {
    iid: Scalars['ID'];
};


export type ProjectPipelinesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    ref?: Maybe<Scalars['String']>;
    sha?: Maybe<Scalars['String']>;
    status?: Maybe<PipelineStatusEnum>;
};


export type ProjectProjectMembersArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    search?: Maybe<Scalars['String']>;
};


export type ProjectReleaseArgs = {
    tagName: Scalars['String'];
};


export type ProjectReleasesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type ProjectRequirementArgs = {
    authorUsername?: Maybe<Array<Scalars['String']>>;
    iid?: Maybe<Scalars['ID']>;
    iids?: Maybe<Array<Scalars['ID']>>;
    search?: Maybe<Scalars['String']>;
    sort?: Maybe<Sort>;
    state?: Maybe<RequirementState>;
};


export type ProjectRequirementsArgs = {
    after?: Maybe<Scalars['String']>;
    authorUsername?: Maybe<Array<Scalars['String']>>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    iid?: Maybe<Scalars['ID']>;
    iids?: Maybe<Array<Scalars['ID']>>;
    last?: Maybe<Scalars['Int']>;
    search?: Maybe<Scalars['String']>;
    sort?: Maybe<Sort>;
    state?: Maybe<RequirementState>;
};


export type ProjectSentryDetailedErrorArgs = {
    id: Scalars['ID'];
};


export type ProjectServicesArgs = {
    active?: Maybe<Scalars['Boolean']>;
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    type?: Maybe<ServiceType>;
};


export type ProjectSnippetsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    ids?: Maybe<Array<Scalars['ID']>>;
    last?: Maybe<Scalars['Int']>;
    visibility?: Maybe<VisibilityScopesEnum>;
};


export type ProjectVulnerabilitiesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    hasIssues?: Maybe<Scalars['Boolean']>;
    hasResolution?: Maybe<Scalars['Boolean']>;
    last?: Maybe<Scalars['Int']>;
    projectId?: Maybe<Array<Scalars['ID']>>;
    reportType?: Maybe<Array<VulnerabilityReportType>>;
    scanner?: Maybe<Array<Scalars['String']>>;
    severity?: Maybe<Array<VulnerabilitySeverity>>;
    sort?: Maybe<VulnerabilitySort>;
    state?: Maybe<Array<VulnerabilityState>>;
};


export type ProjectVulnerabilitiesCountByDayArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    endDate: Scalars['ISO8601Date'];
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    startDate: Scalars['ISO8601Date'];
};


export type ProjectVulnerabilityScannersArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type ProjectVulnerabilitySeveritiesCountArgs = {
    projectId?: Maybe<Array<Scalars['ID']>>;
    reportType?: Maybe<Array<VulnerabilityReportType>>;
    scanner?: Maybe<Array<Scalars['String']>>;
    severity?: Maybe<Array<VulnerabilitySeverity>>;
    state?: Maybe<Array<VulnerabilityState>>;
};

/** The connection type for Project. */
export type ProjectConnection = {
    __typename?: 'ProjectConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<ProjectEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Project>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type ProjectEdge = {
    __typename?: 'ProjectEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Project>;
};

/** Represents a Project Membership */
export type ProjectMember = MemberInterface & {
    __typename?: 'ProjectMember';
    /** GitLab::Access level */
    accessLevel?: Maybe<AccessLevel>;
    /** Date and time the membership was created */
    createdAt?: Maybe<Scalars['Time']>;
    /** User that authorized membership */
    createdBy?: Maybe<User>;
    /** Date and time the membership expires */
    expiresAt?: Maybe<Scalars['Time']>;
    /** ID of the member */
    id: Scalars['ID'];
    /** Project that User is a member of */
    project?: Maybe<Project>;
    /** Date and time the membership was last updated */
    updatedAt?: Maybe<Scalars['Time']>;
    /** User that is associated with the member object */
    user: User;
    /** Permissions for the current user on the resource */
    userPermissions: ProjectPermissions;
};

/** The connection type for ProjectMember. */
export type ProjectMemberConnection = {
    __typename?: 'ProjectMemberConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<ProjectMemberEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<ProjectMember>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type ProjectMemberEdge = {
    __typename?: 'ProjectMemberEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<ProjectMember>;
};

export type ProjectPermissions = {
    __typename?: 'ProjectPermissions';
    /** Indicates the user can perform `admin_operations` on this resource */
    adminOperations: Scalars['Boolean'];
    /** Indicates the user can perform `admin_project` on this resource */
    adminProject: Scalars['Boolean'];
    /** Indicates the user can perform `admin_remote_mirror` on this resource */
    adminRemoteMirror: Scalars['Boolean'];
    /** Indicates the user can perform `admin_wiki` on this resource */
    adminWiki: Scalars['Boolean'];
    /** Indicates the user can perform `archive_project` on this resource */
    archiveProject: Scalars['Boolean'];
    /** Indicates the user can perform `change_namespace` on this resource */
    changeNamespace: Scalars['Boolean'];
    /** Indicates the user can perform `change_visibility_level` on this resource */
    changeVisibilityLevel: Scalars['Boolean'];
    /** Indicates the user can perform `create_deployment` on this resource */
    createDeployment: Scalars['Boolean'];
    /** Indicates the user can perform `create_design` on this resource */
    createDesign: Scalars['Boolean'];
    /** Indicates the user can perform `create_issue` on this resource */
    createIssue: Scalars['Boolean'];
    /** Indicates the user can perform `create_label` on this resource */
    createLabel: Scalars['Boolean'];
    /** Indicates the user can perform `create_merge_request_from` on this resource */
    createMergeRequestFrom: Scalars['Boolean'];
    /** Indicates the user can perform `create_merge_request_in` on this resource */
    createMergeRequestIn: Scalars['Boolean'];
    /** Indicates the user can perform `create_pages` on this resource */
    createPages: Scalars['Boolean'];
    /** Indicates the user can perform `create_pipeline` on this resource */
    createPipeline: Scalars['Boolean'];
    /** Indicates the user can perform `create_pipeline_schedule` on this resource */
    createPipelineSchedule: Scalars['Boolean'];
    /** Indicates the user can perform `create_snippet` on this resource */
    createSnippet: Scalars['Boolean'];
    /** Indicates the user can perform `create_wiki` on this resource */
    createWiki: Scalars['Boolean'];
    /** Indicates the user can perform `destroy_design` on this resource */
    destroyDesign: Scalars['Boolean'];
    /** Indicates the user can perform `destroy_pages` on this resource */
    destroyPages: Scalars['Boolean'];
    /** Indicates the user can perform `destroy_wiki` on this resource */
    destroyWiki: Scalars['Boolean'];
    /** Indicates the user can perform `download_code` on this resource */
    downloadCode: Scalars['Boolean'];
    /** Indicates the user can perform `download_wiki_code` on this resource */
    downloadWikiCode: Scalars['Boolean'];
    /** Indicates the user can perform `fork_project` on this resource */
    forkProject: Scalars['Boolean'];
    /** Indicates the user can perform `push_code` on this resource */
    pushCode: Scalars['Boolean'];
    /** Indicates the user can perform `push_to_delete_protected_branch` on this resource */
    pushToDeleteProtectedBranch: Scalars['Boolean'];
    /** Indicates the user can perform `read_commit_status` on this resource */
    readCommitStatus: Scalars['Boolean'];
    /** Indicates the user can perform `read_cycle_analytics` on this resource */
    readCycleAnalytics: Scalars['Boolean'];
    /** Indicates the user can perform `read_design` on this resource */
    readDesign: Scalars['Boolean'];
    /** Indicates the user can perform `read_merge_request` on this resource */
    readMergeRequest: Scalars['Boolean'];
    /** Indicates the user can perform `read_pages_content` on this resource */
    readPagesContent: Scalars['Boolean'];
    /** Indicates the user can perform `read_project` on this resource */
    readProject: Scalars['Boolean'];
    /** Indicates the user can perform `read_project_member` on this resource */
    readProjectMember: Scalars['Boolean'];
    /** Indicates the user can perform `read_wiki` on this resource */
    readWiki: Scalars['Boolean'];
    /** Indicates the user can perform `remove_fork_project` on this resource */
    removeForkProject: Scalars['Boolean'];
    /** Indicates the user can perform `remove_pages` on this resource */
    removePages: Scalars['Boolean'];
    /** Indicates the user can perform `remove_project` on this resource */
    removeProject: Scalars['Boolean'];
    /** Indicates the user can perform `rename_project` on this resource */
    renameProject: Scalars['Boolean'];
    /** Indicates the user can perform `request_access` on this resource */
    requestAccess: Scalars['Boolean'];
    /** Indicates the user can perform `update_pages` on this resource */
    updatePages: Scalars['Boolean'];
    /** Indicates the user can perform `update_wiki` on this resource */
    updateWiki: Scalars['Boolean'];
    /** Indicates the user can perform `upload_file` on this resource */
    uploadFile: Scalars['Boolean'];
};

export type ProjectStatistics = {
    __typename?: 'ProjectStatistics';
    /** Build artifacts size of the project */
    buildArtifactsSize: Scalars['Float'];
    /** Commit count of the project */
    commitCount: Scalars['Float'];
    /** Large File Storage (LFS) object size of the project */
    lfsObjectsSize: Scalars['Float'];
    /** Packages size of the project */
    packagesSize: Scalars['Float'];
    /** Repository size of the project */
    repositorySize: Scalars['Float'];
    /** Snippets size of the project */
    snippetsSize?: Maybe<Scalars['Float']>;
    /** Storage size of the project */
    storageSize: Scalars['Float'];
    /** Wiki size of the project */
    wikiSize?: Maybe<Scalars['Float']>;
};

/** The alert condition for Prometheus */
export type PrometheusAlert = {
    __typename?: 'PrometheusAlert';
    /** The human-readable text of the alert condition */
    humanizedText: Scalars['String'];
    /** ID of the alert condition */
    id: Scalars['ID'];
};

export type Query = {
    __typename?: 'Query';
    /** Get information about current user */
    currentUser?: Maybe<User>;
    /** Fields related to design management */
    designManagement: DesignManagement;
    /** Text to echo back */
    echo: Scalars['String'];
    /** Find a Geo node */
    geoNode?: Maybe<GeoNode>;
    /** Find a group */
    group?: Maybe<Group>;
    /** Fields related to Instance Security Dashboard */
    instanceSecurityDashboard?: Maybe<InstanceSecurityDashboard>;
    /** Get statistics on the instance */
    instanceStatisticsMeasurements?: Maybe<InstanceStatisticsMeasurementConnection>;
    /** Find an issue */
    issue?: Maybe<Issue>;
    /** Find an iteration */
    iteration?: Maybe<Iteration>;
    /** Metadata about GitLab */
    metadata?: Maybe<Metadata>;
    /** Find a milestone */
    milestone?: Maybe<Milestone>;
    /** Find a namespace */
    namespace?: Maybe<Namespace>;
    /** Find a project */
    project?: Maybe<Project>;
    /** Find projects visible to the current user */
    projects?: Maybe<ProjectConnection>;
    /** Find Snippets visible to the current user */
    snippets?: Maybe<SnippetConnection>;
    /** Find a user */
    user?: Maybe<User>;
    /** Find users */
    users?: Maybe<UserConnection>;
    /** Vulnerabilities reported on projects on the current user's instance security dashboard */
    vulnerabilities?: Maybe<VulnerabilityConnection>;
    /** Number of vulnerabilities per day for the projects on the current user's instance security dashboard */
    vulnerabilitiesCountByDay?: Maybe<VulnerabilitiesCountByDayConnection>;
    /**
     * Number of vulnerabilities per severity level, per day, for the projects on the current user's instance security dashboard. Deprecated in 13.3: Use `vulnerabilitiesCountByDay`
     * @deprecated Use `vulnerabilitiesCountByDay`. Deprecated in 13.3
     */
    vulnerabilitiesCountByDayAndSeverity?: Maybe<VulnerabilitiesCountByDayAndSeverityConnection>;
};


export type QueryEchoArgs = {
    text: Scalars['String'];
};


export type QueryGeoNodeArgs = {
    name?: Maybe<Scalars['String']>;
};


export type QueryGroupArgs = {
    fullPath: Scalars['ID'];
};


export type QueryInstanceStatisticsMeasurementsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    identifier: MeasurementIdentifier;
    last?: Maybe<Scalars['Int']>;
};


export type QueryIssueArgs = {
    id: Scalars['IssueID'];
};


export type QueryIterationArgs = {
    id: Scalars['IterationID'];
};


export type QueryMilestoneArgs = {
    id: Scalars['MilestoneID'];
};


export type QueryNamespaceArgs = {
    fullPath: Scalars['ID'];
};


export type QueryProjectArgs = {
    fullPath: Scalars['ID'];
};


export type QueryProjectsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    ids?: Maybe<Array<Scalars['ID']>>;
    last?: Maybe<Scalars['Int']>;
    membership?: Maybe<Scalars['Boolean']>;
    search?: Maybe<Scalars['String']>;
};


export type QuerySnippetsArgs = {
    after?: Maybe<Scalars['String']>;
    authorId?: Maybe<Scalars['ID']>;
    before?: Maybe<Scalars['String']>;
    explore?: Maybe<Scalars['Boolean']>;
    first?: Maybe<Scalars['Int']>;
    ids?: Maybe<Array<Scalars['ID']>>;
    last?: Maybe<Scalars['Int']>;
    projectId?: Maybe<Scalars['ID']>;
    type?: Maybe<TypeEnum>;
    visibility?: Maybe<VisibilityScopesEnum>;
};


export type QueryUserArgs = {
    id?: Maybe<Scalars['ID']>;
    username?: Maybe<Scalars['String']>;
};


export type QueryUsersArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    ids?: Maybe<Array<Scalars['ID']>>;
    last?: Maybe<Scalars['Int']>;
    sort?: Maybe<Sort>;
    usernames?: Maybe<Array<Scalars['String']>>;
};


export type QueryVulnerabilitiesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    hasIssues?: Maybe<Scalars['Boolean']>;
    hasResolution?: Maybe<Scalars['Boolean']>;
    last?: Maybe<Scalars['Int']>;
    projectId?: Maybe<Array<Scalars['ID']>>;
    reportType?: Maybe<Array<VulnerabilityReportType>>;
    scanner?: Maybe<Array<Scalars['String']>>;
    severity?: Maybe<Array<VulnerabilitySeverity>>;
    sort?: Maybe<VulnerabilitySort>;
    state?: Maybe<Array<VulnerabilityState>>;
};


export type QueryVulnerabilitiesCountByDayArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    endDate: Scalars['ISO8601Date'];
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    startDate: Scalars['ISO8601Date'];
};


export type QueryVulnerabilitiesCountByDayAndSeverityArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    endDate: Scalars['ISO8601Date'];
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    startDate: Scalars['ISO8601Date'];
};

/** Represents a release */
export type Release = {
    __typename?: 'Release';
    /** Assets of the release */
    assets?: Maybe<ReleaseAssets>;
    /** User that created the release */
    author?: Maybe<User>;
    /** The commit associated with the release */
    commit?: Maybe<Commit>;
    /** Timestamp of when the release was created */
    createdAt?: Maybe<Scalars['Time']>;
    /** Description (also known as "release notes") of the release */
    description?: Maybe<Scalars['String']>;
    /** The GitLab Flavored Markdown rendering of `description` */
    descriptionHtml?: Maybe<Scalars['String']>;
    /** Evidence for the release */
    evidences?: Maybe<ReleaseEvidenceConnection>;
    /** Links of the release */
    links?: Maybe<ReleaseLinks>;
    /** Milestones associated to the release */
    milestones?: Maybe<MilestoneConnection>;
    /** Name of the release */
    name?: Maybe<Scalars['String']>;
    /** Timestamp of when the release was released */
    releasedAt?: Maybe<Scalars['Time']>;
    /** Name of the tag associated with the release */
    tagName?: Maybe<Scalars['String']>;
    /** Relative web path to the tag associated with the release */
    tagPath?: Maybe<Scalars['String']>;
    /** Indicates the release is an upcoming release */
    upcomingRelease?: Maybe<Scalars['Boolean']>;
};


/** Represents a release */
export type ReleaseEvidencesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** Represents a release */
export type ReleaseMilestonesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** Represents an asset link associated with a release */
export type ReleaseAssetLink = {
    __typename?: 'ReleaseAssetLink';
    /** Direct asset URL of the link */
    directAssetUrl?: Maybe<Scalars['String']>;
    /** Indicates the link points to an external resource */
    external?: Maybe<Scalars['Boolean']>;
    /** ID of the link */
    id: Scalars['ID'];
    /** Type of the link: `other`, `runbook`, `image`, `package`; defaults to `other` */
    linkType?: Maybe<ReleaseAssetLinkType>;
    /** Name of the link */
    name?: Maybe<Scalars['String']>;
    /** URL of the link */
    url?: Maybe<Scalars['String']>;
};

/** The connection type for ReleaseAssetLink. */
export type ReleaseAssetLinkConnection = {
    __typename?: 'ReleaseAssetLinkConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<ReleaseAssetLinkEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<ReleaseAssetLink>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type ReleaseAssetLinkEdge = {
    __typename?: 'ReleaseAssetLinkEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<ReleaseAssetLink>;
};

/** A container for all assets associated with a release */
export type ReleaseAssets = {
    __typename?: 'ReleaseAssets';
    /** Number of assets of the release */
    count?: Maybe<Scalars['Int']>;
    /** Asset links of the release */
    links?: Maybe<ReleaseAssetLinkConnection>;
    /** Sources of the release */
    sources?: Maybe<ReleaseSourceConnection>;
};


/** A container for all assets associated with a release */
export type ReleaseAssetsLinksArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** A container for all assets associated with a release */
export type ReleaseAssetsSourcesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** The connection type for Release. */
export type ReleaseConnection = {
    __typename?: 'ReleaseConnection';
    /** Total count of collection */
    count: Scalars['Int'];
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<ReleaseEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Release>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type ReleaseEdge = {
    __typename?: 'ReleaseEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Release>;
};

/** Evidence for a release */
export type ReleaseEvidence = {
    __typename?: 'ReleaseEvidence';
    /** Timestamp when the evidence was collected */
    collectedAt?: Maybe<Scalars['Time']>;
    /** URL from where the evidence can be downloaded */
    filepath?: Maybe<Scalars['String']>;
    /** ID of the evidence */
    id: Scalars['ID'];
    /** SHA1 ID of the evidence hash */
    sha?: Maybe<Scalars['String']>;
};

/** The connection type for ReleaseEvidence. */
export type ReleaseEvidenceConnection = {
    __typename?: 'ReleaseEvidenceConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<ReleaseEvidenceEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<ReleaseEvidence>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type ReleaseEvidenceEdge = {
    __typename?: 'ReleaseEvidenceEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<ReleaseEvidence>;
};

export type ReleaseLinks = {
    __typename?: 'ReleaseLinks';
    /** HTTP URL of the release's edit page */
    editUrl?: Maybe<Scalars['String']>;
    /** HTTP URL of the issues page filtered by this release */
    issuesUrl?: Maybe<Scalars['String']>;
    /** HTTP URL of the merge request page filtered by this release */
    mergeRequestsUrl?: Maybe<Scalars['String']>;
    /** HTTP URL of the release */
    selfUrl?: Maybe<Scalars['String']>;
};

/** Represents the source code attached to a release in a particular format */
export type ReleaseSource = {
    __typename?: 'ReleaseSource';
    /** Format of the source */
    format?: Maybe<Scalars['String']>;
    /** Download URL of the source */
    url?: Maybe<Scalars['String']>;
};

/** The connection type for ReleaseSource. */
export type ReleaseSourceConnection = {
    __typename?: 'ReleaseSourceConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<ReleaseSourceEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<ReleaseSource>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type ReleaseSourceEdge = {
    __typename?: 'ReleaseSourceEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<ReleaseSource>;
};

/** Autogenerated return type of RemoveAwardEmoji */
export type RemoveAwardEmojiPayload = {
    __typename?: 'RemoveAwardEmojiPayload';
    /** The award emoji after mutation */
    awardEmoji?: Maybe<AwardEmoji>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** Autogenerated return type of RemoveProjectFromSecurityDashboard */
export type RemoveProjectFromSecurityDashboardPayload = {
    __typename?: 'RemoveProjectFromSecurityDashboardPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

export type Repository = {
    __typename?: 'Repository';
    /** Indicates repository has no visible content */
    empty: Scalars['Boolean'];
    /** Indicates a corresponding Git repository exists on disk */
    exists: Scalars['Boolean'];
    /** Default branch of the repository */
    rootRef?: Maybe<Scalars['String']>;
    /** Tree of the repository */
    tree?: Maybe<Tree>;
};


export type RepositoryTreeArgs = {
    path?: Maybe<Scalars['String']>;
    recursive?: Maybe<Scalars['Boolean']>;
    ref?: Maybe<Scalars['String']>;
};

/** Represents a requirement */
export type Requirement = {
    __typename?: 'Requirement';
    /** Author of the requirement */
    author: User;
    /** Timestamp of when the requirement was created */
    createdAt: Scalars['Time'];
    /** ID of the requirement */
    id: Scalars['ID'];
    /** Internal ID of the requirement */
    iid: Scalars['ID'];
    /** Latest requirement test report state */
    lastTestReportState?: Maybe<TestReportState>;
    /** Project to which the requirement belongs */
    project: Project;
    /** State of the requirement */
    state: RequirementState;
    /** Test reports of the requirement */
    testReports?: Maybe<TestReportConnection>;
    /** Title of the requirement */
    title?: Maybe<Scalars['String']>;
    /** Timestamp of when the requirement was last updated */
    updatedAt: Scalars['Time'];
    /** Permissions for the current user on the resource */
    userPermissions: RequirementPermissions;
};


/** Represents a requirement */
export type RequirementTestReportsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    sort?: Maybe<Sort>;
};

/** The connection type for Requirement. */
export type RequirementConnection = {
    __typename?: 'RequirementConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<RequirementEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Requirement>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type RequirementEdge = {
    __typename?: 'RequirementEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Requirement>;
};

/** Check permissions for the current user on a requirement */
export type RequirementPermissions = {
    __typename?: 'RequirementPermissions';
    /** Indicates the user can perform `admin_requirement` on this resource */
    adminRequirement: Scalars['Boolean'];
    /** Indicates the user can perform `create_requirement` on this resource */
    createRequirement: Scalars['Boolean'];
    /** Indicates the user can perform `destroy_requirement` on this resource */
    destroyRequirement: Scalars['Boolean'];
    /** Indicates the user can perform `read_requirement` on this resource */
    readRequirement: Scalars['Boolean'];
    /** Indicates the user can perform `update_requirement` on this resource */
    updateRequirement: Scalars['Boolean'];
};

/** Counts of requirements by their state */
export type RequirementStatesCount = {
    __typename?: 'RequirementStatesCount';
    /** Number of archived requirements */
    archived?: Maybe<Scalars['Int']>;
    /** Number of opened requirements */
    opened?: Maybe<Scalars['Int']>;
};

export type RootStorageStatistics = {
    __typename?: 'RootStorageStatistics';
    /** The CI artifacts size in bytes */
    buildArtifactsSize: Scalars['Float'];
    /** The LFS objects size in bytes */
    lfsObjectsSize: Scalars['Float'];
    /** The packages size in bytes */
    packagesSize: Scalars['Float'];
    /** The Git repository size in bytes */
    repositorySize: Scalars['Float'];
    /** The snippets size in bytes */
    snippetsSize: Scalars['Float'];
    /** The total storage in bytes */
    storageSize: Scalars['Float'];
    /** The wiki size in bytes */
    wikiSize: Scalars['Float'];
};

/** Autogenerated return type of RunDASTScan */
export type RunDastScanPayload = {
    __typename?: 'RunDASTScanPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** URL of the pipeline that was created. */
    pipelineUrl?: Maybe<Scalars['String']>;
};

/** Represents a CI configuration of SAST */
export type SastCiConfiguration = {
    __typename?: 'SastCiConfiguration';
    /** List of analyzers entities attached to SAST configuration. */
    analyzers?: Maybe<SastCiConfigurationAnalyzersEntityConnection>;
    /** List of global entities related to SAST configuration. */
    global?: Maybe<SastCiConfigurationEntityConnection>;
    /** List of pipeline entities related to SAST configuration. */
    pipeline?: Maybe<SastCiConfigurationEntityConnection>;
};


/** Represents a CI configuration of SAST */
export type SastCiConfigurationAnalyzersArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** Represents a CI configuration of SAST */
export type SastCiConfigurationGlobalArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** Represents a CI configuration of SAST */
export type SastCiConfigurationPipelineArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** Represents an analyzer entity in SAST CI configuration */
export type SastCiConfigurationAnalyzersEntity = {
    __typename?: 'SastCiConfigurationAnalyzersEntity';
    /** Analyzer description that is displayed on the form */
    description?: Maybe<Scalars['String']>;
    /** Indicates whether an analyzer is enabled */
    enabled?: Maybe<Scalars['Boolean']>;
    /** Analyzer label used in the config UI */
    label?: Maybe<Scalars['String']>;
    /** Name of the analyzer */
    name?: Maybe<Scalars['String']>;
    /** List of supported variables */
    variables?: Maybe<SastCiConfigurationEntityConnection>;
};


/** Represents an analyzer entity in SAST CI configuration */
export type SastCiConfigurationAnalyzersEntityVariablesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** The connection type for SastCiConfigurationAnalyzersEntity. */
export type SastCiConfigurationAnalyzersEntityConnection = {
    __typename?: 'SastCiConfigurationAnalyzersEntityConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<SastCiConfigurationAnalyzersEntityEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<SastCiConfigurationAnalyzersEntity>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type SastCiConfigurationAnalyzersEntityEdge = {
    __typename?: 'SastCiConfigurationAnalyzersEntityEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<SastCiConfigurationAnalyzersEntity>;
};

/** Represents an entity in SAST CI configuration */
export type SastCiConfigurationEntity = {
    __typename?: 'SastCiConfigurationEntity';
    /** Default value that is used if value is empty. */
    defaultValue?: Maybe<Scalars['String']>;
    /** Entity description that is displayed on the form. */
    description?: Maybe<Scalars['String']>;
    /** CI keyword of entity. */
    field?: Maybe<Scalars['String']>;
    /** Label for entity used in the form. */
    label?: Maybe<Scalars['String']>;
    /** Different possible values of the field. */
    options?: Maybe<SastCiConfigurationOptionsEntityConnection>;
    /** Size of the UI component. */
    size?: Maybe<SastUiComponentSize>;
    /** Type of the field value. */
    type?: Maybe<Scalars['String']>;
    /** Current value of the entity. */
    value?: Maybe<Scalars['String']>;
};


/** Represents an entity in SAST CI configuration */
export type SastCiConfigurationEntityOptionsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** The connection type for SastCiConfigurationEntity. */
export type SastCiConfigurationEntityConnection = {
    __typename?: 'SastCiConfigurationEntityConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<SastCiConfigurationEntityEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<SastCiConfigurationEntity>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type SastCiConfigurationEntityEdge = {
    __typename?: 'SastCiConfigurationEntityEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<SastCiConfigurationEntity>;
};

/** Represents an entity for options in SAST CI configuration */
export type SastCiConfigurationOptionsEntity = {
    __typename?: 'SastCiConfigurationOptionsEntity';
    /** Label of option entity. */
    label?: Maybe<Scalars['String']>;
    /** Value of option entity. */
    value?: Maybe<Scalars['String']>;
};

/** The connection type for SastCiConfigurationOptionsEntity. */
export type SastCiConfigurationOptionsEntityConnection = {
    __typename?: 'SastCiConfigurationOptionsEntityConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<SastCiConfigurationOptionsEntityEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<SastCiConfigurationOptionsEntity>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type SastCiConfigurationOptionsEntityEdge = {
    __typename?: 'SastCiConfigurationOptionsEntityEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<SastCiConfigurationOptionsEntity>;
};

/** Represents a resource scanned by a security scan */
export type ScannedResource = {
    __typename?: 'ScannedResource';
    /** The HTTP request method used to access the URL */
    requestMethod?: Maybe<Scalars['String']>;
    /** The URL scanned by the scanner */
    url?: Maybe<Scalars['String']>;
};

/** The connection type for ScannedResource. */
export type ScannedResourceConnection = {
    __typename?: 'ScannedResourceConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<ScannedResourceEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<ScannedResource>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type ScannedResourceEdge = {
    __typename?: 'ScannedResourceEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<ScannedResource>;
};

/** Represents summary of a security report */
export type SecurityReportSummary = {
    __typename?: 'SecurityReportSummary';
    /** Aggregated counts for the container_scanning scan */
    containerScanning?: Maybe<SecurityReportSummarySection>;
    /** Aggregated counts for the coverage_fuzzing scan */
    coverageFuzzing?: Maybe<SecurityReportSummarySection>;
    /** Aggregated counts for the dast scan */
    dast?: Maybe<SecurityReportSummarySection>;
    /** Aggregated counts for the dependency_scanning scan */
    dependencyScanning?: Maybe<SecurityReportSummarySection>;
    /** Aggregated counts for the sast scan */
    sast?: Maybe<SecurityReportSummarySection>;
    /** Aggregated counts for the secret_detection scan */
    secretDetection?: Maybe<SecurityReportSummarySection>;
};

/** Represents a section of a summary of a security report */
export type SecurityReportSummarySection = {
    __typename?: 'SecurityReportSummarySection';
    /** A list of the first 20 scanned resources */
    scannedResources?: Maybe<ScannedResourceConnection>;
    /** Total number of scanned resources */
    scannedResourcesCount?: Maybe<Scalars['Int']>;
    /** Path to download all the scanned resources in CSV format */
    scannedResourcesCsvPath?: Maybe<Scalars['String']>;
    /** Total number of vulnerabilities */
    vulnerabilitiesCount?: Maybe<Scalars['Int']>;
};


/** Represents a section of a summary of a security report */
export type SecurityReportSummarySectionScannedResourcesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** Represents a list of security scanners */
export type SecurityScanners = {
    __typename?: 'SecurityScanners';
    /** List of analyzers which are available for the project. */
    available?: Maybe<Array<SecurityScannerType>>;
    /** List of analyzers which are enabled for the project. */
    enabled?: Maybe<Array<SecurityScannerType>>;
    /** List of analyzers which ran successfully in the latest pipeline. */
    pipelineRun?: Maybe<Array<SecurityScannerType>>;
};

/** A Sentry error */
export type SentryDetailedError = {
    __typename?: 'SentryDetailedError';
    /** Count of occurrences */
    count: Scalars['Int'];
    /** Culprit of the error */
    culprit: Scalars['String'];
    /** External Base URL of the Sentry Instance */
    externalBaseUrl: Scalars['String'];
    /** External URL of the error */
    externalUrl: Scalars['String'];
    /** Commit the error was first seen */
    firstReleaseLastCommit?: Maybe<Scalars['String']>;
    /** Release short version the error was first seen */
    firstReleaseShortVersion?: Maybe<Scalars['String']>;
    /** Release version the error was first seen */
    firstReleaseVersion?: Maybe<Scalars['String']>;
    /** Timestamp when the error was first seen */
    firstSeen: Scalars['Time'];
    /** Last 24hr stats of the error */
    frequency: Array<SentryErrorFrequency>;
    /** GitLab commit SHA attributed to the Error based on the release version */
    gitlabCommit?: Maybe<Scalars['String']>;
    /** Path to the GitLab page for the GitLab commit attributed to the error */
    gitlabCommitPath?: Maybe<Scalars['String']>;
    /** URL of GitLab Issue */
    gitlabIssuePath?: Maybe<Scalars['String']>;
    /** ID (global ID) of the error */
    id: Scalars['ID'];
    /** Commit the error was last seen */
    lastReleaseLastCommit?: Maybe<Scalars['String']>;
    /** Release short version the error was last seen */
    lastReleaseShortVersion?: Maybe<Scalars['String']>;
    /** Release version the error was last seen */
    lastReleaseVersion?: Maybe<Scalars['String']>;
    /** Timestamp when the error was last seen */
    lastSeen: Scalars['Time'];
    /** Sentry metadata message of the error */
    message?: Maybe<Scalars['String']>;
    /** ID (Sentry ID) of the error */
    sentryId: Scalars['String'];
    /** ID of the project (Sentry project) */
    sentryProjectId: Scalars['ID'];
    /** Name of the project affected by the error */
    sentryProjectName: Scalars['String'];
    /** Slug of the project affected by the error */
    sentryProjectSlug: Scalars['String'];
    /** Short ID (Sentry ID) of the error */
    shortId: Scalars['String'];
    /** Status of the error */
    status: SentryErrorStatus;
    /** Tags associated with the Sentry Error */
    tags: SentryErrorTags;
    /** Title of the error */
    title: Scalars['String'];
    /** Type of the error */
    type: Scalars['String'];
    /** Count of users affected by the error */
    userCount: Scalars['Int'];
};

/** A Sentry error. A simplified version of SentryDetailedError */
export type SentryError = {
    __typename?: 'SentryError';
    /** Count of occurrences */
    count: Scalars['Int'];
    /** Culprit of the error */
    culprit: Scalars['String'];
    /** External URL of the error */
    externalUrl: Scalars['String'];
    /** Timestamp when the error was first seen */
    firstSeen: Scalars['Time'];
    /** Last 24hr stats of the error */
    frequency: Array<SentryErrorFrequency>;
    /** ID (global ID) of the error */
    id: Scalars['ID'];
    /** Timestamp when the error was last seen */
    lastSeen: Scalars['Time'];
    /** Sentry metadata message of the error */
    message?: Maybe<Scalars['String']>;
    /** ID (Sentry ID) of the error */
    sentryId: Scalars['String'];
    /** ID of the project (Sentry project) */
    sentryProjectId: Scalars['ID'];
    /** Name of the project affected by the error */
    sentryProjectName: Scalars['String'];
    /** Slug of the project affected by the error */
    sentryProjectSlug: Scalars['String'];
    /** Short ID (Sentry ID) of the error */
    shortId: Scalars['String'];
    /** Status of the error */
    status: SentryErrorStatus;
    /** Title of the error */
    title: Scalars['String'];
    /** Type of the error */
    type: Scalars['String'];
    /** Count of users affected by the error */
    userCount: Scalars['Int'];
};

/** An object containing a collection of Sentry errors, and a detailed error */
export type SentryErrorCollection = {
    __typename?: 'SentryErrorCollection';
    /** Detailed version of a Sentry error on the project */
    detailedError?: Maybe<SentryDetailedError>;
    /** Stack Trace of Sentry Error */
    errorStackTrace?: Maybe<SentryErrorStackTrace>;
    /** Collection of Sentry Errors */
    errors?: Maybe<SentryErrorConnection>;
    /** External URL for Sentry */
    externalUrl?: Maybe<Scalars['String']>;
};


/** An object containing a collection of Sentry errors, and a detailed error */
export type SentryErrorCollectionDetailedErrorArgs = {
    id: Scalars['ID'];
};


/** An object containing a collection of Sentry errors, and a detailed error */
export type SentryErrorCollectionErrorStackTraceArgs = {
    id: Scalars['ID'];
};


/** An object containing a collection of Sentry errors, and a detailed error */
export type SentryErrorCollectionErrorsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    searchTerm?: Maybe<Scalars['String']>;
    sort?: Maybe<Scalars['String']>;
};

/** The connection type for SentryError. */
export type SentryErrorConnection = {
    __typename?: 'SentryErrorConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<SentryErrorEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<SentryError>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type SentryErrorEdge = {
    __typename?: 'SentryErrorEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<SentryError>;
};

export type SentryErrorFrequency = {
    __typename?: 'SentryErrorFrequency';
    /** Count of errors received since the previously recorded time */
    count: Scalars['Int'];
    /** Time the error frequency stats were recorded */
    time: Scalars['Time'];
};

/** An object containing a stack trace entry for a Sentry error */
export type SentryErrorStackTrace = {
    __typename?: 'SentryErrorStackTrace';
    /** Time the stack trace was received by Sentry */
    dateReceived: Scalars['String'];
    /** ID of the Sentry error */
    issueId: Scalars['String'];
    /** Stack trace entries for the Sentry error */
    stackTraceEntries: Array<SentryErrorStackTraceEntry>;
};

/** An object context for a Sentry error stack trace */
export type SentryErrorStackTraceContext = {
    __typename?: 'SentryErrorStackTraceContext';
    /** Code number of the context */
    code: Scalars['String'];
    /** Line number of the context */
    line: Scalars['Int'];
};

/** An object containing a stack trace entry for a Sentry error */
export type SentryErrorStackTraceEntry = {
    __typename?: 'SentryErrorStackTraceEntry';
    /** Function in which the Sentry error occurred */
    col?: Maybe<Scalars['String']>;
    /** File in which the Sentry error occurred */
    fileName?: Maybe<Scalars['String']>;
    /** Function in which the Sentry error occurred */
    function?: Maybe<Scalars['String']>;
    /** Function in which the Sentry error occurred */
    line?: Maybe<Scalars['String']>;
    /** Context of the Sentry error */
    traceContext?: Maybe<Array<SentryErrorStackTraceContext>>;
};

/** State of a Sentry error */
export type SentryErrorTags = {
    __typename?: 'SentryErrorTags';
    /** Severity level of the Sentry Error */
    level?: Maybe<Scalars['String']>;
    /** Logger of the Sentry Error */
    logger?: Maybe<Scalars['String']>;
};

/** The connection type for Service. */
export type ServiceConnection = {
    __typename?: 'ServiceConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<ServiceEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Service>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type ServiceEdge = {
    __typename?: 'ServiceEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Service>;
};

/** Represents a snippet entry */
export type Snippet = Noteable & {
    __typename?: 'Snippet';
    /** The owner of the snippet */
    author?: Maybe<User>;
    /**
     * Snippet blob. Deprecated in 13.3: Use `blobs`
     * @deprecated Use `blobs`. Deprecated in 13.3
     */
    blob: SnippetBlob;
    /** Snippet blobs */
    blobs: Array<SnippetBlob>;
    /** Timestamp this snippet was created */
    createdAt: Scalars['Time'];
    /** Description of the snippet */
    description?: Maybe<Scalars['String']>;
    /** The GitLab Flavored Markdown rendering of `description` */
    descriptionHtml?: Maybe<Scalars['String']>;
    /** All discussions on this noteable */
    discussions: DiscussionConnection;
    /** File Name of the snippet */
    fileName?: Maybe<Scalars['String']>;
    /** HTTP URL to the snippet repository */
    httpUrlToRepo?: Maybe<Scalars['String']>;
    /** ID of the snippet */
    id: Scalars['ID'];
    /** All notes on this noteable */
    notes: NoteConnection;
    /** The project the snippet is associated with */
    project?: Maybe<Project>;
    /** Raw URL of the snippet */
    rawUrl: Scalars['String'];
    /** SSH URL to the snippet repository */
    sshUrlToRepo?: Maybe<Scalars['String']>;
    /** Title of the snippet */
    title: Scalars['String'];
    /** Timestamp this snippet was updated */
    updatedAt: Scalars['Time'];
    /** Permissions for the current user on the resource */
    userPermissions: SnippetPermissions;
    /** Visibility Level of the snippet */
    visibilityLevel: VisibilityLevelsEnum;
    /** Web URL of the snippet */
    webUrl: Scalars['String'];
};


/** Represents a snippet entry */
export type SnippetDiscussionsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


/** Represents a snippet entry */
export type SnippetNotesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** Represents the snippet blob */
export type SnippetBlob = {
    __typename?: 'SnippetBlob';
    /** Shows whether the blob is binary */
    binary: Scalars['Boolean'];
    /** Blob external storage */
    externalStorage?: Maybe<Scalars['String']>;
    /** Blob mode */
    mode?: Maybe<Scalars['String']>;
    /** Blob name */
    name?: Maybe<Scalars['String']>;
    /** Blob path */
    path?: Maybe<Scalars['String']>;
    /** Blob plain highlighted data */
    plainData?: Maybe<Scalars['String']>;
    /** Blob raw content endpoint path */
    rawPath: Scalars['String'];
    /** Shows whether the blob is rendered as text */
    renderedAsText: Scalars['Boolean'];
    /** Blob highlighted data */
    richData?: Maybe<Scalars['String']>;
    /** Blob content rich viewer */
    richViewer?: Maybe<SnippetBlobViewer>;
    /** Blob content simple viewer */
    simpleViewer: SnippetBlobViewer;
    /** Blob size */
    size: Scalars['Int'];
};

/** Represents how the blob content should be displayed */
export type SnippetBlobViewer = {
    __typename?: 'SnippetBlobViewer';
    /** Shows whether the blob should be displayed collapsed */
    collapsed: Scalars['Boolean'];
    /** Content file type */
    fileType: Scalars['String'];
    /** Shows whether the blob content is loaded async */
    loadAsync: Scalars['Boolean'];
    /** Loading partial name */
    loadingPartialName: Scalars['String'];
    /** Error rendering the blob content */
    renderError?: Maybe<Scalars['String']>;
    /** Shows whether the blob too large to be displayed */
    tooLarge: Scalars['Boolean'];
    /** Type of blob viewer */
    type: BlobViewersType;
};

/** The connection type for Snippet. */
export type SnippetConnection = {
    __typename?: 'SnippetConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<SnippetEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Snippet>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type SnippetEdge = {
    __typename?: 'SnippetEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Snippet>;
};

export type SnippetPermissions = {
    __typename?: 'SnippetPermissions';
    /** Indicates the user can perform `admin_snippet` on this resource */
    adminSnippet: Scalars['Boolean'];
    /** Indicates the user can perform `award_emoji` on this resource */
    awardEmoji: Scalars['Boolean'];
    /** Indicates the user can perform `create_note` on this resource */
    createNote: Scalars['Boolean'];
    /** Indicates the user can perform `read_snippet` on this resource */
    readSnippet: Scalars['Boolean'];
    /** Indicates the user can perform `report_snippet` on this resource */
    reportSnippet: Scalars['Boolean'];
    /** Indicates the user can perform `update_snippet` on this resource */
    updateSnippet: Scalars['Boolean'];
};

export type Submodule = Entry & {
    __typename?: 'Submodule';
    /** Flat path of the entry */
    flatPath: Scalars['String'];
    /** ID of the entry */
    id: Scalars['ID'];
    /** Name of the entry */
    name: Scalars['String'];
    /** Path of the entry */
    path: Scalars['String'];
    /** Last commit sha for the entry */
    sha: Scalars['String'];
    /** Tree URL for the sub-module */
    treeUrl?: Maybe<Scalars['String']>;
    /** Type of tree entry */
    type: EntryType;
    /** Web URL for the sub-module */
    webUrl?: Maybe<Scalars['String']>;
};

/** The connection type for Submodule. */
export type SubmoduleConnection = {
    __typename?: 'SubmoduleConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<SubmoduleEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Submodule>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type SubmoduleEdge = {
    __typename?: 'SubmoduleEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Submodule>;
};

/** Completion status of tasks */
export type TaskCompletionStatus = {
    __typename?: 'TaskCompletionStatus';
    /** Number of completed tasks */
    completedCount: Scalars['Int'];
    /** Number of total tasks */
    count: Scalars['Int'];
};

/** Represents a requirement test report */
export type TestReport = {
    __typename?: 'TestReport';
    /** Author of the test report */
    author?: Maybe<User>;
    /** Timestamp of when the test report was created */
    createdAt: Scalars['Time'];
    /** ID of the test report */
    id: Scalars['ID'];
    /** State of the test report */
    state: TestReportState;
};

/** The connection type for TestReport. */
export type TestReportConnection = {
    __typename?: 'TestReportConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<TestReportEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<TestReport>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type TestReportEdge = {
    __typename?: 'TestReportEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<TestReport>;
};

export type Timelog = {
    __typename?: 'Timelog';
    /**
     * Timestamp of when the time tracked was spent at. Deprecated in 12.10: Use `spentAt`
     * @deprecated Use `spentAt`. Deprecated in 12.10
     */
    date: Scalars['Time'];
    /** The issue that logged time was added to */
    issue?: Maybe<Issue>;
    /** The note where the quick action to add the logged time was executed */
    note?: Maybe<Note>;
    /** Timestamp of when the time tracked was spent at */
    spentAt?: Maybe<Scalars['Time']>;
    /** The time spent displayed in seconds */
    timeSpent: Scalars['Int'];
    /** The user that logged the time */
    user: User;
};

/** The connection type for Timelog. */
export type TimelogConnection = {
    __typename?: 'TimelogConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<TimelogEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Timelog>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type TimelogEdge = {
    __typename?: 'TimelogEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Timelog>;
};

/** Representing a todo entry */
export type Todo = {
    __typename?: 'Todo';
    /** Action of the todo */
    action: TodoActionEnum;
    /** The author of this todo */
    author: User;
    /** Body of the todo */
    body: Scalars['String'];
    /** Timestamp this todo was created */
    createdAt: Scalars['Time'];
    /** Group this todo is associated with */
    group?: Maybe<Group>;
    /** ID of the todo */
    id: Scalars['ID'];
    /** The project this todo is associated with */
    project?: Maybe<Project>;
    /** State of the todo */
    state: TodoStateEnum;
    /** Target type of the todo */
    targetType: TodoTargetEnum;
};

/** The connection type for Todo. */
export type TodoConnection = {
    __typename?: 'TodoConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<TodoEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Todo>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type TodoEdge = {
    __typename?: 'TodoEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Todo>;
};

/** Autogenerated return type of TodoMarkDone */
export type TodoMarkDonePayload = {
    __typename?: 'TodoMarkDonePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The requested todo */
    todo: Todo;
};

/** Autogenerated return type of TodoRestoreMany */
export type TodoRestoreManyPayload = {
    __typename?: 'TodoRestoreManyPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** Updated todos */
    todos: Array<Todo>;
    /**
     * The ids of the updated todo items. Deprecated in 13.2: Use todos
     * @deprecated Use todos. Deprecated in 13.2
     */
    updatedIds: Array<Scalars['ID']>;
};

/** Autogenerated return type of TodoRestore */
export type TodoRestorePayload = {
    __typename?: 'TodoRestorePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The requested todo */
    todo: Todo;
};

/** Autogenerated return type of TodosMarkAllDone */
export type TodosMarkAllDonePayload = {
    __typename?: 'TodosMarkAllDonePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** Updated todos */
    todos: Array<Todo>;
    /**
     * Ids of the updated todos. Deprecated in 13.2: Use todos
     * @deprecated Use todos. Deprecated in 13.2
     */
    updatedIds: Array<Scalars['ID']>;
};

/** Autogenerated return type of ToggleAwardEmoji */
export type ToggleAwardEmojiPayload = {
    __typename?: 'ToggleAwardEmojiPayload';
    /** The award emoji after mutation */
    awardEmoji?: Maybe<AwardEmoji>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** Indicates the status of the emoji. True if the toggle awarded the emoji, and false if the toggle removed the emoji. */
    toggledOn: Scalars['Boolean'];
};

export type Tree = {
    __typename?: 'Tree';
    /** Blobs of the tree */
    blobs: BlobConnection;
    /** Last commit for the tree */
    lastCommit?: Maybe<Commit>;
    /** Sub-modules of the tree */
    submodules: SubmoduleConnection;
    /** Trees of the tree */
    trees: TreeEntryConnection;
};


export type TreeBlobsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type TreeSubmodulesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type TreeTreesArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** Represents a directory */
export type TreeEntry = Entry & {
    __typename?: 'TreeEntry';
    /** Flat path of the entry */
    flatPath: Scalars['String'];
    /** ID of the entry */
    id: Scalars['ID'];
    /** Name of the entry */
    name: Scalars['String'];
    /** Path of the entry */
    path: Scalars['String'];
    /** Last commit sha for the entry */
    sha: Scalars['String'];
    /** Type of tree entry */
    type: EntryType;
    /** Web path for the tree entry (directory) */
    webPath?: Maybe<Scalars['String']>;
    /** Web URL for the tree entry (directory) */
    webUrl?: Maybe<Scalars['String']>;
};

/** The connection type for TreeEntry. */
export type TreeEntryConnection = {
    __typename?: 'TreeEntryConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<TreeEntryEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<TreeEntry>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type TreeEntryEdge = {
    __typename?: 'TreeEntryEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<TreeEntry>;
};

/** Autogenerated return type of UpdateAlertStatus */
export type UpdateAlertStatusPayload = {
    __typename?: 'UpdateAlertStatusPayload';
    /** The alert after mutation */
    alert?: Maybe<AlertManagementAlert>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The issue created after mutation */
    issue?: Maybe<Issue>;
    /** The todo after mutation */
    todo?: Maybe<Todo>;
};

/** Autogenerated return type of UpdateBoardList */
export type UpdateBoardListPayload = {
    __typename?: 'UpdateBoardListPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** Mutated list */
    list?: Maybe<BoardList>;
};

/** Autogenerated return type of UpdateBoard */
export type UpdateBoardPayload = {
    __typename?: 'UpdateBoardPayload';
    /** The board after mutation. */
    board?: Maybe<Board>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** Autogenerated return type of UpdateContainerExpirationPolicy */
export type UpdateContainerExpirationPolicyPayload = {
    __typename?: 'UpdateContainerExpirationPolicyPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The container expiration policy after mutation */
    containerExpirationPolicy?: Maybe<ContainerExpirationPolicy>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** Autogenerated return type of UpdateEpic */
export type UpdateEpicPayload = {
    __typename?: 'UpdateEpicPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The epic after mutation */
    epic?: Maybe<Epic>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
};

/** Autogenerated return type of UpdateImageDiffNote */
export type UpdateImageDiffNotePayload = {
    __typename?: 'UpdateImageDiffNotePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The note after mutation */
    note?: Maybe<Note>;
};

/** Autogenerated return type of UpdateIssue */
export type UpdateIssuePayload = {
    __typename?: 'UpdateIssuePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The issue after mutation */
    issue?: Maybe<Issue>;
};

/** Autogenerated return type of UpdateIteration */
export type UpdateIterationPayload = {
    __typename?: 'UpdateIterationPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The updated iteration */
    iteration?: Maybe<Iteration>;
};

/** Autogenerated return type of UpdateNote */
export type UpdateNotePayload = {
    __typename?: 'UpdateNotePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The note after mutation */
    note?: Maybe<Note>;
};

/** Autogenerated return type of UpdateRequirement */
export type UpdateRequirementPayload = {
    __typename?: 'UpdateRequirementPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The requirement after mutation */
    requirement?: Maybe<Requirement>;
};

/** Autogenerated return type of UpdateSnippet */
export type UpdateSnippetPayload = {
    __typename?: 'UpdateSnippetPayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The snippet after mutation */
    snippet?: Maybe<Snippet>;
};

export type User = {
    __typename?: 'User';
    /** Merge Requests assigned to the user */
    assignedMergeRequests?: Maybe<MergeRequestConnection>;
    /** Merge Requests authored by the user */
    authoredMergeRequests?: Maybe<MergeRequestConnection>;
    /** URL of the user's avatar */
    avatarUrl?: Maybe<Scalars['String']>;
    /** User email */
    email?: Maybe<Scalars['String']>;
    /** Group memberships of the user */
    groupMemberships?: Maybe<GroupMemberConnection>;
    /** ID of the user */
    id: Scalars['ID'];
    /** Human-readable name of the user */
    name: Scalars['String'];
    /** Project memberships of the user */
    projectMemberships?: Maybe<ProjectMemberConnection>;
    /** Snippets authored by the user */
    snippets?: Maybe<SnippetConnection>;
    /** Projects starred by the user */
    starredProjects?: Maybe<ProjectConnection>;
    /** State of the user */
    state: UserState;
    /** User status */
    status?: Maybe<UserStatus>;
    /** Todos of the user */
    todos: TodoConnection;
    /** Permissions for the current user on the resource */
    userPermissions: UserPermissions;
    /** Username of the user. Unique within this instance of GitLab */
    username: Scalars['String'];
    /** Web path of the user */
    webPath: Scalars['String'];
    /** Web URL of the user */
    webUrl: Scalars['String'];
};


export type UserAssignedMergeRequestsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    iids?: Maybe<Array<Scalars['String']>>;
    labels?: Maybe<Array<Scalars['String']>>;
    last?: Maybe<Scalars['Int']>;
    mergedAfter?: Maybe<Scalars['Time']>;
    mergedBefore?: Maybe<Scalars['Time']>;
    milestoneTitle?: Maybe<Scalars['String']>;
    projectId?: Maybe<Scalars['ID']>;
    projectPath?: Maybe<Scalars['String']>;
    sort?: Maybe<MergeRequestSort>;
    sourceBranches?: Maybe<Array<Scalars['String']>>;
    state?: Maybe<MergeRequestState>;
    targetBranches?: Maybe<Array<Scalars['String']>>;
};


export type UserAuthoredMergeRequestsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    iids?: Maybe<Array<Scalars['String']>>;
    labels?: Maybe<Array<Scalars['String']>>;
    last?: Maybe<Scalars['Int']>;
    mergedAfter?: Maybe<Scalars['Time']>;
    mergedBefore?: Maybe<Scalars['Time']>;
    milestoneTitle?: Maybe<Scalars['String']>;
    projectId?: Maybe<Scalars['ID']>;
    projectPath?: Maybe<Scalars['String']>;
    sort?: Maybe<MergeRequestSort>;
    sourceBranches?: Maybe<Array<Scalars['String']>>;
    state?: Maybe<MergeRequestState>;
    targetBranches?: Maybe<Array<Scalars['String']>>;
};


export type UserGroupMembershipsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type UserProjectMembershipsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};


export type UserSnippetsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    ids?: Maybe<Array<Scalars['ID']>>;
    last?: Maybe<Scalars['Int']>;
    type?: Maybe<TypeEnum>;
    visibility?: Maybe<VisibilityScopesEnum>;
};


export type UserStarredProjectsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    search?: Maybe<Scalars['String']>;
};


export type UserTodosArgs = {
    action?: Maybe<Array<TodoActionEnum>>;
    after?: Maybe<Scalars['String']>;
    authorId?: Maybe<Array<Scalars['ID']>>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    groupId?: Maybe<Array<Scalars['ID']>>;
    last?: Maybe<Scalars['Int']>;
    projectId?: Maybe<Array<Scalars['ID']>>;
    state?: Maybe<Array<TodoStateEnum>>;
    type?: Maybe<Array<TodoTargetEnum>>;
};

/** The connection type for User. */
export type UserConnection = {
    __typename?: 'UserConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<UserEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<User>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type UserEdge = {
    __typename?: 'UserEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<User>;
};

export type UserPermissions = {
    __typename?: 'UserPermissions';
    /** Indicates the user can perform `create_snippet` on this resource */
    createSnippet: Scalars['Boolean'];
};

export type UserStatus = {
    __typename?: 'UserStatus';
    /** String representation of emoji */
    emoji?: Maybe<Scalars['String']>;
    /** User status message */
    message?: Maybe<Scalars['String']>;
    /** HTML of the user status message */
    messageHtml?: Maybe<Scalars['String']>;
};

/** Represents the count of vulnerabilities by severity on a particular day */
export type VulnerabilitiesCountByDay = {
    __typename?: 'VulnerabilitiesCountByDay';
    /** Total number of vulnerabilities on a particular day with critical severity */
    critical: Scalars['Int'];
    /** Date for the count */
    date: Scalars['ISO8601Date'];
    /** Total number of vulnerabilities on a particular day with high severity */
    high: Scalars['Int'];
    /** Total number of vulnerabilities on a particular day with info severity */
    info: Scalars['Int'];
    /** Total number of vulnerabilities on a particular day with low severity */
    low: Scalars['Int'];
    /** Total number of vulnerabilities on a particular day with medium severity */
    medium: Scalars['Int'];
    /** Total number of vulnerabilities on a particular day */
    total: Scalars['Int'];
    /** Total number of vulnerabilities on a particular day with unknown severity */
    unknown: Scalars['Int'];
};

/** Represents the number of vulnerabilities for a particular severity on a particular day */
export type VulnerabilitiesCountByDayAndSeverity = {
    __typename?: 'VulnerabilitiesCountByDayAndSeverity';
    /** Number of vulnerabilities */
    count?: Maybe<Scalars['Int']>;
    /** Date for the count */
    day?: Maybe<Scalars['ISO8601Date']>;
    /** Severity of the counted vulnerabilities */
    severity?: Maybe<VulnerabilitySeverity>;
};

/** The connection type for VulnerabilitiesCountByDayAndSeverity. */
export type VulnerabilitiesCountByDayAndSeverityConnection = {
    __typename?: 'VulnerabilitiesCountByDayAndSeverityConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<VulnerabilitiesCountByDayAndSeverityEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<VulnerabilitiesCountByDayAndSeverity>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type VulnerabilitiesCountByDayAndSeverityEdge = {
    __typename?: 'VulnerabilitiesCountByDayAndSeverityEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<VulnerabilitiesCountByDayAndSeverity>;
};

/** The connection type for VulnerabilitiesCountByDay. */
export type VulnerabilitiesCountByDayConnection = {
    __typename?: 'VulnerabilitiesCountByDayConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<VulnerabilitiesCountByDayEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<VulnerabilitiesCountByDay>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type VulnerabilitiesCountByDayEdge = {
    __typename?: 'VulnerabilitiesCountByDayEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<VulnerabilitiesCountByDay>;
};

/** Represents a vulnerability */
export type Vulnerability = {
    __typename?: 'Vulnerability';
    /** Description of the vulnerability */
    description?: Maybe<Scalars['String']>;
    /** Timestamp of when the vulnerability was first detected */
    detectedAt: Scalars['Time'];
    /** GraphQL ID of the vulnerability */
    id: Scalars['ID'];
    /** Identifiers of the vulnerability. */
    identifiers: Array<VulnerabilityIdentifier>;
    /** List of issue links related to the vulnerability */
    issueLinks: VulnerabilityIssueLinkConnection;
    /** Location metadata for the vulnerability. Its fields depend on the type of security scan that found the vulnerability */
    location?: Maybe<VulnerabilityLocation>;
    /** Primary identifier of the vulnerability. */
    primaryIdentifier?: Maybe<VulnerabilityIdentifier>;
    /** The project on which the vulnerability was found */
    project?: Maybe<Project>;
    /** Type of the security report that found the vulnerability (SAST, DEPENDENCY_SCANNING, CONTAINER_SCANNING, DAST, SECRET_DETECTION, COVERAGE_FUZZING) */
    reportType?: Maybe<VulnerabilityReportType>;
    /** Indicates whether the vulnerability is fixed on the default branch or not */
    resolvedOnDefaultBranch: Scalars['Boolean'];
    /** Scanner metadata for the vulnerability. */
    scanner?: Maybe<VulnerabilityScanner>;
    /** Severity of the vulnerability (INFO, UNKNOWN, LOW, MEDIUM, HIGH, CRITICAL) */
    severity?: Maybe<VulnerabilitySeverity>;
    /** State of the vulnerability (DETECTED, DISMISSED, RESOLVED, CONFIRMED) */
    state?: Maybe<VulnerabilityState>;
    /** Title of the vulnerability */
    title?: Maybe<Scalars['String']>;
    /** Number of user notes attached to the vulnerability */
    userNotesCount: Scalars['Int'];
    /** Permissions for the current user on the resource */
    userPermissions: VulnerabilityPermissions;
    /** URL to the vulnerability's details page */
    vulnerabilityPath?: Maybe<Scalars['String']>;
};


/** Represents a vulnerability */
export type VulnerabilityIssueLinksArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
    linkType?: Maybe<VulnerabilityIssueLinkType>;
};

/** The connection type for Vulnerability. */
export type VulnerabilityConnection = {
    __typename?: 'VulnerabilityConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<VulnerabilityEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<Vulnerability>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type VulnerabilityEdge = {
    __typename?: 'VulnerabilityEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<Vulnerability>;
};

/** Represents a vulnerability identifier */
export type VulnerabilityIdentifier = {
    __typename?: 'VulnerabilityIdentifier';
    /** External ID of the vulnerability identifier */
    externalId?: Maybe<Scalars['String']>;
    /** External type of the vulnerability identifier */
    externalType?: Maybe<Scalars['String']>;
    /** Name of the vulnerability identifier */
    name?: Maybe<Scalars['String']>;
    /** URL of the vulnerability identifier */
    url?: Maybe<Scalars['String']>;
};

/** Represents an issue link of a vulnerability */
export type VulnerabilityIssueLink = {
    __typename?: 'VulnerabilityIssueLink';
    /** GraphQL ID of the vulnerability */
    id: Scalars['ID'];
    /** The issue attached to issue link */
    issue: Issue;
    /** Type of the issue link */
    linkType: VulnerabilityIssueLinkType;
};

/** The connection type for VulnerabilityIssueLink. */
export type VulnerabilityIssueLinkConnection = {
    __typename?: 'VulnerabilityIssueLinkConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<VulnerabilityIssueLinkEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<VulnerabilityIssueLink>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type VulnerabilityIssueLinkEdge = {
    __typename?: 'VulnerabilityIssueLinkEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<VulnerabilityIssueLink>;
};

/** Represents the location of a vulnerability found by a container security scan */
export type VulnerabilityLocationContainerScanning = {
    __typename?: 'VulnerabilityLocationContainerScanning';
    /** Dependency containing the vulnerability */
    dependency?: Maybe<VulnerableDependency>;
    /** Name of the vulnerable container image */
    image?: Maybe<Scalars['String']>;
    /** Operating system that runs on the vulnerable container image */
    operatingSystem?: Maybe<Scalars['String']>;
};

/** Represents the location of a vulnerability found by a Coverage Fuzzing scan */
export type VulnerabilityLocationCoverageFuzzing = {
    __typename?: 'VulnerabilityLocationCoverageFuzzing';
    /** Number of the last relevant line in the vulnerable file */
    endLine?: Maybe<Scalars['String']>;
    /** Path to the vulnerable file */
    file?: Maybe<Scalars['String']>;
    /** Number of the first relevant line in the vulnerable file */
    startLine?: Maybe<Scalars['String']>;
    /** Class containing the vulnerability */
    vulnerableClass?: Maybe<Scalars['String']>;
    /** Method containing the vulnerability */
    vulnerableMethod?: Maybe<Scalars['String']>;
};

/** Represents the location of a vulnerability found by a DAST scan */
export type VulnerabilityLocationDast = {
    __typename?: 'VulnerabilityLocationDast';
    /** Domain name of the vulnerable request */
    hostname?: Maybe<Scalars['String']>;
    /** Query parameter for the URL on which the vulnerability occurred */
    param?: Maybe<Scalars['String']>;
    /** URL path and query string of the vulnerable request */
    path?: Maybe<Scalars['String']>;
    /** HTTP method of the vulnerable request */
    requestMethod?: Maybe<Scalars['String']>;
};

/** Represents the location of a vulnerability found by a dependency security scan */
export type VulnerabilityLocationDependencyScanning = {
    __typename?: 'VulnerabilityLocationDependencyScanning';
    /** Dependency containing the vulnerability */
    dependency?: Maybe<VulnerableDependency>;
    /** Path to the vulnerable file */
    file?: Maybe<Scalars['String']>;
};

/** Represents the location of a vulnerability found by a SAST scan */
export type VulnerabilityLocationSast = {
    __typename?: 'VulnerabilityLocationSast';
    /** Number of the last relevant line in the vulnerable file */
    endLine?: Maybe<Scalars['String']>;
    /** Path to the vulnerable file */
    file?: Maybe<Scalars['String']>;
    /** Number of the first relevant line in the vulnerable file */
    startLine?: Maybe<Scalars['String']>;
    /** Class containing the vulnerability */
    vulnerableClass?: Maybe<Scalars['String']>;
    /** Method containing the vulnerability */
    vulnerableMethod?: Maybe<Scalars['String']>;
};

/** Represents the location of a vulnerability found by a secret detection scan */
export type VulnerabilityLocationSecretDetection = {
    __typename?: 'VulnerabilityLocationSecretDetection';
    /** Number of the last relevant line in the vulnerable file */
    endLine?: Maybe<Scalars['String']>;
    /** Path to the vulnerable file */
    file?: Maybe<Scalars['String']>;
    /** Number of the first relevant line in the vulnerable file */
    startLine?: Maybe<Scalars['String']>;
    /** Class containing the vulnerability */
    vulnerableClass?: Maybe<Scalars['String']>;
    /** Method containing the vulnerability */
    vulnerableMethod?: Maybe<Scalars['String']>;
};

/** Check permissions for the current user on a vulnerability */
export type VulnerabilityPermissions = {
    __typename?: 'VulnerabilityPermissions';
    /** Indicates the user can perform `admin_vulnerability` on this resource */
    adminVulnerability: Scalars['Boolean'];
    /** Indicates the user can perform `admin_vulnerability_issue_link` on this resource */
    adminVulnerabilityIssueLink: Scalars['Boolean'];
    /** Indicates the user can perform `create_vulnerability` on this resource */
    createVulnerability: Scalars['Boolean'];
    /** Indicates the user can perform `create_vulnerability_export` on this resource */
    createVulnerabilityExport: Scalars['Boolean'];
    /** Indicates the user can perform `create_vulnerability_feedback` on this resource */
    createVulnerabilityFeedback: Scalars['Boolean'];
    /** Indicates the user can perform `destroy_vulnerability_feedback` on this resource */
    destroyVulnerabilityFeedback: Scalars['Boolean'];
    /** Indicates the user can perform `read_vulnerability_feedback` on this resource */
    readVulnerabilityFeedback: Scalars['Boolean'];
    /** Indicates the user can perform `update_vulnerability_feedback` on this resource */
    updateVulnerabilityFeedback: Scalars['Boolean'];
};

/** Autogenerated return type of VulnerabilityResolve */
export type VulnerabilityResolvePayload = {
    __typename?: 'VulnerabilityResolvePayload';
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Errors encountered during execution of the mutation. */
    errors: Array<Scalars['String']>;
    /** The vulnerability after state change */
    vulnerability?: Maybe<Vulnerability>;
};

/** Represents a vulnerability scanner */
export type VulnerabilityScanner = {
    __typename?: 'VulnerabilityScanner';
    /** External ID of the vulnerability scanner */
    externalId?: Maybe<Scalars['String']>;
    /** Name of the vulnerability scanner */
    name?: Maybe<Scalars['String']>;
    /** Type of the vulnerability report */
    reportType?: Maybe<VulnerabilityReportType>;
    /** Vendor of the vulnerability scanner */
    vendor?: Maybe<Scalars['String']>;
};

/** The connection type for VulnerabilityScanner. */
export type VulnerabilityScannerConnection = {
    __typename?: 'VulnerabilityScannerConnection';
    /** A list of edges. */
    edges?: Maybe<Array<Maybe<VulnerabilityScannerEdge>>>;
    /** A list of nodes. */
    nodes?: Maybe<Array<Maybe<VulnerabilityScanner>>>;
    /** Information to aid in pagination. */
    pageInfo: PageInfo;
};

/** An edge in a connection. */
export type VulnerabilityScannerEdge = {
    __typename?: 'VulnerabilityScannerEdge';
    /** A cursor for use in pagination. */
    cursor: Scalars['String'];
    /** The item at the end of the edge. */
    node?: Maybe<VulnerabilityScanner>;
};

/** Represents vulnerability counts by severity */
export type VulnerabilitySeveritiesCount = {
    __typename?: 'VulnerabilitySeveritiesCount';
    /** Number of vulnerabilities of CRITICAL severity of the project */
    critical?: Maybe<Scalars['Int']>;
    /** Number of vulnerabilities of HIGH severity of the project */
    high?: Maybe<Scalars['Int']>;
    /** Number of vulnerabilities of INFO severity of the project */
    info?: Maybe<Scalars['Int']>;
    /** Number of vulnerabilities of LOW severity of the project */
    low?: Maybe<Scalars['Int']>;
    /** Number of vulnerabilities of MEDIUM severity of the project */
    medium?: Maybe<Scalars['Int']>;
    /** Number of vulnerabilities of UNKNOWN severity of the project */
    unknown?: Maybe<Scalars['Int']>;
};

/** Represents a vulnerable dependency. Used in vulnerability location data */
export type VulnerableDependency = {
    __typename?: 'VulnerableDependency';
    /** The package associated with the vulnerable dependency */
    package?: Maybe<VulnerablePackage>;
    /** The version of the vulnerable dependency */
    version?: Maybe<Scalars['String']>;
};

/** Represents a vulnerable package. Used in vulnerability dependency data */
export type VulnerablePackage = {
    __typename?: 'VulnerablePackage';
    /** The name of the vulnerable package */
    name?: Maybe<Scalars['String']>;
};

/** Represents vulnerability letter grades with associated projects */
export type VulnerableProjectsByGrade = {
    __typename?: 'VulnerableProjectsByGrade';
    /** Number of projects within this grade */
    count: Scalars['Int'];
    /** Grade based on the highest severity vulnerability present */
    grade: VulnerabilityGrade;
    /** Projects within this grade */
    projects: ProjectConnection;
};


/** Represents vulnerability letter grades with associated projects */
export type VulnerableProjectsByGradeProjectsArgs = {
    after?: Maybe<Scalars['String']>;
    before?: Maybe<Scalars['String']>;
    first?: Maybe<Scalars['Int']>;
    last?: Maybe<Scalars['Int']>;
};

/** Access level to a resource */
export enum AccessLevelEnum {
    Developer = 'DEVELOPER',
    Guest = 'GUEST',
    Maintainer = 'MAINTAINER',
    NoAccess = 'NO_ACCESS',
    Owner = 'OWNER',
    Reporter = 'REPORTER'
}

/** Values for sorting alerts */
export enum AlertManagementAlertSort {
    /** Created time by ascending order */
    CreatedTimeAsc = 'CREATED_TIME_ASC',
    /** Created time by descending order */
    CreatedTimeDesc = 'CREATED_TIME_DESC',
    /** End time by ascending order */
    EndedAtAsc = 'ENDED_AT_ASC',
    /** End time by descending order */
    EndedAtDesc = 'ENDED_AT_DESC',
    /** Events count by ascending order */
    EventCountAsc = 'EVENT_COUNT_ASC',
    /** Events count by descending order */
    EventCountDesc = 'EVENT_COUNT_DESC',
    /** Severity from less critical to more critical */
    SeverityAsc = 'SEVERITY_ASC',
    /** Severity from more critical to less critical */
    SeverityDesc = 'SEVERITY_DESC',
    /** Start time by ascending order */
    StartedAtAsc = 'STARTED_AT_ASC',
    /** Start time by descending order */
    StartedAtDesc = 'STARTED_AT_DESC',
    /** Status by order: Ignored > Resolved > Acknowledged > Triggered */
    StatusAsc = 'STATUS_ASC',
    /** Status by order: Triggered > Acknowledged > Resolved > Ignored */
    StatusDesc = 'STATUS_DESC',
    /** Created time by ascending order */
    UpdatedTimeAsc = 'UPDATED_TIME_ASC',
    /** Created time by descending order */
    UpdatedTimeDesc = 'UPDATED_TIME_DESC',
    /** Created at ascending order */
    CreatedAsc = 'created_asc',
    /** Created at descending order */
    CreatedDesc = 'created_desc',
    /** Updated at ascending order */
    UpdatedAsc = 'updated_asc',
    /** Updated at descending order */
    UpdatedDesc = 'updated_desc'
}

/** Alert severity values */
export enum AlertManagementSeverity {
    /** Critical severity */
    Critical = 'CRITICAL',
    /** High severity */
    High = 'HIGH',
    /** Info severity */
    Info = 'INFO',
    /** Low severity */
    Low = 'LOW',
    /** Medium severity */
    Medium = 'MEDIUM',
    /** Unknown severity */
    Unknown = 'UNKNOWN'
}

/** Alert status values */
export enum AlertManagementStatus {
    /** Acknowledged status */
    Acknowledged = 'ACKNOWLEDGED',
    /** Ignored status */
    Ignored = 'IGNORED',
    /** Resolved status */
    Resolved = 'RESOLVED',
    /** Triggered status */
    Triggered = 'TRIGGERED'
}

/** Types of blob viewers */
export enum BlobViewersType {
    Auxiliary = 'auxiliary',
    Rich = 'rich',
    Simple = 'simple'
}

/** Mode of a commit action */
export enum CommitActionMode {
    /** Chmod command */
    Chmod = 'CHMOD',
    /** Create command */
    Create = 'CREATE',
    /** Delete command */
    Delete = 'DELETE',
    /** Move command */
    Move = 'MOVE',
    /** Update command */
    Update = 'UPDATE'
}

export enum CommitEncoding {
    /** Base64 encoding */
    Base64 = 'BASE64',
    /** Text encoding */
    Text = 'TEXT'
}

export enum ContainerExpirationPolicyCadenceEnum {
    /** Every day */
    EveryDay = 'EVERY_DAY',
    /** Every month */
    EveryMonth = 'EVERY_MONTH',
    /** Every three months */
    EveryThreeMonths = 'EVERY_THREE_MONTHS',
    /** Every two weeks */
    EveryTwoWeeks = 'EVERY_TWO_WEEKS',
    /** Every week */
    EveryWeek = 'EVERY_WEEK'
}

export enum ContainerExpirationPolicyKeepEnum {
    /** 50 tags per image name */
    FiftyTags = 'FIFTY_TAGS',
    /** 5 tags per image name */
    FiveTags = 'FIVE_TAGS',
    /** 100 tags per image name */
    OneHundredTags = 'ONE_HUNDRED_TAGS',
    /** 1 tag per image name */
    OneTag = 'ONE_TAG',
    /** 10 tags per image name */
    TenTags = 'TEN_TAGS',
    /** 25 tags per image name */
    TwentyFiveTags = 'TWENTY_FIVE_TAGS'
}

export enum ContainerExpirationPolicyOlderThanEnum {
    /** 14 days until tags are automatically removed */
    FourteenDays = 'FOURTEEN_DAYS',
    /** 90 days until tags are automatically removed */
    NinetyDays = 'NINETY_DAYS',
    /** 7 days until tags are automatically removed */
    SevenDays = 'SEVEN_DAYS',
    /** 30 days until tags are automatically removed */
    ThirtyDays = 'THIRTY_DAYS'
}

export enum DastScanTypeEnum {
    /** Passive DAST scan. This scan will not make active attacks against the target site. */
    Passive = 'PASSIVE'
}

export enum DastSiteProfileValidationStatusEnum {
    /** Site validation process finished but failed */
    FailedValidation = 'FAILED_VALIDATION',
    /** Site validation process is in progress */
    InprogressValidation = 'INPROGRESS_VALIDATION',
    /** Site validation process finished successfully */
    PassedValidation = 'PASSED_VALIDATION',
    /** Site validation process has not started */
    PendingValidation = 'PENDING_VALIDATION'
}

/** Mutation event of a design within a version */
export enum DesignVersionEvent {
    /** A creation event */
    Creation = 'CREATION',
    /** A deletion event */
    Deletion = 'DELETION',
    /** A modification event */
    Modification = 'MODIFICATION',
    /** No change */
    None = 'NONE'
}

/** Type of file the position refers to */
export enum DiffPositionType {
    Image = 'image',
    Text = 'text'
}

/** Type of a tree entry */
export enum EntryType {
    Blob = 'blob',
    Commit = 'commit',
    Tree = 'tree'
}

/** Roadmap sort values */
export enum EpicSort {
    /** End date at ascending order */
    EndDateAsc = 'end_date_asc',
    /** End date at descending order */
    EndDateDesc = 'end_date_desc',
    /** Start date at ascending order */
    StartDateAsc = 'start_date_asc',
    /** Start date at descending order */
    StartDateDesc = 'start_date_desc'
}

/** State of an epic */
export enum EpicState {
    All = 'all',
    Closed = 'closed',
    Opened = 'opened'
}

/** State event of an epic */
export enum EpicStateEvent {
    /** Close the epic */
    Close = 'CLOSE',
    /** Reopen the epic */
    Reopen = 'REOPEN'
}

/** Epic ID wildcard values */
export enum EpicWildcardId {
    /** Any epic is assigned */
    Any = 'ANY',
    /** No epic is assigned */
    None = 'NONE'
}

/** Health status of an issue or epic */
export enum HealthStatus {
    AtRisk = 'atRisk',
    NeedsAttention = 'needsAttention',
    OnTrack = 'onTrack'
}

/** Incident severity */
export enum IssuableSeverity {
    /** Critical severity */
    Critical = 'CRITICAL',
    /** High severity */
    High = 'HIGH',
    /** Low severity */
    Low = 'LOW',
    /** Medium severity */
    Medium = 'MEDIUM',
    /** Unknown severity */
    Unknown = 'UNKNOWN'
}

/** State of a GitLab issue or merge request */
export enum IssuableState {
    All = 'all',
    Closed = 'closed',
    Locked = 'locked',
    Opened = 'opened'
}

/** Values for sorting issues */
export enum IssueSort {
    /** Due date by ascending order */
    DueDateAsc = 'DUE_DATE_ASC',
    /** Due date by descending order */
    DueDateDesc = 'DUE_DATE_DESC',
    /** Label priority by ascending order */
    LabelPriorityAsc = 'LABEL_PRIORITY_ASC',
    /** Label priority by descending order */
    LabelPriorityDesc = 'LABEL_PRIORITY_DESC',
    /** Milestone due date by ascending order */
    MilestoneDueAsc = 'MILESTONE_DUE_ASC',
    /** Milestone due date by descending order */
    MilestoneDueDesc = 'MILESTONE_DUE_DESC',
    /** Priority by ascending order */
    PriorityAsc = 'PRIORITY_ASC',
    /** Priority by descending order */
    PriorityDesc = 'PRIORITY_DESC',
    /** Relative position by ascending order */
    RelativePositionAsc = 'RELATIVE_POSITION_ASC',
    /** Weight by ascending order */
    WeightAsc = 'WEIGHT_ASC',
    /** Weight by descending order */
    WeightDesc = 'WEIGHT_DESC',
    /** Created at ascending order */
    CreatedAsc = 'created_asc',
    /** Created at descending order */
    CreatedDesc = 'created_desc',
    /** Updated at ascending order */
    UpdatedAsc = 'updated_asc',
    /** Updated at descending order */
    UpdatedDesc = 'updated_desc'
}

/** State of a GitLab issue */
export enum IssueState {
    All = 'all',
    Closed = 'closed',
    Locked = 'locked',
    Opened = 'opened'
}

/** Issue type */
export enum IssueType {
    /** Incident issue type */
    Incident = 'INCIDENT',
    /** Issue issue type */
    Issue = 'ISSUE',
    /** Test Case issue type */
    TestCase = 'TEST_CASE'
}

/** State of a GitLab iteration */
export enum IterationState {
    All = 'all',
    Closed = 'closed',
    Opened = 'opened',
    Started = 'started',
    Upcoming = 'upcoming'
}

/** List limit metric setting */
export enum ListLimitMetric {
    AllMetrics = 'all_metrics',
    IssueCount = 'issue_count',
    IssueWeights = 'issue_weights'
}

/** Possible identifier types for a measurement */
export enum MeasurementIdentifier {
    /** Group count */
    Groups = 'GROUPS',
    /** Issue count */
    Issues = 'ISSUES',
    /** Merge request count */
    MergeRequests = 'MERGE_REQUESTS',
    /** Pipeline count */
    Pipelines = 'PIPELINES',
    /** Project count */
    Projects = 'PROJECTS',
    /** User count */
    Users = 'USERS'
}

/** Values for sorting merge requests */
export enum MergeRequestSort {
    /** Label priority by ascending order */
    LabelPriorityAsc = 'LABEL_PRIORITY_ASC',
    /** Label priority by descending order */
    LabelPriorityDesc = 'LABEL_PRIORITY_DESC',
    /** Merge time by ascending order */
    MergedAtAsc = 'MERGED_AT_ASC',
    /** Merge time by descending order */
    MergedAtDesc = 'MERGED_AT_DESC',
    /** Milestone due date by ascending order */
    MilestoneDueAsc = 'MILESTONE_DUE_ASC',
    /** Milestone due date by descending order */
    MilestoneDueDesc = 'MILESTONE_DUE_DESC',
    /** Priority by ascending order */
    PriorityAsc = 'PRIORITY_ASC',
    /** Priority by descending order */
    PriorityDesc = 'PRIORITY_DESC',
    /** Created at ascending order */
    CreatedAsc = 'created_asc',
    /** Created at descending order */
    CreatedDesc = 'created_desc',
    /** Updated at ascending order */
    UpdatedAsc = 'updated_asc',
    /** Updated at descending order */
    UpdatedDesc = 'updated_desc'
}

/** State of a GitLab merge request */
export enum MergeRequestState {
    All = 'all',
    Closed = 'closed',
    Locked = 'locked',
    Merged = 'merged',
    Opened = 'opened'
}

export enum MilestoneStateEnum {
    Active = 'active',
    Closed = 'closed'
}

/** The position to which the adjacent object should be moved */
export enum MoveType {
    /** The adjacent object will be moved after the object that is being moved */
    After = 'after',
    /** The adjacent object will be moved before the object that is being moved */
    Before = 'before'
}

/** Different toggles for changing mutator behavior */
export enum MutationOperationMode {
    /** Performs an append operation */
    Append = 'APPEND',
    /** Performs a removal operation */
    Remove = 'REMOVE',
    /** Performs a replace operation */
    Replace = 'REPLACE'
}

/** Values for sorting projects */
export enum NamespaceProjectSort {
    /** Most similar to the search query */
    Similarity = 'SIMILARITY'
}

export enum PackageTypeEnum {
    /** Packages from the composer package manager */
    Composer = 'COMPOSER',
    /** Packages from the conan package manager */
    Conan = 'CONAN',
    /** Packages from the generic package manager */
    Generic = 'GENERIC',
    /** Packages from the maven package manager */
    Maven = 'MAVEN',
    /** Packages from the npm package manager */
    Npm = 'NPM',
    /** Packages from the nuget package manager */
    Nuget = 'NUGET',
    /** Packages from the pypi package manager */
    Pypi = 'PYPI'
}

export enum PipelineConfigSourceEnum {
    AutoDevopsSource = 'AUTO_DEVOPS_SOURCE',
    BridgeSource = 'BRIDGE_SOURCE',
    ExternalProjectSource = 'EXTERNAL_PROJECT_SOURCE',
    ParameterSource = 'PARAMETER_SOURCE',
    RemoteSource = 'REMOTE_SOURCE',
    RepositorySource = 'REPOSITORY_SOURCE',
    UnknownSource = 'UNKNOWN_SOURCE',
    WebideSource = 'WEBIDE_SOURCE'
}

export enum PipelineStatusEnum {
    Canceled = 'CANCELED',
    Created = 'CREATED',
    Failed = 'FAILED',
    Manual = 'MANUAL',
    Pending = 'PENDING',
    Preparing = 'PREPARING',
    Running = 'RUNNING',
    Scheduled = 'SCHEDULED',
    Skipped = 'SKIPPED',
    Success = 'SUCCESS',
    WaitingForResource = 'WAITING_FOR_RESOURCE'
}

/** Names of compliance frameworks that can be assigned to a Project */
export enum ProjectSettingEnum {
    Gdpr = 'gdpr',
    Hipaa = 'hipaa',
    PciDss = 'pci_dss',
    Soc_2 = 'soc_2',
    Sox = 'sox'
}

/** Type of the link: `other`, `runbook`, `image`, `package`; defaults to `other` */
export enum ReleaseAssetLinkType {
    /** Image link type */
    Image = 'IMAGE',
    /** Other link type */
    Other = 'OTHER',
    /** Package link type */
    Package = 'PACKAGE',
    /** Runbook link type */
    Runbook = 'RUNBOOK'
}

/** State of a requirement */
export enum RequirementState {
    Archived = 'ARCHIVED',
    Opened = 'OPENED'
}

/** Size of UI component in SAST configuration page */
export enum SastUiComponentSize {
    Large = 'LARGE',
    Medium = 'MEDIUM',
    Small = 'SMALL'
}

/** The type of the security scanner */
export enum SecurityScannerType {
    ContainerScanning = 'CONTAINER_SCANNING',
    CoverageFuzzing = 'COVERAGE_FUZZING',
    Dast = 'DAST',
    DependencyScanning = 'DEPENDENCY_SCANNING',
    Sast = 'SAST',
    SecretDetection = 'SECRET_DETECTION'
}

/** State of a Sentry error */
export enum SentryErrorStatus {
    /** Error has been ignored */
    Ignored = 'IGNORED',
    /** Error has been resolved */
    Resolved = 'RESOLVED',
    /** Error has been ignored until next release */
    ResolvedInNextRelease = 'RESOLVED_IN_NEXT_RELEASE',
    /** Error is unresolved */
    Unresolved = 'UNRESOLVED'
}

export enum ServiceType {
    AlertsService = 'ALERTS_SERVICE',
    AsanaService = 'ASANA_SERVICE',
    AssemblaService = 'ASSEMBLA_SERVICE',
    BambooService = 'BAMBOO_SERVICE',
    BugzillaService = 'BUGZILLA_SERVICE',
    BuildkiteService = 'BUILDKITE_SERVICE',
    CampfireService = 'CAMPFIRE_SERVICE',
    ConfluenceService = 'CONFLUENCE_SERVICE',
    CustomIssueTrackerService = 'CUSTOM_ISSUE_TRACKER_SERVICE',
    DiscordService = 'DISCORD_SERVICE',
    DroneCiService = 'DRONE_CI_SERVICE',
    EmailsOnPushService = 'EMAILS_ON_PUSH_SERVICE',
    EwmService = 'EWM_SERVICE',
    ExternalWikiService = 'EXTERNAL_WIKI_SERVICE',
    FlowdockService = 'FLOWDOCK_SERVICE',
    GithubService = 'GITHUB_SERVICE',
    HangoutsChatService = 'HANGOUTS_CHAT_SERVICE',
    HipchatService = 'HIPCHAT_SERVICE',
    IrkerService = 'IRKER_SERVICE',
    JenkinsService = 'JENKINS_SERVICE',
    JiraService = 'JIRA_SERVICE',
    MattermostService = 'MATTERMOST_SERVICE',
    MattermostSlashCommandsService = 'MATTERMOST_SLASH_COMMANDS_SERVICE',
    MicrosoftTeamsService = 'MICROSOFT_TEAMS_SERVICE',
    PackagistService = 'PACKAGIST_SERVICE',
    PipelinesEmailService = 'PIPELINES_EMAIL_SERVICE',
    PivotaltrackerService = 'PIVOTALTRACKER_SERVICE',
    PrometheusService = 'PROMETHEUS_SERVICE',
    PushoverService = 'PUSHOVER_SERVICE',
    RedmineService = 'REDMINE_SERVICE',
    SlackService = 'SLACK_SERVICE',
    SlackSlashCommandsService = 'SLACK_SLASH_COMMANDS_SERVICE',
    TeamcityService = 'TEAMCITY_SERVICE',
    UnifyCircuitService = 'UNIFY_CIRCUIT_SERVICE',
    WebexTeamsService = 'WEBEX_TEAMS_SERVICE',
    YoutrackService = 'YOUTRACK_SERVICE'
}

/** Type of a snippet blob input action */
export enum SnippetBlobActionEnum {
    Create = 'create',
    Delete = 'delete',
    Move = 'move',
    Update = 'update'
}

/** Common sort values */
export enum Sort {
    /** Created at ascending order */
    CreatedAsc = 'created_asc',
    /** Created at descending order */
    CreatedDesc = 'created_desc',
    /** Updated at ascending order */
    UpdatedAsc = 'updated_asc',
    /** Updated at descending order */
    UpdatedDesc = 'updated_desc'
}

/** State of a test report */
export enum TestReportState {
    Failed = 'FAILED',
    Passed = 'PASSED'
}

export enum TodoActionEnum {
    ApprovalRequired = 'approval_required',
    Assigned = 'assigned',
    BuildFailed = 'build_failed',
    DirectlyAddressed = 'directly_addressed',
    Marked = 'marked',
    Mentioned = 'mentioned',
    Unmergeable = 'unmergeable'
}

export enum TodoStateEnum {
    Done = 'done',
    Pending = 'pending'
}

export enum TodoTargetEnum {
    /** An Alert */
    Alert = 'ALERT',
    /** A Commit */
    Commit = 'COMMIT',
    /** A Design */
    Design = 'DESIGN',
    /** An Epic */
    Epic = 'EPIC',
    /** An Issue */
    Issue = 'ISSUE',
    /** A MergeRequest */
    Mergerequest = 'MERGEREQUEST'
}

export enum TypeEnum {
    Personal = 'personal',
    Project = 'project'
}

/** Possible states of a user */
export enum UserState {
    /** The user is active and is able to use the system */
    Active = 'active',
    /** The user has been blocked and is prevented from using the system */
    Blocked = 'blocked',
    /** The user is no longer active and is unable to use the system */
    Deactivated = 'deactivated'
}

export enum VisibilityLevelsEnum {
    Internal = 'internal',
    Private = 'private',
    Public = 'public'
}

export enum VisibilityScopesEnum {
    Internal = 'internal',
    Private = 'private',
    Public = 'public'
}

/** The grade of the vulnerable project */
export enum VulnerabilityGrade {
    A = 'A',
    B = 'B',
    C = 'C',
    D = 'D',
    F = 'F'
}

/** The type of the issue link related to a vulnerability */
export enum VulnerabilityIssueLinkType {
    Created = 'CREATED',
    Related = 'RELATED'
}

/** The type of the security scan that found the vulnerability */
export enum VulnerabilityReportType {
    ContainerScanning = 'CONTAINER_SCANNING',
    CoverageFuzzing = 'COVERAGE_FUZZING',
    Dast = 'DAST',
    DependencyScanning = 'DEPENDENCY_SCANNING',
    Sast = 'SAST',
    SecretDetection = 'SECRET_DETECTION'
}

/** The severity of the vulnerability */
export enum VulnerabilitySeverity {
    Critical = 'CRITICAL',
    High = 'HIGH',
    Info = 'INFO',
    Low = 'LOW',
    Medium = 'MEDIUM',
    Unknown = 'UNKNOWN'
}

/** Vulnerability sort values */
export enum VulnerabilitySort {
    /** Severity in ascending order */
    SeverityAsc = 'severity_asc',
    /** Severity in descending order */
    SeverityDesc = 'severity_desc'
}

/** The state of the vulnerability */
export enum VulnerabilityState {
    Confirmed = 'CONFIRMED',
    Detected = 'DETECTED',
    Dismissed = 'DISMISSED',
    Resolved = 'RESOLVED'
}

/** Autogenerated input type of AddAwardEmoji */
export type AddAwardEmojiInput = {
    /** The global id of the awardable resource */
    awardableId: Scalars['ID'];
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The emoji name */
    name: Scalars['String'];
};

/** Autogenerated input type of AddProjectToSecurityDashboard */
export type AddProjectToSecurityDashboardInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** ID of the project to be added to Instance Security Dashboard */
    id: Scalars['ID'];
};

/** Autogenerated input type of AdminSidekiqQueuesDeleteJobs */
export type AdminSidekiqQueuesDeleteJobsInput = {
    /** Delete jobs matching caller_id in the context metadata */
    callerId?: Maybe<Scalars['String']>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Delete jobs matching project in the context metadata */
    project?: Maybe<Scalars['String']>;
    /** The name of the queue to delete jobs from */
    queueName: Scalars['String'];
    /** Delete jobs matching related_class in the context metadata */
    relatedClass?: Maybe<Scalars['String']>;
    /** Delete jobs matching root_namespace in the context metadata */
    rootNamespace?: Maybe<Scalars['String']>;
    /** Delete jobs matching subscription_plan in the context metadata */
    subscriptionPlan?: Maybe<Scalars['String']>;
    /** Delete jobs matching user in the context metadata */
    user?: Maybe<Scalars['String']>;
};

/** Autogenerated input type of AlertSetAssignees */
export type AlertSetAssigneesInput = {
    /** The usernames to assign to the alert. Replaces existing assignees by default. */
    assigneeUsernames: Array<Scalars['String']>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The iid of the alert to mutate */
    iid: Scalars['String'];
    /** The operation to perform. Defaults to REPLACE. */
    operationMode?: Maybe<MutationOperationMode>;
    /** The project the alert to mutate is in */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of AlertTodoCreate */
export type AlertTodoCreateInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The iid of the alert to mutate */
    iid: Scalars['String'];
    /** The project the alert to mutate is in */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of AwardEmojiAdd */
export type AwardEmojiAddInput = {
    /** The global id of the awardable resource */
    awardableId: Scalars['ID'];
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The emoji name */
    name: Scalars['String'];
};

/** Autogenerated input type of AwardEmojiRemove */
export type AwardEmojiRemoveInput = {
    /** The global id of the awardable resource */
    awardableId: Scalars['ID'];
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The emoji name */
    name: Scalars['String'];
};

/** Autogenerated input type of AwardEmojiToggle */
export type AwardEmojiToggleInput = {
    /** The global id of the awardable resource */
    awardableId: Scalars['ID'];
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The emoji name */
    name: Scalars['String'];
};

export type BoardIssueInput = {
    /** Filter by assignee username */
    assigneeUsername?: Maybe<Array<Maybe<Scalars['String']>>>;
    /** Filter by author username */
    authorUsername?: Maybe<Scalars['String']>;
    /** Filter by epic ID. Incompatible with epicWildcardId */
    epicId?: Maybe<Scalars['ID']>;
    /** Filter by epic ID wildcard. Incompatible with epicId */
    epicWildcardId?: Maybe<EpicWildcardId>;
    /** Filter by label name */
    labelName?: Maybe<Array<Maybe<Scalars['String']>>>;
    /** Filter by milestone title */
    milestoneTitle?: Maybe<Scalars['String']>;
    /** Filter by reaction emoji */
    myReactionEmoji?: Maybe<Scalars['String']>;
    /** List of negated params. Warning: this argument is experimental and a subject to change in future */
    not?: Maybe<NegatedBoardIssueInput>;
    /** Filter by release tag */
    releaseTag?: Maybe<Scalars['String']>;
    /** Search query for issue title or description */
    search?: Maybe<Scalars['String']>;
    /** Filter by weight */
    weight?: Maybe<Scalars['String']>;
};

/** Autogenerated input type of BoardListCreate */
export type BoardListCreateInput = {
    /** Global ID of an existing user */
    assigneeId?: Maybe<Scalars['UserID']>;
    /** Create the backlog list */
    backlog?: Maybe<Scalars['Boolean']>;
    /** Global ID of the issue board to mutate */
    boardId: Scalars['BoardID'];
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Global ID of an existing label */
    labelId?: Maybe<Scalars['LabelID']>;
    /** Global ID of an existing milestone */
    milestoneId?: Maybe<Scalars['MilestoneID']>;
};

/** Autogenerated input type of BoardListUpdateLimitMetrics */
export type BoardListUpdateLimitMetricsInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The new limit metric type for the list. */
    limitMetric?: Maybe<ListLimitMetric>;
    /** The global ID of the list. */
    listId: Scalars['ID'];
    /** The new maximum issue count limit. */
    maxIssueCount?: Maybe<Scalars['Int']>;
    /** The new maximum issue weight limit. */
    maxIssueWeight?: Maybe<Scalars['Int']>;
};

/** Autogenerated input type of ClusterAgentDelete */
export type ClusterAgentDeleteInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Global id of the cluster agent that will be deleted */
    id: Scalars['ClustersAgentID'];
};

/** Autogenerated input type of ClusterAgentTokenCreate */
export type ClusterAgentTokenCreateInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Global ID of the cluster agent that will be associated with the new token */
    clusterAgentId: Scalars['ClustersAgentID'];
};

/** Autogenerated input type of ClusterAgentTokenDelete */
export type ClusterAgentTokenDeleteInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Global ID of the cluster agent token that will be deleted */
    id: Scalars['ClustersAgentTokenID'];
};

export type CommitAction = {
    /** The action to perform, create, delete, move, update, chmod */
    action: CommitActionMode;
    /** Content of the file */
    content?: Maybe<Scalars['String']>;
    /** Encoding of the file. Default is text */
    encoding?: Maybe<CommitEncoding>;
    /** Enables/disables the execute flag on the file */
    executeFilemode?: Maybe<Scalars['Boolean']>;
    /** Full path to the file */
    filePath: Scalars['String'];
    /** Last known file commit ID */
    lastCommitId?: Maybe<Scalars['String']>;
    /** Original full path to the file being moved */
    previousPath?: Maybe<Scalars['String']>;
};

/** Autogenerated input type of CommitCreate */
export type CommitCreateInput = {
    /** Array of action hashes to commit as a batch */
    actions: Array<CommitAction>;
    /** Name of the branch */
    branch: Scalars['String'];
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Raw commit message */
    message: Scalars['String'];
    /** Project full path the branch is associated with */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of ConfigureSast */
export type ConfigureSastInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** SAST CI configuration for the project */
    configuration: SastCiConfigurationInput;
    /** Full path of the project */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of CreateAlertIssue */
export type CreateAlertIssueInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The iid of the alert to mutate */
    iid: Scalars['String'];
    /** The project the alert to mutate is in */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of CreateAnnotation */
export type CreateAnnotationInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The global id of the cluster to add an annotation to */
    clusterId?: Maybe<Scalars['ID']>;
    /** The path to a file defining the dashboard on which the annotation should be added */
    dashboardPath: Scalars['String'];
    /** The description of the annotation */
    description: Scalars['String'];
    /** Timestamp indicating ending moment to which the annotation relates */
    endingAt?: Maybe<Scalars['Time']>;
    /** The global id of the environment to add an annotation to */
    environmentId?: Maybe<Scalars['ID']>;
    /** Timestamp indicating starting moment to which the annotation relates */
    startingAt: Scalars['Time'];
};

/** Autogenerated input type of CreateBranch */
export type CreateBranchInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Name of the branch */
    name: Scalars['String'];
    /** Project full path the branch is associated with */
    projectPath: Scalars['ID'];
    /** Branch name or commit SHA to create branch from */
    ref: Scalars['String'];
};

/** Autogenerated input type of CreateClusterAgent */
export type CreateClusterAgentInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Name of the cluster agent */
    name: Scalars['String'];
    /** Full path of the associated project for this cluster agent */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of CreateDiffNote */
export type CreateDiffNoteInput = {
    /** Content of the note */
    body: Scalars['String'];
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The confidentiality flag of a note. Default is false. */
    confidential?: Maybe<Scalars['Boolean']>;
    /** The global id of the resource to add a note to */
    noteableId: Scalars['ID'];
    /** The position of this note on a diff */
    position: DiffPositionInput;
};

/** Autogenerated input type of CreateEpic */
export type CreateEpicInput = {
    /** The IDs of labels to be added to the epic. */
    addLabelIds?: Maybe<Array<Scalars['ID']>>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Indicates if the epic is confidential */
    confidential?: Maybe<Scalars['Boolean']>;
    /** The description of the epic */
    description?: Maybe<Scalars['String']>;
    /** The end date of the epic */
    dueDateFixed?: Maybe<Scalars['String']>;
    /** Indicates end date should be sourced from due_date_fixed field not the issue milestones */
    dueDateIsFixed?: Maybe<Scalars['Boolean']>;
    /** The group the epic to mutate is in */
    groupPath: Scalars['ID'];
    /** The IDs of labels to be removed from the epic. */
    removeLabelIds?: Maybe<Array<Scalars['ID']>>;
    /** The start date of the epic */
    startDateFixed?: Maybe<Scalars['String']>;
    /** Indicates start date should be sourced from start_date_fixed field not the issue milestones */
    startDateIsFixed?: Maybe<Scalars['Boolean']>;
    /** The title of the epic */
    title?: Maybe<Scalars['String']>;
};

/** Autogenerated input type of CreateImageDiffNote */
export type CreateImageDiffNoteInput = {
    /** Content of the note */
    body: Scalars['String'];
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The confidentiality flag of a note. Default is false. */
    confidential?: Maybe<Scalars['Boolean']>;
    /** The global id of the resource to add a note to */
    noteableId: Scalars['ID'];
    /** The position of this note on a diff */
    position: DiffImagePositionInput;
};

/** Autogenerated input type of CreateIteration */
export type CreateIterationInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The description of the iteration */
    description?: Maybe<Scalars['String']>;
    /** The end date of the iteration */
    dueDate?: Maybe<Scalars['String']>;
    /** The target group for the iteration */
    groupPath?: Maybe<Scalars['ID']>;
    /** The target project for the iteration */
    projectPath?: Maybe<Scalars['ID']>;
    /** The start date of the iteration */
    startDate?: Maybe<Scalars['String']>;
    /** The title of the iteration */
    title?: Maybe<Scalars['String']>;
};

/** Autogenerated input type of CreateNote */
export type CreateNoteInput = {
    /** Content of the note */
    body: Scalars['String'];
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The confidentiality flag of a note. Default is false. */
    confidential?: Maybe<Scalars['Boolean']>;
    /** The global id of the discussion this note is in reply to */
    discussionId?: Maybe<Scalars['ID']>;
    /** The global id of the resource to add a note to */
    noteableId: Scalars['ID'];
};

/** Autogenerated input type of CreateRequirement */
export type CreateRequirementInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The project full path the requirement is associated with */
    projectPath: Scalars['ID'];
    /** Title of the requirement */
    title: Scalars['String'];
};

/** Autogenerated input type of CreateSnippet */
export type CreateSnippetInput = {
    /** Actions to perform over the snippet repository and blobs */
    blobActions?: Maybe<Array<SnippetBlobActionInputType>>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Description of the snippet */
    description?: Maybe<Scalars['String']>;
    /** The project full path the snippet is associated with */
    projectPath?: Maybe<Scalars['ID']>;
    /** Title of the snippet */
    title: Scalars['String'];
    /** The paths to files uploaded in the snippet description */
    uploadedFiles?: Maybe<Array<Scalars['String']>>;
    /** The visibility level of the snippet */
    visibilityLevel: VisibilityLevelsEnum;
};

/** Autogenerated input type of CreateTestCase */
export type CreateTestCaseInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The test case description */
    description?: Maybe<Scalars['String']>;
    /** The IDs of labels to be added to the test case. */
    labelIds?: Maybe<Array<Scalars['ID']>>;
    /** The project full path to create the test case */
    projectPath: Scalars['ID'];
    /** The test case title */
    title: Scalars['String'];
};

/** Autogenerated input type of DastOnDemandScanCreate */
export type DastOnDemandScanCreateInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** ID of the scanner profile to be used for the scan. */
    dastScannerProfileId?: Maybe<Scalars['DastScannerProfileID']>;
    /** ID of the site profile to be used for the scan. */
    dastSiteProfileId: Scalars['DastSiteProfileID'];
    /** The project the site profile belongs to. */
    fullPath: Scalars['ID'];
};

/** Autogenerated input type of DastScannerProfileCreate */
export type DastScannerProfileCreateInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The project the scanner profile belongs to. */
    fullPath: Scalars['ID'];
    /** The name of the scanner profile. */
    profileName: Scalars['String'];
    /** The maximum number of minutes allowed for the spider to traverse the site. */
    spiderTimeout?: Maybe<Scalars['Int']>;
    /** The maximum number of seconds allowed for the site under test to respond to a request. */
    targetTimeout?: Maybe<Scalars['Int']>;
};

/** Autogenerated input type of DastScannerProfileDelete */
export type DastScannerProfileDeleteInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Full path for the project the scanner profile belongs to. */
    fullPath: Scalars['ID'];
    /** ID of the scanner profile to be deleted. */
    id: Scalars['DastScannerProfileID'];
};

/** Autogenerated input type of DastScannerProfileUpdate */
export type DastScannerProfileUpdateInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The project the scanner profile belongs to. */
    fullPath: Scalars['ID'];
    /** ID of the scanner profile to be updated. */
    id: Scalars['DastScannerProfileID'];
    /** The name of the scanner profile. */
    profileName: Scalars['String'];
    /** The maximum number of minutes allowed for the spider to traverse the site. */
    spiderTimeout: Scalars['Int'];
    /** The maximum number of seconds allowed for the site under test to respond to a request. */
    targetTimeout: Scalars['Int'];
};

/** Autogenerated input type of DastSiteProfileCreate */
export type DastSiteProfileCreateInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The project the site profile belongs to. */
    fullPath: Scalars['ID'];
    /** The name of the site profile. */
    profileName: Scalars['String'];
    /** The URL of the target to be scanned. */
    targetUrl?: Maybe<Scalars['String']>;
};

/** Autogenerated input type of DastSiteProfileDelete */
export type DastSiteProfileDeleteInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The project the site profile belongs to. */
    fullPath: Scalars['ID'];
    /** ID of the site profile to be deleted. */
    id: Scalars['DastSiteProfileID'];
};

/** Autogenerated input type of DastSiteProfileUpdate */
export type DastSiteProfileUpdateInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The project the site profile belongs to. */
    fullPath: Scalars['ID'];
    /** ID of the site profile to be updated. */
    id: Scalars['DastSiteProfileID'];
    /** The name of the site profile. */
    profileName: Scalars['String'];
    /** The URL of the target to be scanned. */
    targetUrl?: Maybe<Scalars['String']>;
};

/** Autogenerated input type of DeleteAnnotation */
export type DeleteAnnotationInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The global ID of the annotation to delete */
    id: Scalars['ID'];
};

/** Autogenerated input type of DesignManagementDelete */
export type DesignManagementDeleteInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The filenames of the designs to delete */
    filenames: Array<Scalars['String']>;
    /** The iid of the issue to modify designs for */
    iid: Scalars['ID'];
    /** The project where the issue is to upload designs for */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of DesignManagementMove */
export type DesignManagementMoveInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** ID of the design to move */
    id: Scalars['DesignManagementDesignID'];
    /** ID of the immediately following design */
    next?: Maybe<Scalars['DesignManagementDesignID']>;
    /** ID of the immediately preceding design */
    previous?: Maybe<Scalars['DesignManagementDesignID']>;
};

/** Autogenerated input type of DesignManagementUpload */
export type DesignManagementUploadInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The files to upload */
    files: Array<Scalars['Upload']>;
    /** The iid of the issue to modify designs for */
    iid: Scalars['ID'];
    /** The project where the issue is to upload designs for */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of DestroyBoard */
export type DestroyBoardInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The global ID of the board to destroy */
    id: Scalars['BoardID'];
};

/** Autogenerated input type of DestroyNote */
export type DestroyNoteInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The global id of the note to destroy */
    id: Scalars['ID'];
};

/** Autogenerated input type of DestroySnippet */
export type DestroySnippetInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The global id of the snippet to destroy */
    id: Scalars['ID'];
};

export type DiffImagePositionInput = {
    /** Merge base of the branch the comment was made on */
    baseSha?: Maybe<Scalars['String']>;
    /** SHA of the HEAD at the time the comment was made */
    headSha: Scalars['String'];
    /** Total height of the image */
    height: Scalars['Int'];
    /** The paths of the file that was changed. Both of the properties of this input are optional, but at least one of them is required */
    paths: DiffPathsInput;
    /** SHA of the branch being compared against */
    startSha: Scalars['String'];
    /** Total width of the image */
    width: Scalars['Int'];
    /** X position of the note */
    x: Scalars['Int'];
    /** Y position of the note */
    y: Scalars['Int'];
};

export type DiffPathsInput = {
    /** The path of the file on the head sha */
    newPath?: Maybe<Scalars['String']>;
    /** The path of the file on the start sha */
    oldPath?: Maybe<Scalars['String']>;
};

export type DiffPositionInput = {
    /** Merge base of the branch the comment was made on */
    baseSha?: Maybe<Scalars['String']>;
    /** SHA of the HEAD at the time the comment was made */
    headSha: Scalars['String'];
    /** Line on HEAD SHA that was changed */
    newLine: Scalars['Int'];
    /** Line on start SHA that was changed */
    oldLine?: Maybe<Scalars['Int']>;
    /** The paths of the file that was changed. Both of the properties of this input are optional, but at least one of them is required */
    paths: DiffPathsInput;
    /** SHA of the branch being compared against */
    startSha: Scalars['String'];
};

/** Autogenerated input type of DiscussionToggleResolve */
export type DiscussionToggleResolveInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The global id of the discussion */
    id: Scalars['ID'];
    /** Will resolve the discussion when true, and unresolve the discussion when false */
    resolve: Scalars['Boolean'];
};

/** Autogenerated input type of DismissVulnerability */
export type DismissVulnerabilityInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Reason why vulnerability should be dismissed */
    comment?: Maybe<Scalars['String']>;
    /** ID of the vulnerability to be dismissed */
    id: Scalars['ID'];
};

/** Autogenerated input type of EpicAddIssue */
export type EpicAddIssueInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The group the epic to mutate belongs to */
    groupPath: Scalars['ID'];
    /** The iid of the epic to mutate */
    iid: Scalars['ID'];
    /** The iid of the issue to be added */
    issueIid: Scalars['String'];
    /** The full path of the project the issue belongs to */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of EpicSetSubscription */
export type EpicSetSubscriptionInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The group the epic to mutate belongs to */
    groupPath: Scalars['ID'];
    /** The iid of the epic to mutate */
    iid: Scalars['ID'];
    /** The desired state of the subscription */
    subscribedState: Scalars['Boolean'];
};

/** A node of an epic tree. */
export type EpicTreeNodeFieldsInputType = {
    /** The id of the epic_issue or issue that the actual epic or issue is switched with */
    adjacentReferenceId?: Maybe<Scalars['ID']>;
    /** The id of the epic_issue or epic that is being moved */
    id: Scalars['ID'];
    /** ID of the new parent epic */
    newParentId?: Maybe<Scalars['ID']>;
    /** The type of the switch, after or before allowed */
    relativePosition?: Maybe<MoveType>;
};

/** Autogenerated input type of EpicTreeReorder */
export type EpicTreeReorderInput = {
    /** The id of the base epic of the tree */
    baseEpicId: Scalars['ID'];
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Parameters for updating the tree positions */
    moved: EpicTreeNodeFieldsInputType;
};

/** Autogenerated input type of IssueMoveList */
export type IssueMoveListInput = {
    /** Global ID of the board that the issue is in */
    boardId: Scalars['ID'];
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The ID of the parent epic. NULL when removing the association */
    epicId?: Maybe<Scalars['EpicID']>;
    /** ID of the board list that the issue will be moved from */
    fromListId?: Maybe<Scalars['ID']>;
    /** IID of the issue to mutate */
    iid: Scalars['String'];
    /** ID of issue that should be placed after the current issue */
    moveAfterId?: Maybe<Scalars['ID']>;
    /** ID of issue that should be placed before the current issue */
    moveBeforeId?: Maybe<Scalars['ID']>;
    /** Project the issue to mutate is in */
    projectPath: Scalars['ID'];
    /** ID of the board list that the issue will be moved to */
    toListId?: Maybe<Scalars['ID']>;
};

/** Autogenerated input type of IssueSetAssignees */
export type IssueSetAssigneesInput = {
    /** The usernames to assign to the resource. Replaces existing assignees by default. */
    assigneeUsernames: Array<Scalars['String']>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The IID of the issue to mutate */
    iid: Scalars['String'];
    /** The operation to perform. Defaults to REPLACE. */
    operationMode?: Maybe<MutationOperationMode>;
    /** The project the issue to mutate is in */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of IssueSetConfidential */
export type IssueSetConfidentialInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Whether or not to set the issue as a confidential. */
    confidential: Scalars['Boolean'];
    /** The IID of the issue to mutate */
    iid: Scalars['String'];
    /** The project the issue to mutate is in */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of IssueSetDueDate */
export type IssueSetDueDateInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The desired due date for the issue */
    dueDate: Scalars['Time'];
    /** The IID of the issue to mutate */
    iid: Scalars['String'];
    /** The project the issue to mutate is in */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of IssueSetEpic */
export type IssueSetEpicInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Global ID of the epic to be assigned to the issue, epic will be removed if absent or set to null */
    epicId?: Maybe<Scalars['ID']>;
    /** The IID of the issue to mutate */
    iid: Scalars['String'];
    /** The project the issue to mutate is in */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of IssueSetIteration */
export type IssueSetIterationInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The IID of the issue to mutate */
    iid: Scalars['String'];
    /** The iteration to assign to the issue. */
    iterationId?: Maybe<Scalars['ID']>;
    /** The project the issue to mutate is in */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of IssueSetLocked */
export type IssueSetLockedInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The IID of the issue to mutate */
    iid: Scalars['String'];
    /** Whether or not to lock discussion on the issue */
    locked: Scalars['Boolean'];
    /** The project the issue to mutate is in */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of IssueSetSeverity */
export type IssueSetSeverityInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The IID of the issue to mutate */
    iid: Scalars['String'];
    /** The project the issue to mutate is in */
    projectPath: Scalars['ID'];
    /** Set the incident severity level. */
    severity: IssuableSeverity;
};

/** Autogenerated input type of IssueSetSubscription */
export type IssueSetSubscriptionInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The IID of the issue to mutate */
    iid: Scalars['String'];
    /** The project the issue to mutate is in */
    projectPath: Scalars['ID'];
    /** The desired state of the subscription */
    subscribedState: Scalars['Boolean'];
};

/** Autogenerated input type of IssueSetWeight */
export type IssueSetWeightInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The IID of the issue to mutate */
    iid: Scalars['String'];
    /** The project the issue to mutate is in */
    projectPath: Scalars['ID'];
    /** The desired weight for the issue */
    weight: Scalars['Int'];
};

/** Autogenerated input type of JiraImportStart */
export type JiraImportStartInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Project key of the importer Jira project */
    jiraProjectKey: Scalars['String'];
    /** Project name of the importer Jira project */
    jiraProjectName?: Maybe<Scalars['String']>;
    /** The project to import the Jira project into */
    projectPath: Scalars['ID'];
    /** The mapping of Jira to GitLab users */
    usersMapping?: Maybe<Array<JiraUsersMappingInputType>>;
};

/** Autogenerated input type of JiraImportUsers */
export type JiraImportUsersInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The project to import the Jira users into */
    projectPath: Scalars['ID'];
    /** The index of the record the import should started at, default 0 (50 records returned) */
    startAt?: Maybe<Scalars['Int']>;
};

export type JiraUsersMappingInputType = {
    /** Id of the GitLab user */
    gitlabId?: Maybe<Scalars['Int']>;
    /** Jira account id of the user */
    jiraAccountId: Scalars['String'];
};

/** Autogenerated input type of MarkAsSpamSnippet */
export type MarkAsSpamSnippetInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The global id of the snippet to update */
    id: Scalars['ID'];
};

/** Autogenerated input type of MergeRequestCreate */
export type MergeRequestCreateInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Description of the merge request (Markdown rendered as HTML for caching) */
    description?: Maybe<Scalars['String']>;
    /** Labels of the merge request */
    labels?: Maybe<Array<Scalars['String']>>;
    /** Project full path the merge request is associated with */
    projectPath: Scalars['ID'];
    /** Source branch of the merge request */
    sourceBranch: Scalars['String'];
    /** Target branch of the merge request */
    targetBranch: Scalars['String'];
    /** Title of the merge request */
    title: Scalars['String'];
};

/** Autogenerated input type of MergeRequestSetAssignees */
export type MergeRequestSetAssigneesInput = {
    /** The usernames to assign to the resource. Replaces existing assignees by default. */
    assigneeUsernames: Array<Scalars['String']>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The iid of the merge request to mutate */
    iid: Scalars['String'];
    /** The operation to perform. Defaults to REPLACE. */
    operationMode?: Maybe<MutationOperationMode>;
    /** The project the merge request to mutate is in */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of MergeRequestSetLabels */
export type MergeRequestSetLabelsInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The iid of the merge request to mutate */
    iid: Scalars['String'];
    /** The Label IDs to set. Replaces existing labels by default. */
    labelIds: Array<Scalars['ID']>;
    /** Changes the operation mode. Defaults to REPLACE. */
    operationMode?: Maybe<MutationOperationMode>;
    /** The project the merge request to mutate is in */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of MergeRequestSetLocked */
export type MergeRequestSetLockedInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The iid of the merge request to mutate */
    iid: Scalars['String'];
    /** Whether or not to lock the merge request. */
    locked: Scalars['Boolean'];
    /** The project the merge request to mutate is in */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of MergeRequestSetMilestone */
export type MergeRequestSetMilestoneInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The iid of the merge request to mutate */
    iid: Scalars['String'];
    /** The milestone to assign to the merge request. */
    milestoneId?: Maybe<Scalars['ID']>;
    /** The project the merge request to mutate is in */
    projectPath: Scalars['ID'];
};

/** Autogenerated input type of MergeRequestSetSubscription */
export type MergeRequestSetSubscriptionInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The iid of the merge request to mutate */
    iid: Scalars['String'];
    /** The project the merge request to mutate is in */
    projectPath: Scalars['ID'];
    /** The desired state of the subscription */
    subscribedState: Scalars['Boolean'];
};

/** Autogenerated input type of MergeRequestSetWip */
export type MergeRequestSetWipInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The iid of the merge request to mutate */
    iid: Scalars['String'];
    /** The project the merge request to mutate is in */
    projectPath: Scalars['ID'];
    /** Whether or not to set the merge request as a WIP. */
    wip: Scalars['Boolean'];
};

/** Autogenerated input type of MergeRequestUpdate */
export type MergeRequestUpdateInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Description of the merge request (Markdown rendered as HTML for caching) */
    description?: Maybe<Scalars['String']>;
    /** The iid of the merge request to mutate */
    iid: Scalars['String'];
    /** The project the merge request to mutate is in */
    projectPath: Scalars['ID'];
    /** Target branch of the merge request */
    targetBranch?: Maybe<Scalars['String']>;
    /** Title of the merge request */
    title?: Maybe<Scalars['String']>;
};

/** Autogenerated input type of NamespaceIncreaseStorageTemporarily */
export type NamespaceIncreaseStorageTemporarilyInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The global id of the namespace to mutate */
    id: Scalars['ID'];
};

export type NegatedBoardIssueInput = {
    /** Filter by assignee username */
    assigneeUsername?: Maybe<Array<Maybe<Scalars['String']>>>;
    /** Filter by author username */
    authorUsername?: Maybe<Scalars['String']>;
    /** Filter by epic ID. Incompatible with epicWildcardId */
    epicId?: Maybe<Scalars['ID']>;
    /** Filter by label name */
    labelName?: Maybe<Array<Maybe<Scalars['String']>>>;
    /** Filter by milestone title */
    milestoneTitle?: Maybe<Scalars['String']>;
    /** Filter by reaction emoji */
    myReactionEmoji?: Maybe<Scalars['String']>;
    /** Filter by release tag */
    releaseTag?: Maybe<Scalars['String']>;
    /** Filter by weight */
    weight?: Maybe<Scalars['String']>;
};

/** Autogenerated input type of PipelineCancel */
export type PipelineCancelInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The id of the pipeline to mutate */
    id: Scalars['CiPipelineID'];
};

/** Autogenerated input type of PipelineDestroy */
export type PipelineDestroyInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The id of the pipeline to mutate */
    id: Scalars['CiPipelineID'];
};

/** Autogenerated input type of PipelineRetry */
export type PipelineRetryInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The id of the pipeline to mutate */
    id: Scalars['CiPipelineID'];
};

/** Autogenerated input type of RemoveAwardEmoji */
export type RemoveAwardEmojiInput = {
    /** The global id of the awardable resource */
    awardableId: Scalars['ID'];
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The emoji name */
    name: Scalars['String'];
};

/** Autogenerated input type of RemoveProjectFromSecurityDashboard */
export type RemoveProjectFromSecurityDashboardInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** ID of the project to remove from the Instance Security Dashboard */
    id: Scalars['ID'];
};

/** Autogenerated input type of RunDASTScan */
export type RunDastScanInput = {
    /** The branch to be associated with the scan. */
    branch: Scalars['String'];
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The project the DAST scan belongs to. */
    projectPath: Scalars['ID'];
    /** The type of scan to be run. */
    scanType: DastScanTypeEnum;
    /** The URL of the target to be scanned. */
    targetUrl: Scalars['String'];
};

/** Represents an entity in SAST CI configuration */
export type SastCiConfigurationEntityInput = {
    /** Default value that is used if value is empty */
    defaultValue: Scalars['String'];
    /** CI keyword of entity */
    field: Scalars['String'];
    /** Current value of the entity */
    value: Scalars['String'];
};

/** Represents a CI configuration of SAST */
export type SastCiConfigurationInput = {
    /** List of global entities related to SAST configuration */
    global?: Maybe<Array<SastCiConfigurationEntityInput>>;
    /** List of pipeline entities related to SAST configuration */
    pipeline?: Maybe<Array<SastCiConfigurationEntityInput>>;
};

/** Represents an action to perform over a snippet file */
export type SnippetBlobActionInputType = {
    /** Type of input action */
    action: SnippetBlobActionEnum;
    /** Snippet file content */
    content?: Maybe<Scalars['String']>;
    /** Path of the snippet file */
    filePath: Scalars['String'];
    /** Previous path of the snippet file */
    previousPath?: Maybe<Scalars['String']>;
};

/** Autogenerated input type of TodoMarkDone */
export type TodoMarkDoneInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The global id of the todo to mark as done */
    id: Scalars['ID'];
};

/** Autogenerated input type of TodoRestore */
export type TodoRestoreInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The global id of the todo to restore */
    id: Scalars['ID'];
};

/** Autogenerated input type of TodoRestoreMany */
export type TodoRestoreManyInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The global ids of the todos to restore (a maximum of 50 is supported at once) */
    ids: Array<Scalars['ID']>;
};

/** Autogenerated input type of TodosMarkAllDone */
export type TodosMarkAllDoneInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
};

/** Autogenerated input type of ToggleAwardEmoji */
export type ToggleAwardEmojiInput = {
    /** The global id of the awardable resource */
    awardableId: Scalars['ID'];
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The emoji name */
    name: Scalars['String'];
};

/** Autogenerated input type of UpdateAlertStatus */
export type UpdateAlertStatusInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The iid of the alert to mutate */
    iid: Scalars['String'];
    /** The project the alert to mutate is in */
    projectPath: Scalars['ID'];
    /** The status to set the alert */
    status: AlertManagementStatus;
};

/** Autogenerated input type of UpdateBoard */
export type UpdateBoardInput = {
    /** The id of user to be assigned to the board. */
    assigneeId?: Maybe<Scalars['ID']>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Whether or not backlog list is hidden. */
    hideBacklogList?: Maybe<Scalars['Boolean']>;
    /** Whether or not closed list is hidden. */
    hideClosedList?: Maybe<Scalars['Boolean']>;
    /** The board global id. */
    id: Scalars['ID'];
    /** The id of milestone to be assigned to the board. */
    milestoneId?: Maybe<Scalars['ID']>;
    /** Name of the board */
    name?: Maybe<Scalars['String']>;
    /** The weight value to be assigned to the board. */
    weight?: Maybe<Scalars['Int']>;
};

/** Autogenerated input type of UpdateBoardList */
export type UpdateBoardListInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Indicates if list is collapsed for this user */
    collapsed?: Maybe<Scalars['Boolean']>;
    /** Global ID of the list. */
    listId: Scalars['ID'];
    /** Position of list within the board */
    position?: Maybe<Scalars['Int']>;
};

/** Autogenerated input type of UpdateContainerExpirationPolicy */
export type UpdateContainerExpirationPolicyInput = {
    /** This container expiration policy schedule */
    cadence?: Maybe<ContainerExpirationPolicyCadenceEnum>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Indicates whether this container expiration policy is enabled */
    enabled?: Maybe<Scalars['Boolean']>;
    /** Number of tags to retain */
    keepN?: Maybe<ContainerExpirationPolicyKeepEnum>;
    /** Tags with names matching this regex pattern will expire */
    nameRegex?: Maybe<Scalars['UntrustedRegexp']>;
    /** Tags with names matching this regex pattern will be preserved */
    nameRegexKeep?: Maybe<Scalars['UntrustedRegexp']>;
    /** Tags older that this will expire */
    olderThan?: Maybe<ContainerExpirationPolicyOlderThanEnum>;
    /** The project path where the container expiration policy is located */
    projectPath: Scalars['ID'];
};

export type UpdateDiffImagePositionInput = {
    /** Total height of the image */
    height?: Maybe<Scalars['Int']>;
    /** Total width of the image */
    width?: Maybe<Scalars['Int']>;
    /** X position of the note */
    x?: Maybe<Scalars['Int']>;
    /** Y position of the note */
    y?: Maybe<Scalars['Int']>;
};

/** Autogenerated input type of UpdateEpic */
export type UpdateEpicInput = {
    /** The IDs of labels to be added to the epic. */
    addLabelIds?: Maybe<Array<Scalars['ID']>>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Indicates if the epic is confidential */
    confidential?: Maybe<Scalars['Boolean']>;
    /** The description of the epic */
    description?: Maybe<Scalars['String']>;
    /** The end date of the epic */
    dueDateFixed?: Maybe<Scalars['String']>;
    /** Indicates end date should be sourced from due_date_fixed field not the issue milestones */
    dueDateIsFixed?: Maybe<Scalars['Boolean']>;
    /** The group the epic to mutate is in */
    groupPath: Scalars['ID'];
    /** The iid of the epic to mutate */
    iid: Scalars['ID'];
    /** The IDs of labels to be removed from the epic. */
    removeLabelIds?: Maybe<Array<Scalars['ID']>>;
    /** The start date of the epic */
    startDateFixed?: Maybe<Scalars['String']>;
    /** Indicates start date should be sourced from start_date_fixed field not the issue milestones */
    startDateIsFixed?: Maybe<Scalars['Boolean']>;
    /** State event for the epic */
    stateEvent?: Maybe<EpicStateEvent>;
    /** The title of the epic */
    title?: Maybe<Scalars['String']>;
};

/** Autogenerated input type of UpdateImageDiffNote */
export type UpdateImageDiffNoteInput = {
    /** Content of the note */
    body?: Maybe<Scalars['String']>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The global id of the note to update */
    id: Scalars['ID'];
    /** The position of this note on a diff */
    position?: Maybe<UpdateDiffImagePositionInput>;
};

/** Autogenerated input type of UpdateIssue */
export type UpdateIssueInput = {
    /** The IDs of labels to be added to the issue. */
    addLabelIds?: Maybe<Array<Scalars['ID']>>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Indicates the issue is confidential */
    confidential?: Maybe<Scalars['Boolean']>;
    /** Description of the issue */
    description?: Maybe<Scalars['String']>;
    /** Due date of the issue */
    dueDate?: Maybe<Scalars['Time']>;
    /** The ID of the parent epic. NULL when removing the association */
    epicId?: Maybe<Scalars['ID']>;
    /** The desired health status */
    healthStatus?: Maybe<HealthStatus>;
    /** The IID of the issue to mutate */
    iid: Scalars['String'];
    /** Indicates discussion is locked on the issue */
    locked?: Maybe<Scalars['Boolean']>;
    /** The ID of the milestone to be assigned, milestone will be removed if set to null. */
    milestoneId?: Maybe<Scalars['ID']>;
    /** The project the issue to mutate is in */
    projectPath: Scalars['ID'];
    /** The IDs of labels to be removed from the issue. */
    removeLabelIds?: Maybe<Array<Scalars['ID']>>;
    /** Title of the issue */
    title?: Maybe<Scalars['String']>;
};

/** Autogenerated input type of UpdateIteration */
export type UpdateIterationInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The description of the iteration */
    description?: Maybe<Scalars['String']>;
    /** The end date of the iteration */
    dueDate?: Maybe<Scalars['String']>;
    /** The group of the iteration */
    groupPath: Scalars['ID'];
    /** The id of the iteration */
    id: Scalars['ID'];
    /** The start date of the iteration */
    startDate?: Maybe<Scalars['String']>;
    /** The title of the iteration */
    title?: Maybe<Scalars['String']>;
};

/** Autogenerated input type of UpdateNote */
export type UpdateNoteInput = {
    /** Content of the note */
    body?: Maybe<Scalars['String']>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The confidentiality flag of a note. Default is false. */
    confidential?: Maybe<Scalars['Boolean']>;
    /** The global id of the note to update */
    id: Scalars['ID'];
};

/** Autogenerated input type of UpdateRequirement */
export type UpdateRequirementInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** The iid of the requirement to update */
    iid: Scalars['String'];
    /** Creates a test report for the requirement with the given state */
    lastTestReportState?: Maybe<TestReportState>;
    /** The project full path the requirement is associated with */
    projectPath: Scalars['ID'];
    /** State of the requirement */
    state?: Maybe<RequirementState>;
    /** Title of the requirement */
    title?: Maybe<Scalars['String']>;
};

/** Autogenerated input type of UpdateSnippet */
export type UpdateSnippetInput = {
    /** Actions to perform over the snippet repository and blobs */
    blobActions?: Maybe<Array<SnippetBlobActionInputType>>;
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** Description of the snippet */
    description?: Maybe<Scalars['String']>;
    /** The global id of the snippet to update */
    id: Scalars['ID'];
    /** Title of the snippet */
    title?: Maybe<Scalars['String']>;
    /** The visibility level of the snippet */
    visibilityLevel?: Maybe<VisibilityLevelsEnum>;
};

/** Autogenerated input type of VulnerabilityResolve */
export type VulnerabilityResolveInput = {
    /** A unique identifier for the client performing the mutation. */
    clientMutationId?: Maybe<Scalars['String']>;
    /** ID of the vulnerability to be resolveed */
    id: Scalars['VulnerabilityID'];
};
