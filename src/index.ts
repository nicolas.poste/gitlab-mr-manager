import { GraphQLClient } from 'graphql-request';
import { RebaseReviewed } from './operations/rebase-reviewed';
import { ApproveRenovateMr } from './operations/renovate-approvals';

const PRIVATE_TOKEN: string = String(process.env.GITLAB_TOKEN);
const GITLAB_HOST: string = String(process.env.GITLAB_HOST);

interface HeaderConfig {
    headers: { [key: string]: string }
}

const headersConfig: HeaderConfig = {'headers': {'PRIVATE-TOKEN': PRIVATE_TOKEN}};
const graphQLClient = new GraphQLClient(`${GITLAB_HOST}/api/graphql`, headersConfig);

async function main() {

    new RebaseReviewed(graphQLClient, headersConfig).proceed();
    new ApproveRenovateMr(graphQLClient, headersConfig).proceed();

}

main().catch((error) => {
    console.error(error);
    throw error;
});
