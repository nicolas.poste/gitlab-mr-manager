
# GitLab MR Manager

Use this repository as a base to build your own merge request managing system!

Here are a few examples of operations:
- mrs-to-rebase: rebase MRs which needs rebase and are not under "work in progress" (WIP)
- renovate-mrs: approve automatically MRs from [renovate](https://docs.renovatebot.com/) (which have "update" label in this case) - GitLab's starter license needed

You can add your own rules by extending `gitlab-operation.ts`. Don't forget to modify `index.ts` also.

## How to use

### With GitLab jobs

Define two variables in CI/CD config, GITLAB_TOKEN & GITLAB_HOST and that's pretty much it!  
You can create a scheduled pipeline or create a new pipeline using GitLab's API, whenever a pipeline on a master branch succeeds for example!

### Locally

```shell script
    npm ci
    export GITLAB_TOKEN=<your GitLab API Token>
    export GITLAB_HOST=<GitLab host, such as https://gitlab.com>
    npm run start
```

## Trigger a new pipeline from another repository

Use this job or adapt it to trigger a new pipeline each time a push is done on master on another repository.
It has to be defined in the other repository. 

This job can help your branches to be kept up to date by rebasing them as soon as a push is done.

```yaml
update:mr-manager:
  stage: build
  image: appropriate/curl
  variables:
    GIT_STRATEGY: none
    GIT_CHECKOUT: "false"
    MR_MANAGER_PROJECT_ID: <project id>
  dependencies: []
  needs: []
  cache: {}
  only:
    - master
  except:
    - schedules
  script:
    - 'curl --request POST --header "PRIVATE-TOKEN: $CI_API_TOKEN" "https://gitlab.socrate.vsct.fr/api/v4/projects/$MR_MANAGER_PROJECT_ID/pipeline?ref=master"'
  allow_failure: true
  interruptible: true
```

## Convert graphql schema to typescript

In case you need to update the schema, you can use this website: https://graphql-code-generator.com/

## Contribution

Any contribution is welcome
